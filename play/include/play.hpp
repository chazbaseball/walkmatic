#ifndef PLAY_H
#define PLAY_H
#include "../../common/common.hpp"
#include "../../common/diagnostics/include/diagnostics.hpp"
#include "../../common/drivers/imu/include/imu.hpp"
#include "../../common/drivers/servo/include/servo.hpp"
#include "../../common/drivers/ultrasonic/include/ultrasonic.hpp"
#include "../../common/conditioner/include/conditioner.hpp"
#include "../../common/compfilter/include/compfilter.hpp"
#include "../../common/pathgenerator/include/pathgenerator.hpp"
#include "../../common/plotter/include/plotter.hpp"
#include "../../common/controller/include/controller.hpp"

namespace play
{
	static uint8_t MESSAGE_FREQ[] = "FREQ";
	static const uint8_t MESSAGE_FREQ_LENGTH= 4u;
	
    class Play
    {
        private:
			diagnostics::Diagnostics diagnostics;
			const configPlay::Config CONFIG;
            imu::Imu imu;
			ultrasonic::Ultrasonic ultrasonic;
            conditioner::Conditioner conditioner;
            compfilter::CompFilter compfilter;
			controller::Controller controller;
			servo::Servo servo;
			float past_time;
			float current_curvature;
        public:
            Play(const configPlay::Config & config);
            void step(Path & path); 
			void reset();
            ~Play();
    };
}
#endif
