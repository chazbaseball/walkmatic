#include "../include/play.hpp"

// Constructor
play::Play::Play(const configPlay::Config & config) : diagnostics(config.DIAGNOSTICS_CONFIG),
																		 CONFIG(config),
																	     servo(config.SERVO_CONFIG),
																		 ultrasonic(config.ULTRASONIC_CONFIG),
																		 imu(config.IMU_CONFIG),
																		 conditioner(config.CONDITIONER_CONFIG),
																		 compfilter(config.COMP_FILTER_CONFIG),
																		 controller(config.CONTROLLER_CONFIG)
{
	past_time = static_cast<float>(getTimestamp()) * configTimestamp::TICKS_TO_SECONDS;
}

void play::Play::reset()
{
	current_curvature = 0.0f;
	conditioner.reset();
	compfilter.reset();
	controller.reset();
	servo.disableAndReset();
}

// Keeps the walker on the path
void play::Play::step(Path & path)
{
	float current_time = static_cast<float>(getTimestamp()) * configTimestamp::TICKS_TO_SECONDS;
	diagnostics.update(MESSAGE_FREQ, MESSAGE_FREQ_LENGTH, 1.0f / (current_time - past_time));
	
	while(current_time - past_time < 1.0f / CONFIG.MAX_FREQUENCY) // Make sure things are not running too fast
	{
		current_time = static_cast<float>(getTimestamp()) * configTimestamp::TICKS_TO_SECONDS;
	}
	
	past_time = static_cast<float>(getTimestamp()) * configTimestamp::TICKS_TO_SECONDS;
	
	if(path.isCircular())
	{
		Measurement sensor_measurement;
		sensor_measurement.imu_data = imu.getMeasurement();
		sensor_measurement.ultrasonic_data = ultrasonic.getMeasurement();
		States raw_states = conditioner.generateStates(sensor_measurement);
		if(conditioner.stateStatus())
		{
			compfilter.loadCommandedCurvature(current_curvature);
			State output = compfilter.filter(raw_states);
			SteeringCommand command = controller.step(output, path);
			servo.updateTheta(command.theta_left, command.theta_right);
			current_curvature = controller.getCommandedCurvature();
		}
	}
}

// Deconstructor
play::Play::~Play()
{

}

#ifdef PLAY_UNIT_TEST
#include "../../common/interrupts/include/interrupts.hpp"

int main()
{
	timestampInitialize();
	play::Play play(configPlay::CONFIG_DEFAULT);
	Path path;

	while(1)
	{
		play.step(path);
	}
}

#endif