#include "record/include/record.hpp"
#include "play/include/play.hpp"
#include "modemanager/include/modemanager.hpp"
#include "common/common.hpp"
#include "common/interrupts/include/interrupts.hpp"

#ifdef RUN_ALL
int main()
{
	// Initialize timestamp
	timestampInitialize();
	
	// Get the mode manager
	modemanager::ModeManager* modemanager = getModeManager();
	
	// Create a path
	Path path;
	
	// Create the mode objects (record and play)
	record::Record record(configRecord::CONFIG_DEFAULT);
	play::Play play(configPlay::CONFIG_DEFAULT);
	bool valid_path = false;
	bool use_test_path = false;
	
	while(1u)
	{
		modemanager::Mode mode = modemanager->updateMode();
		modemanager->printMode();
		switch(mode)
		{
			case modemanager::RECORD:
			{
				path.reset(); // Reset path object every time RECORD mode is entered
				while(mode == modemanager::RECORD)
				{
					record.step(path); // Record path traced by parent
					mode = modemanager->updateMode();
				}
				record.reset(); // Reset record object every time RECORD mode is left
				
				if(mode == modemanager::READY)
				{
					if(use_test_path)
					{
						path.reset();
						for(uint32_t i = 0u; i < 12u; i++)
						{
							path.add(pathgenerator::TestPath[i].x_part, pathgenerator::TestPath[i].y_part);
						}
					}
					valid_path = record.finish(path); // Smooth path when transitioning from RECORD to PLAY mode
				}
				
				while(mode == modemanager::READY)
				{
					mode = modemanager->updateMode(); // Stay in READY mode until user indicates otherwise
					if(mode == modemanager::RECORD)
					{
						path.reset();
					}
				}
				break;
			}
			case modemanager::PLAY:
			{
				while(mode == modemanager::PLAY)
				{
					if(valid_path)
					{
						play.step(path); // Stay on path traced by parent
					}
					mode = modemanager->updateMode();
				}
				play.reset(); // Reset play object every time PLAY mode is left
				break;
			}
			case modemanager::CHARGE:
			{
				path.reset(); // The path is reset every time CHARGE mode is entered
				while(mode == modemanager::CHARGE)
				{
					mode = modemanager->updateMode();
				}
				break;
			}
			default:
				break;
		}
	}
	
    return 0; // Should never get here
}
#endif 

