# How to create plantuml diagrams #
All diagrams in this repository are created with a plantuml tool.

1. Clone this repository
2. Create a new <name>.puml file in this directory
3. Open the .jar executable in this directory
4. Download Graphviz and move Graphviz to Program Files (if on Windows)
5. Create an environment variable named GRAPHVIZ_DOT=C:\Program Files\Graphviz\bin\dot.exe
4. Code the diagram in the newly created .puml file
5. Save the .puml file and a .png of the diagram will automatically be created