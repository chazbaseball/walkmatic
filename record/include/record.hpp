#ifndef RECORD_H
#define RECORD_H
#include "../../common/common.hpp"
#include "../../common/diagnostics/include/diagnostics.hpp"
#include "../../common/drivers/imu/include/imu.hpp"
#include "../../common/drivers/ultrasonic/include/ultrasonic.hpp"
#include "../../common/conditioner/include/conditioner.hpp"
#include "../../common/compfilter/include/compfilter.hpp"
#include "../../common/pathgenerator/include/pathgenerator.hpp"
#include "../../common/plotter/include/plotter.hpp"

namespace record
{
	static uint8_t MESSAGE_FREQ[] = "FREQ";
	static const uint8_t MESSAGE_FREQ_LENGTH= 4u;
	
    class Record
    {
        private:
            diagnostics::Diagnostics diagnostics;
			const configRecord::Config CONFIG;
            imu::Imu imu;
			ultrasonic::Ultrasonic ultrasonic;
            conditioner::Conditioner conditioner;
            compfilter::CompFilter compfilter;
            pathgenerator::Pathgenerator pathgenerator;
			float past_time;
        public:
            Record(const configRecord::Config & config);
            void step(Path & path); // Record updates the path
			bool finish(Path & path);
			void reset();
            ~Record();
    };
}
#endif
