#include "../include/record.hpp"

// Constructor
record::Record::Record(const configRecord::Config & config) : diagnostics(config.DIAGNOSTICS_CONFIG),
																		   CONFIG(config),
																		   imu(config.IMU_CONFIG),
																		   ultrasonic(config.ULTRASONIC_CONFIG),
																		   conditioner(config.CONDITIONER_CONFIG),
																		   compfilter(config.COMP_FILTER_CONFIG),
																		   pathgenerator(config.PATH_GENERATOR_CONFIG)
{
	past_time = static_cast<float>(getTimestamp()) * configTimestamp::TICKS_TO_SECONDS;
}

void record::Record::reset()
{
	conditioner.reset();
	compfilter.reset();
}

// Updates the path based on walker location
void record::Record::step(Path & path)
{
	float current_time = static_cast<float>(getTimestamp()) * configTimestamp::TICKS_TO_SECONDS;
	diagnostics.update(MESSAGE_FREQ, MESSAGE_FREQ_LENGTH, 1.0f / (current_time - past_time));
	
	while(current_time - past_time < 1.0f / CONFIG.MAX_FREQUENCY) // Make sure things are not running too fast
	{
		current_time = static_cast<float>(getTimestamp()) * configTimestamp::TICKS_TO_SECONDS;
	}
	
	past_time = static_cast<float>(getTimestamp()) * configTimestamp::TICKS_TO_SECONDS;

	Measurement sensor_measurement;
	sensor_measurement.imu_data = imu.getMeasurement();
	sensor_measurement.ultrasonic_data = ultrasonic.getMeasurement();
	States raw_states = conditioner.generateStates(sensor_measurement);
	if(conditioner.stateStatus())
	{
		State output = compfilter.filter(raw_states);
		pathgenerator.step(output, path);
	}
}

bool record::Record::finish(Path & path)
{
	return pathgenerator.finishPath(path);
}

// Deconstructor
record::Record::~Record()
{

}

#ifdef RECORD_UNIT_TEST
#include "../../common/interrupts/include/interrupts.hpp"

int main()
{
	timestampInitialize();
	record::Record record(configRecord::CONFIG_DEFAULT);
	Path path;

	while(1)
	{
		record.step(path);
	}
}

#endif

