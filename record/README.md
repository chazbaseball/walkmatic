# record #
This module is responsible for recording the path a parent traces with the walker.

![](../diagrams/record.png)