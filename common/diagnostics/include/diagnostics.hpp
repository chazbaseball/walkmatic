#ifndef DIAGNOSTICS_H
#define DIAGNOSTICS_H
#include "../../common.hpp"
#include "../../drivers/uart/include/uart.hpp"

namespace diagnostics
{
    // White space characters
	static unsigned char SPACE[1] = {32u};
	static unsigned char NEW_LINE[2] = {13u, 10u};
	static unsigned char TAB[1] = {9u};
    
    // Strings to be used as module names
    static unsigned char PLAY_TITLE[] = "PLAY";
    static unsigned char RECORD_TITLE[] = "RECORD";
    static unsigned char IMU_TITLE[] = "IMU";
    static unsigned char CONDITIONER_TITLE[] = "CONDITIONER";
    static unsigned char COMP_FILTER_TITLE[] = "COMP_FILTER";
    static unsigned char PATH_GENERATOR_TITLE[] = "PATH_GENERATOR";
    static unsigned char CONTROLLER_TITLE[] = "CONTROLLER";
    static unsigned char PLOTTER_TITLE[] = "PLOTTER";
    static unsigned char LCD_TITLE[] = "LCD";
    static unsigned char SERVO_TITLE[] = "SERVO";
    static unsigned char TEST_TITLE[] = "TEST";
    static unsigned char ERROR_TITLE[] = "ERROR";
	static unsigned char I2C_TITLE[] = "I2C";
	static unsigned char SPI_TITLE[] = "SPI";
	static unsigned char MODE_MANAGER_TITLE[] = "MODE_MANAGER";
	static unsigned char ULTRASONIC_TITLE[] = "ULTRASONIC";

    class Diagnostics
    {
        private:
			const configDiagnostics::Config CONFIG;
			uart::Uart DeviceUart;
			unsigned char * module_name;
			uint8_t module_name_length;
			unsigned char ** output;
			void numToString(unsigned char * const output, const float & value);
        public:
            Diagnostics(const configDiagnostics::Config & config);
            void update(unsigned char * const name, const uint8_t & lenName, const float & value);
            ~Diagnostics();
    };
}
#endif

