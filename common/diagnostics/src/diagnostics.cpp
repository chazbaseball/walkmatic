#include "../include/diagnostics.hpp"

// Constructor
diagnostics::Diagnostics::Diagnostics(const configDiagnostics::Config & config) :  CONFIG(config),
																				   DeviceUart(configUart::CONFIG_DIAGNOSTICS)
{
	output = new unsigned char*[10];
	switch(CONFIG.MODULE)
	{
		case PLAY:
			module_name_length = 4u;
			module_name = new unsigned char[module_name_length];
			module_name = PLAY_TITLE;
			break;
		case RECORD:
			module_name_length = 6u;
			module_name = new unsigned char[module_name_length];
			module_name = RECORD_TITLE;
			break;
		case IMU:
			module_name_length = 3u;
			module_name = new unsigned char[module_name_length];
			module_name = IMU_TITLE;
			break;
		case CONDITIONER:
			module_name_length = 11u;
			module_name = new unsigned char[module_name_length];
			module_name = CONDITIONER_TITLE;
			break;
		case COMP_FILTER:
			module_name_length = 11u;
			module_name = new unsigned char[module_name_length];
			module_name = COMP_FILTER_TITLE;
			break;
		case PATH_GENERATOR:
			module_name_length = 14u;
			module_name = new unsigned char[module_name_length];
			module_name = PATH_GENERATOR_TITLE;
			break;
		case CONTROLLER:
			module_name_length = 10u;
			module_name = new unsigned char[module_name_length];
			module_name = CONTROLLER_TITLE;
			break;
		case PLOTTER:
			module_name_length = 7u;
			module_name = new unsigned char[module_name_length];
			module_name = PLOTTER_TITLE;
			break;
		case LCD:
			module_name_length = 3u;
			module_name = new unsigned char[module_name_length];
			module_name = LCD_TITLE;
			break;
		case SERVO:
			module_name_length = 5u;
			module_name = new unsigned char[module_name_length];
			module_name = SERVO_TITLE;
			break;
		case TEST:
			module_name_length = 4u;
			module_name = new unsigned char[module_name_length];
			module_name = TEST_TITLE;	
			break;
		case UART: // Note that diagnostics should not be printed for UART. An infinite loop will occur.
			module_name_length = 5u;
			module_name = new unsigned char[module_name_length];
			module_name = ERROR_TITLE;	
			break;
		case I2C:
			module_name_length = 3u;
			module_name = new unsigned char[module_name_length];
			module_name = I2C_TITLE;
			break;
		case SPI:
			module_name_length = 3u;
			module_name = new unsigned char[module_name_length];
			module_name = SPI_TITLE;
			break;
		case MODE_MANAGER:
			module_name_length = 12u;
			module_name = new unsigned char[module_name_length];
			module_name = MODE_MANAGER_TITLE;
			break;
		case ULTRASONIC:
			module_name_length = 10u;
			module_name = new unsigned char[module_name_length];
			module_name = ULTRASONIC_TITLE;
			break;
		default: 
			break;
	}
}

// Update the diagnostics
void diagnostics::Diagnostics::update(unsigned char * const name, const uint8_t & lenName, const float & value)
{
	if(CONFIG.ENABLE)
	{
		unsigned char number[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		numToString(number, value);
		DeviceUart.send(module_name, module_name_length);
		DeviceUart.send(TAB, 1);
		DeviceUart.send(name, lenName);
		DeviceUart.send(TAB, 1);
		DeviceUart.send(number, 16u);
		DeviceUart.send(NEW_LINE, 2);
	}
}

void diagnostics::Diagnostics::numToString(unsigned char * const output, const float & value)
{
	float input = value;
	float abs_of_input = value;
	bool sign = false;
	
	if(input == 0.0f)
	{
		output[0] = 48u;
		return;
	}
	
	if(input == 1.0f)
	{
		output[0] = 49u;
		return;
	}
	
	if(input < 0)
	{
		sign = true;
		input = -input;
		abs_of_input = -value;
	}
	
	if(input < 100000000 && input > 0.00001f)
	{
		uint8_t positive_decimal_places = 0;
		while(input > 1.0f)
		{
			input /= 10.0f;
			positive_decimal_places++;
		}
		
		if(sign) // If the value is negative, add a negative sign
		{
			positive_decimal_places++;
			output[0] = '-';
		}

		int32_t input_as_int = static_cast<int32_t>(abs_of_input);
		float frac = abs_of_input - static_cast<float>(input_as_int);
		frac *= 100000; // Shift to make fractional part non-fractional
		uint8_t fracDigits = 5; // 5 places for 100000s
		
		unsigned char remainder = 0;
		uint8_t length_of_number = positive_decimal_places + fracDigits + 1u;
		uint8_t from_end = length_of_number - 1u;
		uint8_t start_index = (sign) ? 1u : 0;

		for(uint8_t i = 0; i < length_of_number; i++)
		{
			if(positive_decimal_places - 1 - i >= start_index) // Place non-fractional values
			{
				remainder = input_as_int % 10;
				input_as_int /= 10;
				output[positive_decimal_places - 1 - i] = remainder + 48u; // Convert to ascii character
			}
			
			if(positive_decimal_places - 1 - i == -1) // Place decimal point
			{
				output[i] = '.';
			}
			
			if(positive_decimal_places - 1 - i < -1) // Place fractional values
			{
				remainder = static_cast<int32_t>(frac) % 10;
				frac /= 10;
				output[from_end] = remainder + 48u; // Convert to ascii character
				from_end--;
			}
		}
	}
	else
	{
		output[0] = 'O'; output[1] = 'U'; output[2] = 'T'; output[3] = ' '; output[4] = 'O'; output[5] = 'F'; output[6] = ' '; output[7] = 'R'; output[8] = 'A'; output[9] = 'N'; output[10] = 'G'; output[11] = 'E';
	}
}

// Destructor
diagnostics::Diagnostics::~Diagnostics()
{
	delete output;
	delete module_name;
}

#ifdef DIAGNOSTICS_UNIT_TEST
int main()
{		
	diagnostics::Diagnostics* DeviceDiagnostics = new diagnostics::Diagnostics(PLAY);
	diagnostics::Diagnostics* DeviceDiagnostics1 = new diagnostics::Diagnostics(RECORD);
	unsigned char name[] = "TESTING";
    while (1)
    {
		float val0 = -5.365934f;
		float val1 = 123456;
		DeviceDiagnostics->update(name, 7, val0);
		DeviceDiagnostics1->update(name, 7, val1);
		for(int i = 0; i < 100000; i++);
    }
}
#endif

