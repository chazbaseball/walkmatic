# diagnostics #
This module is responsible for formatting diagnostic messages for the uart driver.

![](../../diagrams/diagnostics.png)