#ifndef PLOTTER_H
#define PLOTTER_H
#include "../../common.hpp"
#include "../../drivers/lcd/include/lcd.hpp"

namespace plotter
{
    class Plotter
    {
        private:
			Path path;
			bool displayArray[configPlotter::ARRAY_SIZE];
			bool walkerArray[configPlotter::WALKER_ARRAY_SIZE];
			lcd::Lcd deviceLcd;
			void plotPath();
			void plotWalker();
        public:
			Plotter();
			void plot();
    };
}

#endif
