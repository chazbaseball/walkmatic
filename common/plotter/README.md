# plotter #
This module is responsible for creating pixel arrays for plotting with the lcd driver. The principal functions include plotting the path and the walker, including its heading.

![](../../diagrams/plotter.png)