#include "../include/compfilter.hpp"

compfilter::CompFilter::CompFilter(const configCompFilter::Config & config) : diagnostics(config.DIAGNOSTICS_CONFIG),
																			  CONFIG(config)
{
	reset();
}

void compfilter::CompFilter::reset()
{
	commanded_curvature = 0.0f;
	position.x_part = 0.0f;
	position.y_part = 0.0f;
	filtered_past_state.reset();
	raw_past_states.high_freq_noise.reset();
	raw_past_states.low_freq_noise.reset();
}

void compfilter::CompFilter::loadRaw(const States & raw_states)
{
	raw.high_freq_noise = raw_states.high_freq_noise;
	raw.low_freq_noise = raw_states.low_freq_noise;
}

void compfilter::CompFilter::loadCommandedCurvature(const float & commanded_curvature)
{
	this->commanded_curvature = commanded_curvature;
}

void compfilter::CompFilter::scale()
{
	// Theta (heading) is always in the world frame. 
	filtered.x *= CONFIG.SCALE_TO_CENTIMETERS;
	filtered.y *= CONFIG.SCALE_TO_CENTIMETERS;
}

TwoDim compfilter::CompFilter::getIncrCurvTraversal(const float & commanded_curvature)
{
	float incr_theta = filtered.theta - filtered_past_state.theta;
	if(incr_theta > utils::trig::PI * 1.5f)  
	{
		incr_theta = 2.0f * utils::trig::PI - filtered.theta + filtered_past_state.theta;
	}
	
	if(incr_theta < -utils::trig::PI * 1.5f)
	{
		incr_theta = -(2.0f * utils::trig::PI - filtered_past_state.theta + filtered.theta);
	}
	
	float turning_radius, arc_length;
	if(CONFIG.CURVATURE_OPTION == configCompFilter::MEASURED)
	{
		turning_radius = (utils::geometry::abs(filtered.curvature) > CONFIG.CURVATURE_THRESH) ? 1.0f / utils::geometry::abs(filtered.curvature) : 0.0f;
		arc_length = utils::geometry::abs(incr_theta) * turning_radius * CONFIG.SCALE_CURVATURE_TRAVERSAL;
	}
	else // if CONFIG.CURVATURE_OPTION == configCompFilter::COMMANDED
	{
		turning_radius = (utils::geometry::abs(commanded_curvature) > 0.001f) ? 1.0f / commanded_curvature : 0.0f;
		arc_length = utils::geometry::abs(incr_theta) * turning_radius;
	}
	
	float direction = incr_theta / 2.0f;
	utils::geometry::TwoDim incr_position_low_freq = utils::geometry::transform(arc_length,
																				0.0f,
																				direction,
																				0.0f,
																				0.0f);
	TwoDim output;
	output.x_part = incr_position_low_freq.x_part;
	output.y_part = incr_position_low_freq.y_part;
	return output;
}

State compfilter::CompFilter::filter(const States & input)
{
	loadRaw(input);
	
	// There are four state variables: theta, curvature, x, and y
	// Theta will not be filtered because the IMU already provides a filtered orientation
	// Curvature will be filtered using a frequency complimentary filter
	// Position (x and y) will be filtered using a curvature complimentary filter
	
	// Create intermediate values for complimentary filter
	State scaled_low_freq_noise = raw.low_freq_noise * CONFIG.ALPHA;
	State scaled_past_low_freq_noise = raw_past_states.high_freq_noise * CONFIG.ALPHA;
	State scaled_past_filtered = filtered_past_state * CONFIG.ALPHA;
	
	// Create filtered state from frequency complimentary filter
	filtered = (raw.high_freq_noise + scaled_low_freq_noise - scaled_past_low_freq_noise + scaled_past_filtered) * (1.0f / (1.0f + CONFIG.ALPHA));
	
	// Create filtered state from curvature complimentary filter
	TwoDim incr_position_high_freq, incr_position_low_freq;
	incr_position_high_freq.x_part = raw.high_freq_noise.x - raw_past_states.high_freq_noise.x;
	incr_position_high_freq.y_part = raw.high_freq_noise.y - raw_past_states.high_freq_noise.y;
	
	incr_position_low_freq = getIncrCurvTraversal(commanded_curvature);
	
	float ang_rate_norm = filtered.ang_rate / CONFIG.CURVATURE_THRESH;
	ang_rate_norm = utils::geometry::abs(ang_rate_norm);
	
	// Saturate ang_rate_norm
	ang_rate_norm = (ang_rate_norm > 1.0f) ? 1.0f : ang_rate_norm;
	
	// If there is a lot of angular rate, use the imu. If there is not a lot of angular rate, use the ultrasonic sensors.
	// This acts closer to a switch
	position.x_part = position.x_part + ang_rate_norm * incr_position_low_freq.x_part + (1.0f - ang_rate_norm) * incr_position_high_freq.x_part;
	position.y_part = position.y_part + ang_rate_norm * incr_position_low_freq.y_part + (1.0f - ang_rate_norm) * incr_position_high_freq.y_part;
	
	// Save current raw and filtered states for next iteration of frequency complimentary filter
	raw_past_states = raw;
	filtered_past_state = filtered;
	
	// Update filtered postion before returning filtered state
	filtered.x = position.x_part;
	filtered.y = position.y_part;
	
	scale(); // Scale to centimeters
	
	diagnostics.update(MESSAGE_X, MESSAGE_X_LENGTH, filtered.x);
	diagnostics.update(MESSAGE_Y, MESSAGE_Y_LENGTH, filtered.y);
	diagnostics.update(MESSAGE_YAW, MESSAGE_YAW_LENGTH, filtered.theta);
	diagnostics.update(MESSAGE_CURV, MESSAGE_CURV_LENGTH, filtered.curvature);
	
	return filtered;
}

compfilter::CompFilter::~CompFilter()
{

}

#ifdef COMP_FILTER_UNIT_TEST
#include "../../drivers/imu/include/imu.hpp"
#include "../../drivers/ultrasonic/include/ultrasonic.hpp"
#include "../../conditioner/include/conditioner.hpp"

int main()
{
	timestampInitialize();
	imu::Imu imuDevice(configImu::CONFIG_DEFAULT);
	ultrasonic::Ultrasonic ultrasonicDevice(configUltrasonic::CONFIG_DEFAULT);
	conditioner::Conditioner conditioner(configConditioner::CONFIG_PLAY);
	compfilter::CompFilter compfilter(configCompFilter::CONFIG_RECORD);
	
	uint8_t name_x[] = "X:";
	uint8_t name_y[] = "Y:";
	uint8_t name_x_raw[] = "X_RAW:";
	uint8_t name_theta[] = "THETA:";
	
	State output;
	while(1)
	{
		Measurement sensor_measurement;
		sensor_measurement.imu_data = imuDevice.getMeasurement();
		sensor_measurement.ultrasonic_data = ultrasonicDevice.getMeasurement();
		States raw_states = conditioner.generateStates(sensor_measurement);
		if(conditioner.stateStatus())
		{
			output = compfilter.filter(raw_states);
		}
	}

}
#endif

