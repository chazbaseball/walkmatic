# ekf
This module is an implementation of an Extender Kalman Filter (EKF). This filter is used for finding the best fit gaussian (in the form of a mean and covariance parameter)
from nonlinear data. 

![](../../diagrams/ekf.png)