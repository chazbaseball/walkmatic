#ifndef COMPFILTER_H
#define COMPFILTER_H
#include "../../common.hpp"
#include "../../diagnostics/include/diagnostics.hpp"

namespace compfilter
{
	// Diagnostic Messages
	static uint8_t MESSAGE_X[] = "STATE_X";
	static const uint8_t MESSAGE_X_LENGTH = 7u;
	static uint8_t MESSAGE_Y[] = "STATE_Y";
	static const uint8_t MESSAGE_Y_LENGTH = 7u;
	static uint8_t MESSAGE_YAW[] = "STATE_YAW";
	static const uint8_t MESSAGE_YAW_LENGTH = 9u;
	static uint8_t MESSAGE_CURV[] = "STATE_CURVATURE";
	static const uint8_t MESSAGE_CURV_LENGTH = 15u;

    class CompFilter
    {
        private:
			diagnostics::Diagnostics diagnostics;
			const configCompFilter::Config CONFIG;
			TwoDim position;
			float commanded_curvature;
			float past_commanded_curvature;
			State filtered_past_state;
			States raw_past_states;
			States raw;
			State filtered;
			State filtered_world_frame;
			void scale();
			void loadRaw(const States & raw_states);
			TwoDim getIncrCurvTraversal(const float & commanded_curvature);
        public:
            CompFilter(const configCompFilter::Config & config);
            State filter(const States & input);
			void loadCommandedCurvature(const float & commanded_curvature);
			void reset();
            ~CompFilter();
    };
}
#endif

