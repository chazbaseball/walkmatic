#ifndef CONDITIONER_H
#define CONDITIONER_H
#include "../../common.hpp"
#include "../../diagnostics/include/diagnostics.hpp"

namespace conditioner
{
	enum MeasType
	{
		LINEAR_POSITION,
		LINEAR_VELOCITY,
		LINEAR_ACCELERATION,
		ANGULAR_POSITION,
		ANGULAR_VELOCITY,
		ANGULAR_ACCELERATION
	};
	
	struct SystemMotion
	{
		uint64_t timestamp_acc;
		uint64_t timestamp_gyro;
		TwoDim linear_position;
		TwoDim linear_velocity;
		TwoDim linear_acceleration;
		float angular_acceleration;
		float angular_velocity;
		float angular_position;
		SystemMotion(){timestamp_acc = 0u;
					   timestamp_acc = 0u;
					   linear_position.x_part = 0u;
					   linear_position.y_part = 0u;
					   linear_velocity.x_part = 0u;
					   linear_velocity.y_part = 0u;
			           linear_acceleration.x_part = 0u;
					   linear_acceleration.y_part = 0u;
					   angular_acceleration = 0.0f;
					   angular_velocity = 0.0f;
					   angular_position = 0.0f;}
		void reset(){timestamp_acc = 0u;
					   timestamp_acc = 0u;
					   linear_position.x_part = 0u;
					   linear_position.y_part = 0u;
					   linear_velocity.x_part = 0u;
					   linear_velocity.y_part = 0u;
			           linear_acceleration.x_part = 0u;
					   linear_acceleration.y_part = 0u;
					   angular_acceleration = 0.0f;
					   angular_velocity = 0.0f;
					   angular_position = 0.0f;}
	};
	
	const uint32_t MIN_NUM_OF_MEASUREMENTS = 4u;
	const uint32_t NUM_OF_BIAS_MEASUREMENTS = 20u;
	
	static uint8_t MESSAGE_X_LOW_FREQ[] = "X LOW FREQ";
	static uint8_t MESSAGE_X_LOW_FREQ_LENGTH = 10u;
	static uint8_t MESSAGE_Y_LOW_FREQ[] = "Y LOW FREQ";
	static uint8_t MESSAGE_Y_LOW_FREQ_LENGTH = 10u;
	static uint8_t MESSAGE_X_HIGH_FREQ[] = "X HIGH FREQ";
	static uint8_t MESSAGE_X_HIGH_FREQ_LENGTH = 10u;
	static uint8_t MESSAGE_Y_HIGH_FREQ[] = "Y HIGH FREQ";
	static uint8_t MESSAGE_Y_HIGH_FREQ_LENGTH = 10u;
	static uint8_t MESSAGE_ULTRASONIC_FILTERED[] = "ULTRASONIC FILTERED";
	static uint8_t MESSAGE_ULTRASONIC_FILTERED_LENGTH = 19u;
	
	class BinaryBayesFilter
	{
		private:
			float past_state_prob[2]; // Positive probability is always listed first
			float measurement_prob[4];
			float state_transition_prob[4];
		public:
			const configBinaryBayesFilter::Config CONFIG;
			BinaryBayesFilter(const configBinaryBayesFilter::Config & config);
			void setPastStateProb(const float pos_state, const float neg_state);
			void setStateTransitionProb(const float & pos_to_pos, const float & neg_to_pos, const float & pos_to_neg, const float & neg_to_neg);
			void setMeasurementProb(const float & true_positive, const float & false_positive, const float & true_negative, const float & false_negative);
			float iterate(bool measurement);
			float getCurrentPosStateProb();
			float getCurrentNegStateProb();
			void reset();
			~BinaryBayesFilter();
	};
	
    class Conditioner
    {
        private:
			diagnostics::Diagnostics diagnostics;
			const configConditioner::Config CONFIG;
			uint64_t num_of_measurements;
		
			// Storage of past data
			SystemMotion past_motion[MIN_NUM_OF_MEASUREMENTS];
			SystemMotion past_input_for_filters;
			UltrasonicData past_ultrasonic_data[MIN_NUM_OF_MEASUREMENTS];
			TwoDim past_position_ultrasonic;
			utils::geometry::TwoDim past_ultrasonic_point;

			// Filtering
			BinaryBayesFilter bbf;
			bool iterateBbf();
			float highPassFilter(const float & current_input, const float & past_output, const float & past_input);
			float lowPassFilter(const float & current_input, const float & past_output);
			float slamToZero(const float & input, const float & lower_bound_to_zero, const float & upper_bound_to_zero);
			float ultrasonicSpeedFilter(const UltrasonicData & input);
		
			// IMU operations
			void integrate(MeasType type);
			void differentiate(MeasType type);
			float normalizeAngularDistance(float & unnormalized_distance);
			
			// Ultrasonic operations
			TwoDim processUltrasonicData();
	
			// General Functions
			void loadMeasurement(const Measurement & current_measurement);
			State generateInitialState();
			State generateStateFromHighFreqNoiseMeasurement();
			State generateStateFromLowFreqNoiseMeasurement();
			void transformAccToWorld(const ImuData & input_data);
			void transformImuToWalker();
			float dummy;
        public:
            Conditioner(const configConditioner::Config & config);
			bool stateStatus();
			States generateStates(const Measurement & current_measurement);
			void reset();
            ~Conditioner();
    };
}
#endif 
