# signalgenerator 
This module is responsible for converting raw data (linear acceleration and angular velocity) to signals and states needed for the EKF: 
a state vector (x,y location and heading angle) along with a control signal (linear velocity and angular acceleration).

![](../../diagrams/signalgenerator.png)
