#include "../include/conditioner.hpp"

conditioner::Conditioner::Conditioner(const configConditioner::Config & config) : diagnostics(config.DIAGNOSTICS_CONFIG),
																				  CONFIG(config),
																				  bbf(configBinaryBayesFilter::ZERO_VELOCITY)
{
	reset();
}

void conditioner::Conditioner::reset()
{
	for(uint32_t i = 0; i < MIN_NUM_OF_MEASUREMENTS; i++)
	{
		past_motion[i] = SystemMotion();
		past_ultrasonic_data[i].distance = 0.0f;
	}
	
	dummy = 0.0f;
	num_of_measurements = 0u;
	
	// Initialize ultrasonic storage
	past_position_ultrasonic.x_part = 0.0f;
	past_position_ultrasonic.y_part = 0.0f;
	
	past_ultrasonic_point.x_part = 0.0f;
	past_ultrasonic_point.y_part = 0.0f;
	bbf.reset();
}

void conditioner::Conditioner::integrate(MeasType type)
{
	// Use Trapezoidal Rule: Area of Trapezoid = h * (a + b) / 2
	float dt_acc = static_cast<float>(past_motion[0].timestamp_acc - past_motion[1].timestamp_acc) * configTimestamp::TICKS_TO_SECONDS;
	float dt_gyro = static_cast<float>(past_motion[0].timestamp_gyro - past_motion[1].timestamp_gyro) * configTimestamp::TICKS_TO_SECONDS;
	float two = 2.0f;
	switch(type)
	{
		case LINEAR_VELOCITY:
			if(num_of_measurements > MIN_NUM_OF_MEASUREMENTS - 2u)
			{
				// Compute integration
				past_motion[0].linear_position = past_motion[0].linear_velocity + past_motion[1].linear_velocity;
				past_motion[0].linear_position *= dt_acc;
				past_motion[0].linear_position /= two;
				past_motion[0].linear_position += past_motion[1].linear_position;
			}
			break;
		case LINEAR_ACCELERATION:
			if(num_of_measurements > MIN_NUM_OF_MEASUREMENTS - 3u)
			{
				// Compute integration
				past_motion[0].linear_velocity = past_motion[0].linear_acceleration + past_motion[1].linear_acceleration;
				past_motion[0].linear_velocity *= dt_acc;
				past_motion[0].linear_velocity /= two;
				past_motion[0].linear_velocity += past_motion[1].linear_velocity;
				
				//Binary Bayes Filter to do a zero-velocity update
				if(iterateBbf()) 
				{
					past_motion[0].linear_velocity.x_part = 0.0f;
					past_motion[0].linear_velocity.y_part = 0.0f;
				}
			}
			break;
		case ANGULAR_VELOCITY:
			if(CONFIG.MODE == configConditioner::IMU_MODE && num_of_measurements > MIN_NUM_OF_MEASUREMENTS - 2u || CONFIG.MODE == configConditioner::AMG_MODE && num_of_measurements > MIN_NUM_OF_MEASUREMENTS - 3u)
			{
				// Compute integration
				past_motion[0].angular_position = past_motion[0].angular_velocity + past_motion[1].angular_velocity;
				past_motion[0].angular_position *= dt_gyro;
				past_motion[0].angular_position /= 2.0f;
				past_motion[0].angular_position += past_motion[1].angular_position;
			}
			break;
		case ANGULAR_ACCELERATION:
			if(num_of_measurements > MIN_NUM_OF_MEASUREMENTS - 2u)
			{
				// Compute integration
				past_motion[0].angular_velocity = past_motion[0].angular_acceleration + past_motion[1].angular_acceleration;
				past_motion[0].angular_velocity *= dt_gyro;
				past_motion[0].angular_velocity /= 2.0f;
				past_motion[0].angular_velocity += past_motion[1].angular_velocity;
			}
			break;
		default:
			break;
	}
}

float conditioner::Conditioner::highPassFilter(const float & current_input, const float & past_output, const float & past_input)
{
	return CONFIG.ALPHA_HIGH_PASS * (current_input - past_input + past_output) / (1.0f + CONFIG.ALPHA_HIGH_PASS);
}

float conditioner::Conditioner::lowPassFilter(const float & current_input, const float & past_output)
{
	return (current_input + CONFIG.ALPHA_LOW_PASS * past_output) / (1.0f + CONFIG.ALPHA_LOW_PASS);
}

void conditioner::Conditioner::differentiate(MeasType type)
{
	float dt_acc = static_cast<float>(past_motion[0].timestamp_acc - past_motion[1].timestamp_acc) * configTimestamp::TICKS_TO_SECONDS;
	float dt_gyro = static_cast<float>(past_motion[0].timestamp_gyro - past_motion[1].timestamp_gyro) * configTimestamp::TICKS_TO_SECONDS;
	switch(type)
	{
		case LINEAR_POSITION:
			if(num_of_measurements > MIN_NUM_OF_MEASUREMENTS - 1u)
			{
				// Compute differentiation
				past_motion[0].linear_velocity = past_motion[0].linear_position - past_motion[1].linear_position;
				past_motion[0].linear_velocity /= dt_acc;
			}
			break;
		case LINEAR_VELOCITY:
			if(num_of_measurements > MIN_NUM_OF_MEASUREMENTS - 2u)
			{
				// Compute differentiation
				past_motion[0].linear_acceleration = past_motion[0].linear_velocity - past_motion[1].linear_velocity;
				past_motion[0].linear_acceleration /= dt_acc;
			}
			break;
		case ANGULAR_POSITION:
			if(CONFIG.MODE == configConditioner::IMU_MODE && num_of_measurements > MIN_NUM_OF_MEASUREMENTS - 3u || CONFIG.MODE == configConditioner::AMG_MODE && num_of_measurements > MIN_NUM_OF_MEASUREMENTS - 2u)
			{
				// Compute differentiation
				past_motion[0].angular_velocity = past_motion[0].angular_position - past_motion[1].angular_position;
				past_motion[0].angular_velocity = normalizeAngularDistance(past_motion[0].angular_velocity);
				past_motion[0].angular_velocity /= dt_gyro;
			}
			break;
		case ANGULAR_VELOCITY:
			if(CONFIG.MODE == configConditioner::IMU_MODE && num_of_measurements > 2u || CONFIG.MODE == configConditioner::AMG_MODE && num_of_measurements > 1u)
			{
				// Compute differentiation
				past_motion[0].angular_acceleration = past_motion[0].angular_velocity - past_motion[1].angular_velocity;
				past_motion[0].angular_acceleration *= dt_gyro;
			}
			break;
		default:
			break;
	}
}

float conditioner::Conditioner::normalizeAngularDistance(float & unnormalized_distance)
{
	// If values are not valid
	if(unnormalized_distance > 2.0f * utils::trig::PI || unnormalized_distance < -2.0f * utils::trig::PI)
	{
		unnormalized_distance = 0.0f;
	}
	
	// Use the smallest angular velocity possible
	if(unnormalized_distance > utils::trig::PI)
	{
		return 2.0f * utils::trig::PI - unnormalized_distance;
	}
	
	if(unnormalized_distance < -utils::trig::PI)
	{
		return -2.0f * utils::trig::PI + unnormalized_distance;
	}
	
	return unnormalized_distance;
}

bool conditioner::Conditioner::iterateBbf()
{
	// Do sensor fusion with a binary bayes filter to determine when vel = 0
	bool measurement;
	bbf.setMeasurementProb(0.65f, 0.35f, 0.35f, 0.65f); // when x acceleration is 0
	measurement = (past_motion[0].linear_acceleration.x_part) ? false : true;
	bbf.iterate(measurement);
	bbf.setMeasurementProb(0.9f, 0.1f, 0.1f, 0.9f); // when angular velocity is 0
	measurement = (past_motion[0].angular_velocity) ? false : true;
	bbf.iterate(measurement);
	bbf.setMeasurementProb(0.65f, 0.35f, 0.35f, 0.65f); // when y acceleration is 0
	measurement = (past_motion[0].linear_acceleration.y_part) ? false : true;
	bbf.iterate(measurement);
	float prob = bbf.getCurrentPosStateProb();
	
	uint8_t name_zero_vel_prob[] = "ZERO_VEL_PROB";
	diagnostics.update(name_zero_vel_prob, 13u, prob);

	if(prob > CONFIG.BBF_ZERO_VEL_PROB_THRESH)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void conditioner::Conditioner::transformAccToWorld(const ImuData & input_data)
{
	float angular_position = past_motion[0].angular_position;
	float total_rotation = angular_position + configTransform::imu_to_walker.yaw;
	
	utils::geometry::TwoDim transformed_acc = utils::geometry::transform(input_data.x_acc,
																		 input_data.y_acc,
																		 total_rotation,
																		 0.0f,
																		 0.0f);
	past_motion[0].linear_acceleration.x_part = transformed_acc.x_part;
	past_motion[0].linear_acceleration.y_part = transformed_acc.y_part;
}

void conditioner::Conditioner::transformImuToWalker()
{
	float incr_angular_position = past_motion[0].angular_position - past_motion[1].angular_position;
	utils::geometry::TwoDim new_walker_location = utils::geometry::transform(configTransform::imu_to_walker.x,
																			 configTransform::imu_to_walker.y,
																			 incr_angular_position,
																			 0.0f,
																		     0.0f);
	float incr_x = new_walker_location.x_part - configTransform::imu_to_walker.x;
	float incr_y = new_walker_location.y_part - configTransform::imu_to_walker.y;
	
	utils::geometry::TwoDim incr = utils::geometry::transform(incr_x,
															  incr_y,
															  past_motion[0].angular_position,
															  0.0f,
															  0.0f);
	
	uint8_t name_x[] = "INCR_X_FROM_ROT";
	uint8_t name_y[] = "INCR_Y_FROM_ROT";
	diagnostics.update(name_x, 15u, incr.x_part);
	diagnostics.update(name_y, 15u, incr.y_part);
	
	past_motion[0].linear_position.x_part += incr.x_part;
	past_motion[0].linear_position.y_part += incr.y_part;
}

State conditioner::Conditioner::generateInitialState()
{
	State dummy = {
		0.0f,
		0.0f,
		0.0f,
		0.0f
	};
	return dummy;
}

void conditioner::Conditioner::loadMeasurement(const Measurement & current_measurement)
{
	for(int32_t i = MIN_NUM_OF_MEASUREMENTS - 2; i > -1; i--) // Shift buffer so most recent measurement is at index = 0
	{
		past_motion[i + 1] = past_motion[i];
		past_ultrasonic_data[i + 1] = past_ultrasonic_data[i];
	}
	
	// Read in ultrasonic data
	past_ultrasonic_data[0].distance = ultrasonicSpeedFilter(current_measurement.ultrasonic_data);
	past_ultrasonic_data[0].timestamp = current_measurement.ultrasonic_data.timestamp;

	// Depending on mode, load angular data from imu
	if(CONFIG.MODE == configConditioner::AMG_MODE)
	{
		past_motion[0].angular_velocity = current_measurement.imu_data.z_rot_vel;
		integrate(ANGULAR_VELOCITY);
	}
	else
	{
		// Make sure yaw follows the right hand rule
		past_motion[0].angular_position = 2.0f * utils::trig::PI - current_measurement.imu_data.heading;
	}
	
	// Read in imu data
	past_motion[0].timestamp_acc = current_measurement.imu_data.timestamp_acc;
	past_motion[0].timestamp_gyro = current_measurement.imu_data.timestamp_gyro;
	
	// Transform acceleration vectors from the imu to the world rotational frame
	transformAccToWorld(current_measurement.imu_data); 
	
	// Perform high-pass filtering on acceleration data
	float past_input = past_motion[0].linear_acceleration.y_part;
	past_motion[0].linear_acceleration.y_part = CONFIG.HIGH_PASS_GAIN * slamToZero(highPassFilter(past_motion[0].linear_acceleration.y_part, past_motion[1].linear_acceleration.y_part, past_input_for_filters.linear_acceleration.y_part), -0.0001f, 0.0001f);
	past_input_for_filters.linear_acceleration.y_part = past_input;
	
	// Perform high-pass filtering on acceleration data
	past_input = past_motion[0].linear_acceleration.x_part;
	past_motion[0].linear_acceleration.x_part = CONFIG.HIGH_PASS_GAIN * slamToZero(highPassFilter(past_motion[0].linear_acceleration.x_part, past_motion[1].linear_acceleration.x_part, past_input_for_filters.linear_acceleration.x_part), -0.0001f, 0.0001f);
	past_input_for_filters.linear_acceleration.x_part = past_input;
	
	diagnostics.update(MESSAGE_X_LOW_FREQ, MESSAGE_X_LOW_FREQ_LENGTH, current_measurement.imu_data.x_acc);
	diagnostics.update(MESSAGE_Y_LOW_FREQ, MESSAGE_Y_LOW_FREQ_LENGTH, current_measurement.imu_data.y_acc);
	
	// Due to displacement between the imu and walker, a angular rotation in the imu frame also creates a translation
	transformImuToWalker(); 
	
	++num_of_measurements;
}

float conditioner::Conditioner::ultrasonicSpeedFilter(const UltrasonicData & input)
{
	bool not_first = past_ultrasonic_data[1].distance != 0.0f;
	float elapsed_s = static_cast<float>(input.timestamp - past_ultrasonic_data[1].timestamp) * configTimestamp::TICKS_TO_SECONDS;
	float lateral_speed = utils::geometry::abs(input.distance - past_ultrasonic_data[1].distance) / elapsed_s;
	lateral_speed *= 0.001f; // convert to meters / second from millimeters / second
	bool too_fast = utils::geometry::abs(lateral_speed) > CONFIG.MAX_SPEED;
	if(input.distance < 0.0f || (not_first && too_fast)) // If ultrasonic measurement is invalid
	{
		return past_ultrasonic_data[1].distance;
	}
	else
	{
		return input.distance;
	}
}

TwoDim conditioner::Conditioner::processUltrasonicData()
{
	// Median Filter on ultrasonic data
	float ultrasonic_1_distance = utils::regression::median(past_ultrasonic_data[0].distance,
															past_ultrasonic_data[1].distance,
															past_ultrasonic_data[2].distance);
	
	diagnostics.update(MESSAGE_ULTRASONIC_FILTERED, MESSAGE_ULTRASONIC_FILTERED_LENGTH, ultrasonic_1_distance);

	// Transform ultrasonic data to world
	float angular_position = past_motion[0].angular_position;
	float total_rotation = angular_position + configTransform::walker_to_ultrasonic.yaw;

	utils::geometry::TwoDim point = utils::geometry::transform(ultrasonic_1_distance, 
															   0.0f, 
															   total_rotation,
															   0.0f, 
															   0.0f);
	// Ensure the system starts at position 0
	if(past_ultrasonic_point.x_part == 0.0f)
	{
		past_ultrasonic_point.x_part = point.x_part;
		past_ultrasonic_point.y_part = point.y_part;
	}
	
	// Create position measurement solely based on ultrasonic data
	past_position_ultrasonic.x_part += past_ultrasonic_point.x_part - point.x_part; 
	past_position_ultrasonic.y_part += past_ultrasonic_point.y_part - point.y_part;
	
	past_ultrasonic_point = point; // Update past point
	
	return past_position_ultrasonic;
}

State conditioner::Conditioner::generateStateFromHighFreqNoiseMeasurement()
{
	State output;
	switch(CONFIG.DISPLACEMENT_OPTION_HIGH_FREQ_NOISE)
	{
		case configConditioner::DisplacementCalculation::INTEGRATE_TWICE_ACC:
			integrate(LINEAR_ACCELERATION);
			integrate(LINEAR_VELOCITY);
			output.x = past_motion[0].linear_position.x_part * 100.0f; // Scale to centimeters
			output.y = past_motion[0].linear_position.y_part * 100.0f;
			break;
		case configConditioner::DisplacementCalculation::ULTRASONIC:
			TwoDim coord = processUltrasonicData();
			output.x = coord.x_part * 0.1f; // Scale to centimeters
			output.y = coord.y_part * 0.1f;
			break;
	}
	
	switch(CONFIG.CURVATURE_OPTION_HIGH_FREQ_NOISE)
	{
		case configConditioner::CurvatureCalculation::DIFF_GYRO:
			if(CONFIG.MODE == configConditioner::AMG_MODE)
			{
				differentiate(ANGULAR_VELOCITY);
			}
			else
			{
				differentiate(ANGULAR_POSITION);
				differentiate(ANGULAR_VELOCITY);
			}
			output.theta = past_motion[0].angular_position;
			output.ang_rate = past_motion[0].angular_velocity;
			
			if(past_motion[0].linear_acceleration.x_part < CONFIG.DIVIDE_BY_ZERO_THRESH)
			{
				output.curvature = 0.0f;
			}
			else
			{
				output.curvature = past_motion[0].angular_acceleration / utils::geometry::mag(past_motion[0].linear_acceleration.x_part, past_motion[0].linear_acceleration.y_part);
			}
			break;
		case configConditioner::CurvatureCalculation::INTEGRATE_ACC:
			if(CONFIG.MODE == configConditioner::AMG_MODE)
			{
				integrate(LINEAR_ACCELERATION);
			}
			else
			{
				integrate(LINEAR_ACCELERATION);	
				differentiate(ANGULAR_POSITION);
			}
			output.theta = past_motion[0].angular_position;
			output.ang_rate = past_motion[0].angular_velocity;
			
			if(past_motion[0].linear_velocity.x_part < CONFIG.DIVIDE_BY_ZERO_THRESH)
			{
				output.curvature = 0.0f;
			}
			else
			{
				output.curvature = past_motion[0].angular_velocity / utils::geometry::mag(past_motion[0].linear_velocity.x_part, past_motion[0].linear_velocity.y_part);
			}
			break;
		default:
			break;
	}
	output.time = static_cast<float>(past_motion[0].timestamp_acc) * configTimestamp::TICKS_TO_SECONDS;
	return output;
}

State conditioner::Conditioner::generateStateFromLowFreqNoiseMeasurement()
{
	State output;
	switch(CONFIG.DISPLACEMENT_OPTION_LOW_FREQ_NOISE)
	{
		case configConditioner::DisplacementCalculation::INTEGRATE_TWICE_ACC:
			integrate(LINEAR_ACCELERATION);
			integrate(LINEAR_VELOCITY);
			output.x = past_motion[0].linear_position.x_part * 100.0f; // Scale to centimeters
			output.y = past_motion[0].linear_position.y_part * 100.0f;
			break;
		case configConditioner::DisplacementCalculation::ULTRASONIC:
			TwoDim coord = processUltrasonicData();
			output.x = coord.x_part * 0.1f; // Scale to centimeters
			output.y = coord.y_part * 0.1f;
			break;
	}
	
	switch(CONFIG.CURVATURE_OPTION_LOW_FREQ_NOISE)
	{
		case configConditioner::CurvatureCalculation::DIFF_GYRO:
			if(CONFIG.MODE == configConditioner::AMG_MODE)
			{
				differentiate(ANGULAR_VELOCITY);
			}
			else
			{
				differentiate(ANGULAR_POSITION);
				differentiate(ANGULAR_VELOCITY);
			}
			output.theta = past_motion[0].angular_position;
			output.ang_rate = past_motion[0].angular_velocity;
			
			if(past_motion[0].linear_acceleration.x_part < CONFIG.DIVIDE_BY_ZERO_THRESH)
			{
				output.curvature = 0.0f;
			}
			else
			{
				output.curvature = past_motion[0].angular_acceleration / utils::geometry::mag(past_motion[0].linear_acceleration.x_part, past_motion[0].linear_acceleration.y_part);
			}
			break;
		case configConditioner::CurvatureCalculation::INTEGRATE_ACC:
			if(CONFIG.MODE == configConditioner::AMG_MODE)
			{
				integrate(LINEAR_ACCELERATION);
			}
			else
			{
				integrate(LINEAR_ACCELERATION);	
				differentiate(ANGULAR_POSITION);
			}
			output.theta = past_motion[0].angular_position;
			output.ang_rate = past_motion[0].angular_velocity;
			
			if(past_motion[0].linear_velocity.x_part < CONFIG.DIVIDE_BY_ZERO_THRESH)
			{
				output.curvature = 0.0f;
			}
			else
			{
				output.curvature = past_motion[0].angular_velocity / utils::geometry::mag(past_motion[0].linear_velocity.x_part, past_motion[0].linear_velocity.y_part);
			}
			break;
		default:
			break;
	}
	output.time = static_cast<float>(past_motion[0].timestamp_acc) * configTimestamp::TICKS_TO_SECONDS;
	return output;
}

States conditioner::Conditioner::generateStates(const Measurement & current_measurement)
{
	States output;
	loadMeasurement(current_measurement);
	output.high_freq_noise = generateStateFromHighFreqNoiseMeasurement();
	output.low_freq_noise = generateStateFromLowFreqNoiseMeasurement();
	
	// Output diagnostics information
	diagnostics.update(MESSAGE_X_LOW_FREQ, MESSAGE_X_LOW_FREQ_LENGTH, output.low_freq_noise.x);
	diagnostics.update(MESSAGE_Y_LOW_FREQ, MESSAGE_Y_LOW_FREQ_LENGTH, output.low_freq_noise.y);
	diagnostics.update(MESSAGE_X_HIGH_FREQ, MESSAGE_X_HIGH_FREQ_LENGTH, output.high_freq_noise.x);
	diagnostics.update(MESSAGE_Y_HIGH_FREQ, MESSAGE_Y_HIGH_FREQ_LENGTH, output.high_freq_noise.y);
	return output;
}

float conditioner::Conditioner::slamToZero(const float & input, const float & lower_bound_to_zero, const float & upper_bound_to_zero)
{
	return (input > lower_bound_to_zero && input < upper_bound_to_zero) ? 0.0f : input;
}

bool conditioner::Conditioner::stateStatus()
{
	return num_of_measurements >= MIN_NUM_OF_MEASUREMENTS;
}

conditioner::Conditioner::~Conditioner()
{

}

#ifdef CONDITIONER_UNIT_TEST
#include "../../drivers/imu/include/imu.hpp"
#include "../../drivers/ultrasonic/include/ultrasonic.hpp"

int main()
{
	timestampInitialize();
	imu::Imu imuDevice(configImu::CONFIG_DEFAULT);
	ultrasonic::Ultrasonic ultrasonicDevice(configUltrasonic::CONFIG_DEFAULT);
	conditioner::Conditioner conditioner(configConditioner::CONFIG_PLAY);
	diagnostics::Diagnostics diagnostics(CONDITIONER);
	uint8_t name_imu_x[] = "POS_X_IMU";
	uint8_t name_imu_y[] = "POS_Y_IMU";
	uint8_t name_ultrasonic_x[] = "POS_X_ULTRASONIC";
	uint8_t name_ultrasonic_y[] = "POS_Y_ULTRASONIC";
	uint8_t name_theta[] = "THETA";
	uint8_t name_curv[] = "CURVATURE";
	uint8_t name_1[] = "ACC_X_IMU";
	uint8_t ultrasonic[] = "POS_X_ULT";
	uint8_t time[] = "TIME";
	float test[5000];
	Measurement current;
	float dummy = 0.0f;
	while(1)
	{
		current.imu_data = imuDevice.getMeasurement();
		current.ultrasonic_data = ultrasonicDevice.getMeasurement();
		
		States current_states = conditioner.generateStates(current);
		if(conditioner.stateStatus())
		{
			// diagnostics.update(name_imu, 9u, current_states.low_freq_noise.x);
			//diagnostics.update(name_imu_x, 16u, current_states.low_freq_noise.x);
			//diagnostics.update(name_imu_y, 16u, current_states.low_freq_noise.y);
			diagnostics.update(name_curv, 9u, current_states.low_freq_noise.curvature);
			//diagnostics.update(name_theta, 5u, current_states.low_freq_noise.theta);
		}
		
		float sec = static_cast<float>(current.imu_data.timestamp_acc) * configTimestamp::TICKS_TO_SECONDS;
		// diagnostics.update(time, 4u, sec);
	}
}

#endif

