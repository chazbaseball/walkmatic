#include "../include/conditioner.hpp"

conditioner::BinaryBayesFilter::BinaryBayesFilter(const configBinaryBayesFilter::Config & config) : CONFIG(config)
{
	reset();
}

void conditioner::BinaryBayesFilter::reset()
{
	setPastStateProb(CONFIG.POS_INITIAL_STATE_PROB, CONFIG.NEG_INITIAL_STATE_PROB);
	setStateTransitionProb(CONFIG.POS_TO_POS_TRANSITION_PROB, CONFIG.NEG_TO_POS_TRANSITION_PROB, CONFIG.POS_TO_NEG_TRANSITION_PROB, CONFIG.NEG_TO_NEG_TRANSITION_PROB);
	setMeasurementProb(0.5f, 0.5f, 0.5f, 0.5f);
}
	
conditioner::BinaryBayesFilter::~BinaryBayesFilter(){}
	
void conditioner::BinaryBayesFilter::setPastStateProb(const float pos_state, const float neg_state)
{
	past_state_prob[0] = pos_state;
	past_state_prob[1] = neg_state;
}

void conditioner::BinaryBayesFilter::setStateTransitionProb(const float & pos_to_pos, const float & neg_to_pos, const float & pos_to_neg, const float & neg_to_neg)
{
	// Conditional probabilites of transition to positive state (or true state)
	state_transition_prob[0] = pos_to_pos;
	state_transition_prob[1] = neg_to_pos;
	
	// Conditional probabilites of transition to negative state (or false state)
	state_transition_prob[2] = pos_to_neg;
	state_transition_prob[3] = neg_to_neg;
}
void conditioner::BinaryBayesFilter::setMeasurementProb(const float & true_positive, const float & false_positive, const float & true_negative, const float & false_negative)
{
	// Conditional measurement probabilites given a ground truth positive state
	measurement_prob[0] = true_positive;
	measurement_prob[1] = false_positive;
	
	// Conditional measurement probabilites given a ground truth negative state
	measurement_prob[2] = true_negative;
	measurement_prob[3] = false_negative;
}

float conditioner::BinaryBayesFilter::getCurrentPosStateProb()
{
	return past_state_prob[0];
}

float conditioner::BinaryBayesFilter::getCurrentNegStateProb()
{
	return past_state_prob[1];
}
			
float conditioner::BinaryBayesFilter::iterate(bool measurement)
{
	// Prediction step using past state probabilites
	float prediction_pos_belief = state_transition_prob[0] * past_state_prob[0] + state_transition_prob[1] * past_state_prob[1];
	float prediction_neg_belief = state_transition_prob[2] * past_state_prob[0] + state_transition_prob[3] * past_state_prob[1];
	
	float unnormalized_pos_state;
	float unnormalized_neg_state;
	
	// Measurement update step using current measurement
	if(measurement)
	{
		unnormalized_pos_state = measurement_prob[0] * prediction_pos_belief;
		unnormalized_neg_state = measurement_prob[2] * prediction_neg_belief;
	}
	else
	{
		unnormalized_pos_state = measurement_prob[1] * prediction_pos_belief;
		unnormalized_neg_state = measurement_prob[3] * prediction_neg_belief;
	}
	
	float normalizer = 1.0f / (unnormalized_pos_state + unnormalized_neg_state); // Probabilites must add to 1
	
	float normalized_pos_state = normalizer * unnormalized_pos_state;
	float normalized_neg_state = normalizer * unnormalized_neg_state;
	
	//setPastStateProb(normalized_pos_state, normalized_neg_state);
	past_state_prob[0] = normalized_pos_state;
	past_state_prob[1] = normalized_neg_state;
	return normalized_pos_state;
}

#ifdef BBF_UNIT_TEST

int main()
{
	conditioner::BinaryBayesFilter bbf(configBinaryBayesFilter::ZERO_VELOCITY);
	diagnostics::Diagnostics diagnostics(TEST);
	uint64_t count = 0u;
	uint8_t name[] = "PROB_POS";
	uint8_t name_1[] = "PROB_NEG";
	while(1)
	{
		
		bbf.setMeasurementProb(0.65f, 0.35f, 0.35f, 0.65f); // when angular velocity is 0
		float current_pos_prob = bbf.iterate(false);
		float current_neg_prob = bbf.getCurrentNegStateProb();
		//diagnostics.update(name, 8u, current_pos_prob);
		//diagnostics.update(name_1, 8u, current_neg_prob);
		bbf.setMeasurementProb(0.9f, 0.1f, 0.1f, 0.9f); // when accelerometer is 0
		current_pos_prob = bbf.iterate(true);
		current_neg_prob = bbf.getCurrentNegStateProb();
		//diagnostics.update(name, 8u, current_pos_prob);
		//diagnostics.update(name_1, 8u, current_neg_prob);
		bbf.setMeasurementProb(0.65f, 0.35f, 0.35f, 0.65f); // when accelerometer is 0
		current_pos_prob = bbf.iterate(false);
		current_neg_prob = bbf.getCurrentNegStateProb();
		diagnostics.update(name, 8u, current_pos_prob);
		diagnostics.update(name_1, 8u, current_neg_prob);
		count++;
	}
}

#endif