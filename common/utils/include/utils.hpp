#ifndef UTILS_HPP
#define UTILS_HPP
#include "stm32f4xx.h"

/*********************************/
// UTILS UNIT TESTS
// *******************************
// Define one of the following preprocessing
// variables to determine which unit tests in this
// directory should be ran
// *******************************
// UTILS_GEO_UNIT_TEST
// UTILS_TRIG_UNIT_TEST
//*********/
#define NONE

namespace utils
{
    namespace trig
    {
        const float PI = 3.1415926f;
        enum MEAS
        {
            RADIANS,
            DEGREES
        };

        float sin(const float & theta, MEAS type);
        float cos(const float & theta, MEAS type);
        float acos(const float & theta, MEAS type);
        float acosLin(const float & sin_of_theta, const float & cos_of_theta, const float & shift, MEAS type);
    }
	
	namespace geometry
	{
		typedef struct
		{
			float x_part;
			float y_part;
		} TwoDim;
		
		float abs(const float input);
		float mag(const float & x, const float & y);
		float maxAbs(const float & input_1, const float & input_2);
		float dotProduct(const TwoDim & input_1, const TwoDim & input_2);
		float crossProduct(const TwoDim & input_1, const TwoDim & input_2);
		float squareRoot(const float & input, const uint32_t & iterations, const float & initial_guess);
		float euclideanDistance(const float & x0, const float & y0, const float & x1, const float & y1);
		TwoDim transform(const float & x, const float & y, const float & yaw, const float & x_trans, const float & y_trans);
		TwoDim findCentroid(TwoDim * input, const uint32_t size);
		float findCurvature(const TwoDim & input_1, const TwoDim & input_2, const TwoDim & input_3);
		float findCurvature(const float & a_length, const float & b_length);
	}
	
	namespace regression
	{
		float median(const float & input_1, const float & input_2, const float & input_3);
		void linreg(const float * const inputs, float * x, const float * const b, int32_t num_of_inputs);
		void quadreg(const float * const inputs, float * x, const float * const b, int32_t num_of_inputs);
	}
}

#endif
