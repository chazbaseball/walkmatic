#include "../include/utils.hpp"

void utils::regression::linreg(const float * const inputs, float * x, const float * const b, int32_t num_of_inputs)
{
    // Solve the normal equations: A^T*A*x = A^T*b -> x = (A^T*A)^(-1)*A^T*b
    // The vector x is mapped onto the column space of A

    float mult_and_accumulate_ATA = 0.0f;
    float mult_and_accumulate_ATb = 0.0f;
    float accumulate_ATA = 0.0f;
    float accumulate_b = 0.0f; 
    for (int32_t i = 0; i < num_of_inputs; i++)
    {
        float tempInput = inputs[i];
        float tempB = b[i];

        // Create values for A^T * A (row-ordered)
        mult_and_accumulate_ATA += tempInput * tempInput;
        accumulate_ATA += tempInput;

        // Create values for A^T * b (row-ordered)
        mult_and_accumulate_ATb += tempB * tempInput;
        accumulate_b += tempB;
    }

    /* Create A^T * A 
    1   1   1   1   1   *   1   x1 = A^T * A
    x1  x2  x3  x4  x5      1   x2
                            1   x3
                            1   x4  
                            1   x5  
    */
    float ATA[4];
    ATA[0] = static_cast<float>(num_of_inputs);
    ATA[1] = accumulate_ATA;
    ATA[2] = accumulate_ATA;
    ATA[3] = mult_and_accumulate_ATA;

    /* Create A^T * b
    1   1   1   1   1   *   b1 = A^T * b
    x1  x2  x3  x4  x5      b2
                            b3
                            b4  
                            b5  
    */
    float ATb[2];
    ATb[0] = accumulate_b;
    ATb[1] = mult_and_accumulate_ATb;

    // Create (A^T * A)^(-1)
    float ATAInv[4];
    float determinant = 1 / (ATA[0] * ATA[3] - ATA[1] * ATA[2]);
    ATAInv[0] = determinant * ATA[3];
    ATAInv[1] = -determinant * ATA[1];
    ATAInv[2] = -determinant * ATA[2];
    ATAInv[3] = determinant * ATA[0];

    // Solve for x = (A^T*A)^(-1)*A^T*b
	// x[0] is the constant term and x[1] is the rate (y = x[0] + x[1] * input)
    x[0] = ATAInv[0] * ATb[0] + ATAInv[1] * ATb[1];
    x[1] = ATAInv[2] * ATb[0] + ATAInv[3] * ATb[1];
}

void utils::regression::quadreg(const float * const inputs, float * x, const float * const b, int32_t num_of_inputs)
{
    // Solve the normal equations: A^T*A*x = A^T*b -> x = (A^T*A)^(-1)*A^T*b
    // The vector x is mapped onto the column space of A
	
	float accumulate_ATA = 0.0f;
    float mult_and_accumulate_ATA_square = 0.0f;
	float mult_and_accumulate_ATA_cube = 0.0f;
	float mult_and_accumulate_ATA_quad = 0.0f;
	
	float accumulate_b = 0.0f; 
    float mult_and_accumulate_ATb = 0.0f;
	float mult_and_accumulate_ATb_square = 0.0f;
    
    for (int32_t i = 0; i < num_of_inputs; i++)
    {
        float tempInput = inputs[i];
        float tempB = b[i];

        // Create values for A^T * A (row-ordered)
		accumulate_ATA += tempInput;
        mult_and_accumulate_ATA_square += tempInput * tempInput;
		mult_and_accumulate_ATA_cube += tempInput * tempInput * tempInput;
		mult_and_accumulate_ATA_quad += tempInput * tempInput * tempInput * tempInput;

        // Create values for A^T * b (row-ordered)
		accumulate_b += tempB;
        mult_and_accumulate_ATb += tempB * tempInput;
		mult_and_accumulate_ATb_square += tempB * tempInput * tempInput;
    }

    /* Create A^T * A 
    1   	1   	1   	1   	1   	*   1   x1	x1*x1 = A^T * A
    x1  	x2  	x3  	x4  	x5      	1   x2	x2*x2
    x1*x1   x2*x2   x3*x3   x4*x4   x5*x5       1   x3	x3*x3
												1   x4	x4*x4  
												1   x5  x5*x5
    */
    float ATA[9] = {static_cast<float>(num_of_inputs), accumulate_ATA, 					mult_and_accumulate_ATA_square,
					accumulate_ATA,					   mult_and_accumulate_ATA_square,  mult_and_accumulate_ATA_cube,
					mult_and_accumulate_ATA_square,	   mult_and_accumulate_ATA_cube,	mult_and_accumulate_ATA_quad};

    /* Create A^T * b
    1   	1   	1   	1   	1   	*   b1  = A^T * b
    x1  	x2  	x3  	x4  	x5      	b2   
    x1*x1   x2*x2   x3*x3   x4*x4   x5*x5      	b3
												b4
												b5   
    */
    float ATb[3];
    ATb[0] = accumulate_b;
    ATb[1] = mult_and_accumulate_ATb;
	ATb[2] = mult_and_accumulate_ATb_square;
					
	const uint32_t ROW = 3u;

    // Create (A^T * A)^(-1)
    float ATAInv[9];
    float determinant = ATA[0 * ROW + 0] * (ATA[1 * ROW + 1] * ATA[2 * ROW + 2] - ATA[2 * ROW + 1] * ATA[1 * ROW + 2]);
	determinant -= ATA[0 * ROW + 1] * (ATA[1 * ROW + 0] * ATA[2 * ROW + 2] - ATA[1 * ROW + 2] * ATA[2 * ROW + 0]);
	determinant += ATA[0 * ROW + 2] * (ATA[1 * ROW + 0] * ATA[2 * ROW + 1] - ATA[1 * ROW + 1] * ATA[2 * ROW + 0]);
    
	float determinant_inv = 1.0f / determinant;
	
	ATAInv[0 * ROW + 0] = determinant_inv * (ATA[1 * ROW + 1] * ATA[2 * ROW + 2] - ATA[2 * ROW + 1] * ATA[1 * ROW + 2]);
	ATAInv[0 * ROW + 1] = determinant_inv * (ATA[0 * ROW + 2] * ATA[2 * ROW + 1] - ATA[0 * ROW + 1] * ATA[2 * ROW + 2]);
	ATAInv[0 * ROW + 2] = determinant_inv * (ATA[0 * ROW + 1] * ATA[1 * ROW + 2] - ATA[0 * ROW + 2] * ATA[1 * ROW + 1]);
	ATAInv[1 * ROW + 0] = determinant_inv * (ATA[1 * ROW + 2] * ATA[2 * ROW + 0] - ATA[1 * ROW + 0] * ATA[2 * ROW + 2]);
	ATAInv[1 * ROW + 1] = determinant_inv * (ATA[0 * ROW + 0] * ATA[2 * ROW + 2] - ATA[0 * ROW + 2] * ATA[2 * ROW + 0]);
	ATAInv[1 * ROW + 2] = determinant_inv * (ATA[1 * ROW + 0] * ATA[0 * ROW + 2] - ATA[0 * ROW + 0] * ATA[1 * ROW + 2]);
	ATAInv[2 * ROW + 0] = determinant_inv * (ATA[1 * ROW + 0] * ATA[2 * ROW + 1] - ATA[2 * ROW + 0] * ATA[1 * ROW + 1]);
	ATAInv[2 * ROW + 1] = determinant_inv * (ATA[2 * ROW + 0] * ATA[0 * ROW + 1] - ATA[0 * ROW + 0] * ATA[2 * ROW + 1]);
	ATAInv[2 * ROW + 2] = determinant_inv * (ATA[0 * ROW + 0] * ATA[1 * ROW + 1] - ATA[1 * ROW + 0] * ATA[0 * ROW + 1]);

    // Solve for x = (A^T*A)^(-1)*A^T*b
	// y = x[0] + x[1] * input + x[2] * input *  input
    x[0] = ATAInv[0 * ROW + 0] * ATb[0] + ATAInv[0 * ROW + 1] * ATb[1] + ATAInv[0 * ROW + 2] * ATb[2];
	x[1] = ATAInv[1 * ROW + 0] * ATb[0] + ATAInv[1 * ROW + 1] * ATb[1] + ATAInv[1 * ROW + 2] * ATb[2];
	x[2] = ATAInv[2 * ROW + 0] * ATb[0] + ATAInv[2 * ROW + 1] * ATb[1] + ATAInv[2 * ROW + 2] * ATb[2];
}

float utils::regression::median(const float & input_1, const float & input_2, const float & input_3)
{
	if(input_1 > input_2)
	{
		if(input_1 < input_3)
		{
			return input_1;
		}
	}
	else
	{
		if(input_2 < input_3)
		{
			return input_2;
		}
	}
	
	return input_3;
}

