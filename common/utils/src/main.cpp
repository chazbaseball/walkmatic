#include "../include/utils.hpp"
#include <iostream>
#include <cmath>
#include <chrono>

int main()
{
    float theta = 0.0f;
    float avgError = 0.0f;
    float maxError = 0.0f;
    for(size_t i = 0; i < 180; i++)
    {
        double approx = static_cast<double>(utils::trig::cos(theta, utils::trig::DEGREES));
        double approx1 = cos(theta * utils::trig::PI / 180.0f);

        auto start1 = std::chrono::high_resolution_clock::now();
        double approxsin = static_cast<double>(utils::trig::sin(theta, utils::trig::DEGREES));
        std::cout <<  "inverse1: " << utils::trig::acosLin(approxsin, approx, 0.5f, utils::trig::DEGREES) << std::endl;
        auto stop1 = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop1 - start1);
        // std::cout << "Duration: " << duration.count() / 1000.0 << " milliseconds" << std::endl;


        auto start2 = std::chrono::high_resolution_clock::now();
        std::cout <<  "inverse2: " << acos(approx1) * 180.0f / utils::trig::PI << std::endl;
        auto stop2 = std::chrono::high_resolution_clock::now();
        duration = std::chrono::duration_cast<std::chrono::microseconds>(stop2 - start2);
        // std::cout << "Duration: " << duration.count() / 1000.0 << " milliseconds" << std::endl;

        approx = (approx > 0) ? approx : -approx;
        approx1 = (approx1 > 0) ? approx1 : -approx1;
        if(theta == 360.0f || theta == 180.0f)
        {
            approx1 = 0.0f;
        }
        if(approx1 != 0)
        {
            double error = (approx - approx1) / approx1;
            error = (error > 0) ? error : -error;
            if(error > maxError)
            {
                maxError = error;
            }
            avgError += error;

            std::cout << "Relative Error for theta = " << theta << ": " << error << std::endl;
        }
        
        theta = theta + 1.0f;
    }

    avgError /= 178;
    std::cout << "Avg Error: " << avgError << std::endl;
    std::cout << "Max Error: " << maxError << std::endl;
    
}