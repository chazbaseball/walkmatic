#include "../include/utils.hpp"

// Use Babilonian Method. https://en.wikipedia.org/wiki/Methods_of_computing_square_roots
float utils::geometry::squareRoot(const float & input, const uint32_t & iterations, const float & initial_guess)
{
	float guess = initial_guess;
	for(uint32_t i = 0; i < iterations; i++)
	{
		guess = (input / guess + guess) / 2.0f;
	}
	
	return guess;
}

float utils::geometry::abs(const float input)
{
	if(input < 0.0f)
	{
		return -input;
	}
	return input;
}

float utils::geometry::mag(const float & x, const float & y)
{
	float under_square_root = x * x + y * y;
	return squareRoot(under_square_root, 10u, 3.0f);
}

float utils::geometry::maxAbs(const float & input_1, const float & input_2)
{
	if(utils::geometry::abs(input_1) > utils::geometry::abs(input_2))
	{
		return utils::geometry::abs(input_1);
	}
	else
	{
		return utils::geometry::abs(input_2);
	}
}

float utils::geometry::euclideanDistance(const float & x0, const float & y0, const float & x1, const float & y1)
{
	float under_square_root = (x0 - x1) * (x0 - x1) + (y0 - y1) * (y0 - y1);
	return squareRoot(under_square_root, 10u, 3.0f);
}

utils::geometry::TwoDim utils::geometry::transform(const float & x, const float & y, const float & yaw, const float & x_trans, const float & y_trans)
{
	TwoDim output;
	
	// Rotate
	output.x_part = utils::trig::cos(yaw, utils::trig::RADIANS) * x - utils::trig::sin(yaw, utils::trig::RADIANS) * y;
	output.y_part = utils::trig::sin(yaw, utils::trig::RADIANS) * x + utils::trig::cos(yaw, utils::trig::RADIANS) * y;
	
	// Translate
	output.x_part += x_trans;
	output.y_part += y_trans;
	
	return output;
}

utils::geometry::TwoDim utils::geometry::findCentroid(utils::geometry::TwoDim * input, const uint32_t size)
{
	TwoDim output;
	output.x_part = 0.0f;
	output.y_part = 0.0f;
	
	for(uint32_t i = 0u; i < size; i++)
	{
		output.x_part += input[i].x_part;
		output.y_part += input[i].y_part;
	}
	
	output.x_part /= static_cast<float>(size);
	output.y_part /= static_cast<float>(size);
	
	return output;
}

float utils::geometry::dotProduct(const TwoDim & input_1, const TwoDim & input_2)
{
	return input_1.x_part * input_2.x_part + input_1.y_part * input_2.y_part;
}

float utils::geometry::crossProduct(const TwoDim & input_1, const TwoDim & input_2)
{
	// Since input vectors will always live in the x-y plane, this output is the magnitude of the
	// output vector  x_hat * 0 + y_hat * 0 + z_hat * <output>
	
	return input_1.x_part * input_2.y_part - input_1.y_part * input_2.x_part;
}

float utils::geometry::findCurvature(const TwoDim & input_1, const TwoDim & input_2, const TwoDim & input_3)
{
	// See here: https://www.researchgate.net/figure/Calculating-curvature-and-bending-strain-from-three-points-in-space-How-does-standard_fig1_311949206
	
	float a_length = euclideanDistance(input_1.x_part, input_1.y_part, input_2.x_part, input_2.y_part);
	float b_length = euclideanDistance(input_2.x_part, input_2.y_part, input_3.x_part, input_3.y_part);
	float c_length = euclideanDistance(input_1.x_part, input_1.y_part, input_3.x_part, input_3.y_part);
	
	float q = (a_length * a_length + b_length * b_length - c_length * c_length) / (2.0f * a_length * b_length);
	
	float under_square_root = 1.0f - q * q;
	float radius = c_length / (2.0f * squareRoot(under_square_root, 10u, 1.0f));
	
	// Determine if the curvature should be positive or negative (positive is in the direction of positive angular rate)
	TwoDim one_to_three = {input_3.x_part - input_1.x_part, input_3.y_part - input_1.y_part};
	TwoDim one_to_two = {input_2.x_part - input_1.y_part, input_2.y_part - input_1.y_part};
	
	float direction = crossProduct(one_to_two, one_to_three);
	
	float curvature = 1.0f / radius;
	if(direction < 0.0f)
	{
		curvature = -curvature;
	}
	
	return curvature;
}

float utils::geometry::findCurvature(const float & a_length, const float & b_length)
{
	
}

#ifdef UTILS_GEO_UNIT_TEST
#include "../../diagnostics/include/diagnostics.hpp"
int main()
{
	float result;
	float verify;
	diagnostics::Diagnostics diagnostics(TEST);
	uint8_t x_part[] = "X: ";
	uint8_t y_part[] = "Y: ";
	while(1)
	{
		float x = 1.0f;
		float y = 0.0f;
		float yaw = -utils::trig::PI / 4.0f;
		float x_trans = 0.0f;
		float y_trans = 0.0f;
		float test = 1.456f;
		utils::geometry::TwoDim output = utils::geometry::transform(x, y, yaw, x_trans, y_trans);
		verify = result * result;
		diagnostics.update(x_part, 3u, output.x_part);
		diagnostics.update(y_part, 3u, output.y_part);
	}
}
#endif