#include "../include/utils.hpp"

// Implement Bhaskara I's sine approximation (less work than McLaurin series)
// See here for more details: https://en.wikipedia.org/wiki/Bhaskara_I%27s_sine_approximation_formula

float utils::trig::sin(const float & theta, MEAS type)
{
    // Convert theta to degrees
    float equivTheta = (type == RADIANS) ? theta * 180.0f / PI : theta;

    // Find equivalent values of sin
    while(equivTheta >= 360.0f)
    {
        equivTheta -= 360.0f;
    }

    while(equivTheta <= 0)
    {
        equivTheta += 360.0f;
    }

    float sign = 1.0f;

    if(equivTheta >= 180.0f)
    {
        equivTheta -= 180.0f;
        sign = -sign;
    }

    // Implement Bhaskara I's sine approximation (less work than McLaurin series)
    // See here for more details: https://en.wikipedia.org/wiki/Bhaskara_I%27s_sine_approximation_formula
    return sign * (4.0f * equivTheta * (180.0f - equivTheta)) / (40500.0f - equivTheta * (180 - equivTheta));
}

float utils::trig::cos(const float & theta, MEAS type)
{
    float output;
    if(type == RADIANS)
    {
        output = utils::trig::sin(theta + PI / 2.0f, type);
    }
    else
    {
        output = utils::trig::sin(theta + 90.0f, type);
    }
    return output;
}


// Maclaurin series
/**
 * Compute the inverse cosine of theta with a 10th order Taylor Series
 * https://proofwiki.org/wiki/Power_Series_Expansion_for_Real_Arccosine_Function
 */ 
float utils::trig::acos(const float & theta, MEAS type)
{
    const uint32_t NUM_OF_COEFFICIENTS = 10u;
    float coefficients[10] = {1.0000000f, 
							  0.1666666f, 
							  0.0750000f, 
							  0.0446429f, 
							  0.0303819f, 
							  0.0223722f, 
							  0.0173528f,
							  0.0139648f,
							  0.0115518f,
							  0.0097616f};
    float output = 0.5f * PI;
    float store_theta_powers = theta;
	float m = 0.2003348f / 0.02f; // acos(0.98) / 0.02
							  
	// For inputs close to limit, use a linear approximation
	if(theta > 0.98f && theta <= 1.0f)
	{
		output = -m * (theta - 1.0f); // point-slope form
	}
	else if(theta < -0.98f && theta >= -1.0f)
	{
		output = -m * (theta + 1.0f) + utils::trig::PI; // point-slope form
	}
	else if(theta >= -0.98f && theta <= 0.98f)
	{
		for(uint32_t i = 0; i < NUM_OF_COEFFICIENTS; i++)
		{
			output -= coefficients[i] * store_theta_powers;
			store_theta_powers *= theta * theta; // Increase power of theta by 2
		}
	}
	else
	{
		output = theta; // Not valid!
	}

    if(type == DEGREES && output != theta)
    {
        output = output * 180.0f / PI;
    }

    return output;
}

/**
 * Computes the approximate inverse cosine of theta given sin(theta) and cos(theta). Uses linearized models of tan(theta) and cot(theta)
 * to compute the solution.
 */ 
float utils::trig::acosLin(const float & sin_of_theta, const float & cos_of_theta, const float & shift, MEAS type)
{
    float output;
    if(cos_of_theta > 0)
    {
        if(cos_of_theta > sin_of_theta)
        {
            float tan_of_theta = sin_of_theta / cos_of_theta;
            output = 45.0f * tan_of_theta + shift; // From y = (1/45)x = tan(theta) for 0 to 45 degrees
        }
        else
        {
            float cot_of_theta = cos_of_theta / sin_of_theta;
            output = -45.0f * cot_of_theta + 90.0f - shift; // From y = -(1/45)x + 2 = cot(theta) for 45 to 90 degrees
        }
    }
    else
    {
        float cos_of_theta_abs = -cos_of_theta;
        if(cos_of_theta_abs > sin_of_theta)
        {
            float tan_of_theta = sin_of_theta / cos_of_theta_abs;
            output = -45.0f * tan_of_theta + 180.0f - shift; // From y = -(1/45)x + 4 = tan(theta) for 135 to 180 degrees
        }
        else
        {
            float cot_of_theta = cos_of_theta_abs / sin_of_theta;
            output = 45.0f * cot_of_theta + 90.0f + shift; // From y = (1/45)x + 2 = cot(theta) for 90 to 135 degrees
        }
    }
    
    return output;
}

#ifdef UTILS_TRIG_UNIT_TEST
#include "../../diagnostics/include/diagnostics.hpp"
int main()
{
	float result_sin, result_cos, result_acos;
	diagnostics::Diagnostics diagnostics(configDiagnostics::UNIT_TEST_DIAGNOSTICS);
	uint8_t name_sin[] = "VERIFY_SIN";
	uint8_t name_cos[] = "VERIFY_COS";
	uint8_t name_acos[] = "VERIFY_ACOS";
	while(1)
	{
		float test = 1.456f;
		float test_acos = -0.98f;
		result_acos = utils::trig::acos(test_acos, utils::trig::DEGREES);
		result_sin = utils::trig::sin(45.0f, utils::trig::DEGREES);
		result_cos = utils::trig::cos(45.0f - 90.0f, utils::trig::DEGREES);
		diagnostics.update(name_sin, 10u, result_sin);
		diagnostics.update(name_cos, 10u, result_cos);
		diagnostics.update(name_acos, 11u, result_acos);
	}
}
#endif