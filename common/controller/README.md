# controller #
This module is responsible for controlling the heading of the walker such that it remains on the path created by the parent.

![](../../diagrams/pid.png)