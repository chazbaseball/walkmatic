#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP

#include "../../common.hpp"
#include "../../diagnostics/include/diagnostics.hpp"

namespace controller
{
	// Diagnostic Messages
	static uint8_t MESSAGE_DISP_ERROR[] = "DISP_ERROR";
	static const uint8_t MESSAGE_DISP_ERROR_LENGTH= 10u;
	static uint8_t MESSAGE_DISP_DIRECTION[] = "DISP_DIRECTION";
	static const uint8_t MESSAGE_DISP_DIRECTION_LENGTH = 14u;
	static uint8_t MESSAGE_BIAS_CURV[] = "BIAS_CURVATURE";
	static const uint8_t MESSAGE_BIAS_CURV_LENGTH = 14u;
	static uint8_t MESSAGE_COMMAND_CURV[] = "COMMAND_CURVATURE";
	static const uint8_t MESSAGE_COMMAND_CURV_LENGTH = 17u;
	static uint8_t MESSAGE_WAYPOINT_X[] = "WAYPOINT_X";
	static const uint8_t MESSAGE_WAYPOINT_X_LENGTH = 10u;
	static uint8_t MESSAGE_WAYPOINT_Y[] = "WAYPOINT_Y";
	static const uint8_t MESSAGE_WAYPOINT_Y_LENGTH = 10u;
	
	enum CommandCurvatureOptions
	{
		GO_RIGHT, // The walker turning right corresponds to a negative curvature
		GO_LEFT, // The walker turning left corresponds to a positive curvature
		SAME_AS_BIAS
	};

    class Controller
    {
        private:
			diagnostics::Diagnostics diagnostics;
			const configController::Config CONFIG;
			uint32_t current_index_in_path;
			float normal_to_path_error;
			float normal_to_path_error_direction;
			float bias_curvature;
			float command_curvature;
			TwoDim front_left_wheel_location;
			TwoDim front_right_wheel_location;
			TwoDim back_left_wheel_location; // Assume wheel locations form a rectangle
			TwoDim back_right_wheel_location;
			void findCurrentErrorAndBias(const State & current_location, Path & path);
			float findCommandCurvature(const float & displacement_error, const float & displacement_error_direction);
			SteeringCommand calcServoCommand(const float & curvature);
        public:
            Controller(const configController::Config & config);
            SteeringCommand step(const State & current_location, Path & path);
			float getCommandedCurvature();
			void reset();
            ~Controller();
    };
}

#endif 