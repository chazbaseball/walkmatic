#include "../include/controller.hpp"

controller::Controller::Controller(const configController::Config & config) : diagnostics(config.DIAGNOSTICS_CONFIG),
																			  CONFIG(config)																		  
{
	reset();
}

void controller::Controller::reset()
{
	current_index_in_path = 0u;
	normal_to_path_error = 0.0f;
	normal_to_path_error_direction = 0.0f;
	bias_curvature = 0.0f;
	command_curvature = 0.0f;
	
	front_left_wheel_location.y_part = configWalkerDimensions::axle_length / 2.0f * CONFIG.SCALE_TO_METERS;
	front_right_wheel_location.y_part = -configWalkerDimensions::axle_length / 2.0f * CONFIG.SCALE_TO_METERS;
	
	front_left_wheel_location.x_part = configWalkerDimensions::axle_to_axle_length / 2.0f * CONFIG.SCALE_TO_METERS;
	front_right_wheel_location.x_part = configWalkerDimensions::axle_to_axle_length / 2.0f * CONFIG.SCALE_TO_METERS;

	back_left_wheel_location.y_part = front_left_wheel_location.y_part;
	back_right_wheel_location.y_part = front_right_wheel_location.y_part;
	
	back_left_wheel_location.x_part = 0.0f;
	back_right_wheel_location.x_part = 0.0f;
}

void controller::Controller::findCurrentErrorAndBias(const State & current_location, Path & path)
{
	float current_distance, min_distance;
	min_distance = 100000.0f;
	
	// Move current location back
	for(uint32_t i = 0u; i < CONFIG.WAYPOINTS_TO_CHECK; i++)
	{
		path.moveCurrBack();
	}
	
	// Find the minimum distance to the path, this is the current index
	for(uint32_t i = 0u; i < 2u * CONFIG.WAYPOINTS_TO_CHECK + 1u; i++)
	{
		current_distance = utils::geometry::euclideanDistance(current_location.x, current_location.y, path.getCurrX(), path.getCurrY());
		if(current_distance < min_distance)
		{
			min_distance = current_distance;
			current_index_in_path = path.getCurr();
		}
		path.moveCurrForward();
	}
	path.setCurr(current_index_in_path); // Update the current path index
	
	// Send diagnostics about current waypoint
	diagnostics.update(MESSAGE_WAYPOINT_X, MESSAGE_WAYPOINT_X_LENGTH, path.getCurrX());
	diagnostics.update(MESSAGE_WAYPOINT_Y, MESSAGE_WAYPOINT_Y_LENGTH, path.getCurrY());
	
	bias_curvature = path.getCurrCurvature();
	
	// Find unit vector in direction towards the nearest point on the path
	float x_disp = path.getCurrX() - current_location.x;
	float y_disp = path.getCurrY() - current_location.y;
	utils::geometry::TwoDim path_to_walker_direction = {-x_disp / min_distance, -y_disp / min_distance};
	
	// Get unit vector of next path segment
	utils::geometry::TwoDim segment_direction = path.getCurrSegmentUnitVector();
	
	// Calculate a number indicating on which side of the path the walker is located
	// Positive: walker is located to the right of the path, Negative: walker is located to the left of the path
	normal_to_path_error_direction = utils::geometry::crossProduct(path_to_walker_direction, segment_direction);
	
	// Calcaulate the displacement error normal to the next path segment
	float theta_between_lines = utils::trig::acos(utils::geometry::dotProduct(path_to_walker_direction, segment_direction), utils::trig::RADIANS);
	
	if(theta_between_lines > utils::trig::PI / 2.0f)
	{
		theta_between_lines = utils::trig::PI - theta_between_lines;
	}
	
	normal_to_path_error = min_distance * utils::trig::sin(theta_between_lines, utils::trig::RADIANS);
	normal_to_path_error = utils::geometry::abs(normal_to_path_error);
	
	// Send diagnostics about the displacement direction and bias curvature
	diagnostics.update(MESSAGE_DISP_DIRECTION, MESSAGE_DISP_DIRECTION_LENGTH, normal_to_path_error_direction);
	diagnostics.update(MESSAGE_DISP_ERROR, MESSAGE_DISP_ERROR_LENGTH, normal_to_path_error);
	diagnostics.update(MESSAGE_BIAS_CURV, MESSAGE_BIAS_CURV_LENGTH, bias_curvature);
}

float controller::Controller::findCommandCurvature(const float & displacement_error, const float & displacement_error_direction)
{
	CommandCurvatureOptions curvature_command_option;
	
	// Determine an action the walker must take to stay on the path
	if(displacement_error_direction > 0.0f)
	{
		curvature_command_option = GO_LEFT;
	}
	else if(displacement_error_direction < 0.0f)
	{
		curvature_command_option = GO_RIGHT;
	}
	else
	{
		curvature_command_option = SAME_AS_BIAS;
	}
	
	// Normalize and saturate displacement error
	float normalized_displacement_error = displacement_error / CONFIG.MAX_DISPLACEMENT_ERROR;
	if(normalized_displacement_error > 1.0f)
	{
		normalized_displacement_error = 1.0f;
	}
	
	float command_curvature;
	// Use the path displacement error and the direction of that error to calculate command curvature
	switch(curvature_command_option)
	{
		case GO_LEFT:
			command_curvature = bias_curvature + (CONFIG.MAX_CURVATURE - bias_curvature) * normalized_displacement_error;
			break;
		case GO_RIGHT:
			command_curvature = bias_curvature - (CONFIG.MAX_CURVATURE + bias_curvature) * normalized_displacement_error;
			break;
		case SAME_AS_BIAS:
			command_curvature = bias_curvature;
			break;
		default:
			break;
	}
	
	// Saturate curvature
	if(command_curvature > CONFIG.MAX_CURVATURE)
	{
		command_curvature = CONFIG.MAX_CURVATURE;
	}
	
	if(command_curvature < -CONFIG.MAX_CURVATURE)
	{
		command_curvature = -CONFIG.MAX_CURVATURE;
	}
		
	command_curvature *= 1.0f / CONFIG.SCALE_TO_METERS; // Remember curvature has units [1/m]

	diagnostics.update(MESSAGE_COMMAND_CURV, MESSAGE_COMMAND_CURV_LENGTH, command_curvature);
	return command_curvature;
}

SteeringCommand controller::Controller::calcServoCommand(const float & curvature)
{
	SteeringCommand output;
	
	// EVERYING IS IN METERS IN THIS FUNCTION
	if(utils::geometry::abs(curvature) > CONFIG.MIN_CURVATURE)
	{
		float turning_radius = 1.0f / curvature;
	
		// Find curvature that each front wheel must follow for ackermann steering
		// Hypotenuse of triangles
		float curvature_left_wheel = 1.0f / utils::geometry::euclideanDistance(front_left_wheel_location.x_part,
																		   front_left_wheel_location.y_part,
																		   0.0f,
																		   turning_radius);
		float curvature_right_wheel = 1.0f / utils::geometry::euclideanDistance(front_right_wheel_location.x_part,
																			front_right_wheel_location.y_part,
																			0.0f,
																			turning_radius);
	
		if(turning_radius < 0.0f)
		{
			curvature_left_wheel = -curvature_left_wheel;
			curvature_right_wheel = -curvature_right_wheel;
		}
	
		// Get adjacent sides of triangles 
		float left_adj = utils::geometry::euclideanDistance(back_left_wheel_location.x_part,
														back_left_wheel_location.y_part,
														0.0f,
														turning_radius);
	
		float right_adj = utils::geometry::euclideanDistance(back_right_wheel_location.x_part,
														 back_right_wheel_location.y_part,
														 0.0f,
														 turning_radius);

		// Calculate acos() operands
		float operand_acos_left = curvature_left_wheel * left_adj;
		float operand_acos_right = curvature_right_wheel * right_adj;
	
	
		output.theta_left = utils::trig::acos(operand_acos_left, utils::trig::RADIANS);
		output.theta_right = utils::trig::acos(operand_acos_right, utils::trig::RADIANS);
	
		// Make sure output is in terms of positive and negative angles
		if(output.theta_left > utils::trig::PI / 2.0f)
		{
			output.theta_left = output.theta_left - utils::trig::PI;
		}

		if(output.theta_right > utils::trig::PI / 2.0f)
		{
			output.theta_right = output.theta_right - utils::trig::PI;
		}
	
		// Make sure output is in terms of positive and negative angles
		if(output.theta_left < -utils::trig::PI / 2.0f)
		{
			output.theta_left = output.theta_left + utils::trig::PI;
		}

		if(output.theta_right < -utils::trig::PI / 2.0f)
		{
			output.theta_right = output.theta_right + utils::trig::PI;
		}
	
		// Account for the fact that turning left is positive and turning right is negative
		float temp = output.theta_right;
		output.theta_right = -output.theta_left;
		output.theta_left = -temp;
	}
	else
	{
		output.theta_left = 0.0f;
		output.theta_right = 0.0f;
	}

	return output;
}

SteeringCommand controller::Controller::step(const State & current_location, Path & path)
{
	if(path.getSize() > CONFIG.WAYPOINTS_TO_CHECK * 2u)
	{
		findCurrentErrorAndBias(current_location, path);
		command_curvature = findCommandCurvature(normal_to_path_error, normal_to_path_error_direction);
	}
	
	return calcServoCommand(command_curvature);
}

float controller::Controller::getCommandedCurvature()
{
	return command_curvature;
}

controller::Controller::~Controller()
{
	
}

#ifdef CONTROLLER_UNIT_TEST
#include "../../common/interrupts/include/interrupts.hpp"

int main()
{
	timestampInitialize();
	controller::Controller controller(configController::DEFAULT);
	Path path;
	State location;
	location.reset();
	location.y = -3.0f;

	//path.add(-5.0f, -10.0f);
	path.add(-5.0f, -5.0f);
	path.add(5.0f, 5.0f);
	path.add(5.0f, 10.0f);
	path.add(5.0f, 15.0f);
	path.add(5.0f, 20.0f);
	path.add(5.0f, 25.0f);
	path.add(5.0f, 30.0f);
	path.add(5.0f, 35.0f);
	path.add(5.0f, 40.0f);
	path.add(5.0f, 45.0f);
	path.add(5.0f, 50.0f);
	path.makeCircular();
	
	while(1)
	{
		controller.step(location, path);
	}
}

#endif