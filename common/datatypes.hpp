#ifndef DATATYPES_HPP
#define DATATYPES_HPP

#include "pathobject/include/pathobject.hpp"
#include "stm32f4xx.h"

typedef struct
{
	float x; // meters
	float y;
	float yaw; // radians
} Transform;

struct ImuData
{
	uint64_t timestamp_acc;
	uint64_t timestamp_gyro;
    float x_acc; // IMU FRAME
    float y_acc; // IMU FRAME
    float z_rot_vel; // WORLD FRAME
	float heading; // WORLD FRAME
};

struct UltrasonicData
{
	uint64_t timestamp;
	float distance; // in mm
	Transform ultrasonic_to_walker;
};

struct Measurement 
{
	ImuData imu_data;
	UltrasonicData ultrasonic_data;
};

struct SteeringCommand
{
	float theta_left;
	float theta_right;
};

struct TwoDim
{
	float x_part;
	float y_part;
	
	TwoDim operator+(TwoDim & operand)
	{
		TwoDim output;
		output.x_part = this->x_part + operand.x_part;
		output.y_part = this->y_part + operand.y_part;
		return output;
	};
	
	TwoDim operator-(TwoDim & operand)
	{
		TwoDim output;
		output.x_part = this->x_part - operand.x_part;
		output.y_part = this->y_part - operand.y_part;
		return output;
	};
	
	TwoDim operator*(float & operand)
	{
		TwoDim output;
		output.x_part = this->x_part * operand;
		output.y_part = this->y_part * operand;
		return output;
	};
	
	TwoDim operator*=(float & operand)
	{
		this->x_part = this->x_part * operand;
		this->y_part = this->y_part * operand;
		return *this;
	};
	
	TwoDim operator/=(float & operand)
	{
		this->x_part = this->x_part / operand;
		this->y_part = this->y_part / operand;
		return *this;
	};
	
	TwoDim operator+=(TwoDim & operand)
	{
		this->x_part = this->x_part + operand.x_part;
		this->y_part = this->y_part + operand.y_part;
		return *this;
	};
};

struct State
{
    float x;
    float y;
	float curvature; 
    float theta; 
	float ang_rate;
	float time;
	
	State operator-(State & another_state)
	{
		State output;
		output.x = this->x - another_state.x;
		output.y = this->y - another_state.y;
		output.curvature = this->curvature - another_state.curvature;
		output.theta = this->theta;
		output.ang_rate = this->ang_rate;
		output.time = this->time;
		return output;
	};
	
	State operator+(State & another_state)
	{
		State output;
		output.x = this->x + another_state.x;
		output.y = this->y + another_state.y;
		output.curvature = this->curvature + another_state.curvature;
		output.theta = this->theta;
		output.ang_rate = this->ang_rate;
		output.time = this->time;
		return output;
	};
	
	State operator*(const float & multiplicand)
	{
		State output;
		output.x = this->x * multiplicand;
		output.y = this->y * multiplicand;
		output.curvature = this->curvature * multiplicand;
		output.theta = this->theta;
		output.ang_rate = this->ang_rate;
		output.time = this->time;
		return output;
	};
	
	State& operator=(const State & input) 
	{
		x = input.x;
		y = input.y;
		theta = input.theta;
		curvature = input.curvature;
		ang_rate = input.ang_rate;
		time = input.time;
		return *this;  // Return a reference to myself
	}
	
	void reset()
	{
		x = 0.0f;
		y = 0.0f;
		curvature = 0.0f;
		theta = 0.0f;
		ang_rate = 0.0f;
		time = 0.0f;
	}
};

typedef struct
{
	State high_freq_noise;
	State low_freq_noise;
} States;

enum Module
{
    PLAY,
    RECORD,
    IMU,
    CONDITIONER,
    COMP_FILTER,
    PATH_GENERATOR,
    CONTROLLER,
    PLOTTER,
    LCD,
    SERVO,
    UART,
	TEST, 
	I2C,
	SPI,
	MODE_MANAGER,
	ULTRASONIC
};

#endif

