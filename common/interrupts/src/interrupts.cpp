#include "../include/interrupts.hpp"

static uint64_t g_timestamp; // file scope
static modemanager::ModeManager g_manager(configModeManager::DEFAULT);

void timestampInitialize()
{
	g_timestamp = 0u;
	SysTick->CTRL = 0u; // Disable SysTick IRQ and SysTick counter
	SysTick->LOAD = configTimestamp::TICKS - 1u; // Set reload register
	NVIC_SetPriority(SysTick_IRQn, configTimestamp::PRIORITY);
	SysTick->VAL = 0u; // Reset counter value
	SysTick->CTRL |= SysTick_CTRL_CLKSOURCE_Msk; // Select processor clock
	SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk; // Enable SysTick exception request
	SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk; // Enable
}

uint64_t getTimestamp()
{
	return g_timestamp;
}

modemanager::ModeManager* getModeManager()
{
	return &g_manager;
}
	
extern "C" void SysTick_Handler(void)
{
	++g_timestamp;
}

extern "C" void EXTI9_5_IRQHandler(void)
{
	if((g_EXTI->PR & EXTI_PR_PR9) == EXTI_PR_PR9) // Check for EXTI5 flag
	{
		// Toggle signal
		g_manager.setRisingEdgeIndicator(true);
		g_EXTI->PR |= EXTI_PR_PR9; // Clear interrupt pending request
	}
}

extern "C" void TIM5_IRQHandler(void)
{
	if(modemanager::CHARGE == g_manager.getMode())
	{
		g_manager.checkCellChargeLevel();	//
	}
	TIM5->SR &= ~TIM_SR_CC1IF;				// Clear the interrupt flag
}
