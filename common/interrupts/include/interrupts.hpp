#ifndef INTERRUPTS_HPP
#define INTERRUPTS_HPP
#include "../../../common/common.hpp"
#include "../../../modemanager/include/modemanager.hpp"

modemanager::ModeManager* getModeManager();

extern "C" void SysTick_Handler(void);
extern "C" void EXTI9_5_IRQHandler(void);
extern "C" void TIM5_IRQHandler(void);

#endif
