#include "../include/buzzer.hpp"

buzzer::Buzzer::Buzzer(const configBuzzer::Config & config) : CONFIG(config)
{
	configureGPIO();
	configureTimer1();
}

void buzzer::Buzzer::configureGPIO()
{
	// Enable GPIO clockd
	g_RCC->AHB1ENR |= CONFIG.GPIO_CLOCK_ENABLE;
	
	// Clear and then set GPIO pin to Alternate Function Mode (0b10)
	CONFIG.GPIO->MODER &= ~(0x00000003u << (CONFIG.PIN * 2u)); // Clears 2 bits for setting mode
	CONFIG.GPIO->MODER |=  (0x00000002u << (CONFIG.PIN * 2u)); // Set to alternate function mode
	
	// Set Alternate Function source to Timer 1 Channel 2
	CONFIG.GPIO->AFR[0] &= ~(0x0000000Fu << (CONFIG.PIN * 2u)); // Clears AFRL
	CONFIG.GPIO->AFR[1] &= ~(0x0000000Fu << (CONFIG.PIN * 2u)); // Clears AFRH
	CONFIG.GPIO->AFR[1] |= CONFIG.ALTERNATE_FUNCTION;						// Sets bit 12 in AFRH for AF2 (TIM1_CH2) 
}

void buzzer::Buzzer::configureTimer1()
{
	g_RCC->APB2ENR 				|= CONFIG.TIMER1_CLOCK_ENABLE;		// Enable Timer 5 Clock
	CONFIG.BUZZER_TIMER->CR1 	&= ~TIM_CR1_DIR;									// Select Up Counting
	CONFIG.BUZZER_TIMER->PSC   	 = CONFIG.PRESCALER;							// Clock Frequency (Fc) = 44.1 kHz
	CONFIG.BUZZER_TIMER->ARR 	 = CONFIG.AUTO_RELOAD;						// Buzzer frequency Fb = Fc/(ARR+1) default = 441 Hz (A4)
	CONFIG.BUZZER_TIMER->CCMR1	&= ~TIM_CCMR1_OC2M;								// Clear Output Compare bits for channel 2
	CONFIG.BUZZER_TIMER->CCMR1  |= TIM_CCMR1_OC2M_1;							// Select PWM Mode 1 output on channel 2 (OC1M = 110)
	CONFIG.BUZZER_TIMER->CCMR1  |= TIM_CCMR1_OC2M_2;
	CONFIG.BUZZER_TIMER->CCMR1  |= TIM_CCMR1_OC2PE;								// Enable channel 2 preload
	CONFIG.BUZZER_TIMER->CCER   &= ~TIM_CCER_CC2P;								// Active high output
	CONFIG.BUZZER_TIMER->CCER   |= TIM_CCER_CC2E;									// Enable main output to PA0
	CONFIG.BUZZER_TIMER->BDTR   |= TIM_BDTR_MOE;									// Main output enable to PA0
	CONFIG.BUZZER_TIMER->CCR2   |= CONFIG.DUTY_CYCLE;							// Initial Duty Cycle of 50%;
}

void buzzer::Buzzer::setBuzzer(float time, float frequency)
{
	int time_step = 0; // Iterator
	int num_steps = static_cast<int>(time*43956.1f); // Number of runs through the counter
	CONFIG.BUZZER_TIMER->ARR = static_cast<uint32_t>(44100/frequency-1); // Set buzzer frequency ARR = frequency/Fc-1
	CONFIG.BUZZER_TIMER->CCR2 = CONFIG.BUZZER_TIMER->ARR/2u;
	
	CONFIG.BUZZER_TIMER->CR1 |= TIM_CR1_CEN; // Enable Timer
	while(time_step < num_steps) // Loop until time_step is equal to num_steps
	{
		if (CONFIG.BUZZER_TIMER->CNT == 0) // If the timer overflows
		{
			time_step++; //	Increment time_step
		}																												
	}																													
	CONFIG.BUZZER_TIMER->CR1 &= ~TIM_CR1_CEN; // Disable Timer
}

void buzzer::Buzzer::playCharge()
{
	// Star Spangled Banner
	setBuzzer(0.125,NOTE.G4);
	setBuzzer(0.125,NOTE.E4);
	setBuzzer(0.2,NOTE.C4);
	setBuzzer(0.05,rest);
	setBuzzer(0.2,NOTE.E4);
	setBuzzer(0.05,rest);
	setBuzzer(0.2,NOTE.G4);
	setBuzzer(0.05,rest);
	setBuzzer(0.5,NOTE.C5);
}

void buzzer::Buzzer::playRecord()
{
	// Once there was a snowman
	setBuzzer(0.1,NOTE.C4);
	setBuzzer(0.025,rest);
	setBuzzer(0.1,NOTE.D4);
	setBuzzer(0.025,rest);
	setBuzzer(0.1,NOTE.E4);
	setBuzzer(0.025,rest);
	setBuzzer(0.1,NOTE.F4);
	setBuzzer(0.025,rest);
	setBuzzer(0.225,NOTE.G4);
	setBuzzer(0.025,rest);
	setBuzzer(0.25,NOTE.E4);
}

void buzzer::Buzzer::playReady()
{
	// Hot Cross Buns
	setBuzzer(0.5,NOTE.E4);
	setBuzzer(0.5,NOTE.D4);
	setBuzzer(0.5,NOTE.C4);
}

void buzzer::Buzzer::playPlay()
{
	// Super Mario Medley
	setBuzzer(0.1,NOTE.E4);
	setBuzzer(0.025,rest);
	setBuzzer(0.125,NOTE.E4);
	setBuzzer(0.125,rest);
	setBuzzer(0.125,NOTE.E4);
	setBuzzer(0.125,rest);
	setBuzzer(0.10,NOTE.C4);
	setBuzzer(0.025,rest);
	setBuzzer(0.125,NOTE.E4);
	setBuzzer(0.125,rest);
	setBuzzer(0.125,NOTE.G4);
	setBuzzer(0.375,rest);
	setBuzzer(0.5,NOTE.G4);
}

buzzer::Buzzer::~Buzzer()
{
	
}

#ifdef BUZZER_UNIT_TEST
#include "../../common/interrupts/include/interrupts.hpp"

int main()
{
	buzzer::Buzzer buzzerDevice(configBuzzer::CONFIG_DEFAULT);
	while(1)
	{
		buzzerDevice.playCharge(configBuzzer::CONFIG_DEFAULT);
		for(int i; i < 1000000; i++);
	}
}

#endif