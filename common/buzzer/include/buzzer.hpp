#ifndef BUZZER_HPP
#define BUZZER_HPP
#include "../../common.hpp"
#include "../../diagnostics/include/diagnostics.hpp"

namespace buzzer
{
	const struct Notes
	{
		const float C2 = 65.41;
		const float D2 = 73.42;
		const float E2 = 82.41;
		const float F2 = 87.31;
		const float G2 = 98.00;
		const float A2 = 110.00;
		const float B2 = 123.47;
		
		const float C3 = 130.81;
		const float D3 = 146.83;
		const float E3 = 164.81;
		const float F3 = 174.61;
		const float G3 = 196.00;
		const float A3 = 220.00;
		const float B3 = 246.94;
		
		const float C4 = 261.63;
		const float D4 = 293.66;
		const float E4 = 329.63;
		const float F4 = 349.23;
		const float G4 = 392.00;
		const float A4 = 440.00;
		const float B4 = 493.88;
		
		const float C5 = 523.25;
		const float D5 = 587.33;
		const float E5 = 659.25;
		const float F5 = 698.46;
		const float G5 = 783.99;
		const float A5 = 880.00;
		const float B5 = 987.77;
		
		const float C6 = 1046.50;
		const float D6 = 1174.66;
		const float E6 = 1318.51;
		const float F6 = 1396.91;
		const float G6 = 1567.98;
		const float A6 = 1760.00;
		const float B6 = 1975.53;
	} NOTE;
				
	class Buzzer
	{
		public:
				Buzzer(const configBuzzer::Config & config);
				~Buzzer();
				void playCharge();
				void playRecord();
				void playReady();
				void playPlay();
		private:
				const configBuzzer::Config CONFIG;
			    void configureGPIO();
				void configureTimer1();
				void setBuzzer(float time, float frequency);
				float rest = 22050.0f;
	};
}
#endif //BUZZER_HPP