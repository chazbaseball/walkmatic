#ifndef COMMON_HPP
#define COMMON_HPP

#include "datatypes.hpp"
#include "stm32f4xx.h"
#include "../../configuration/config.hpp"
#include "utils/include/utils.hpp"

void timestampInitialize();
uint64_t getTimestamp();

#endif 
