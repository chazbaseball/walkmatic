#include "../include/pathgenerator.hpp"

pathgenerator::Pathgenerator::Pathgenerator(const configPathgenerator::Config & config) : diagnostics(config.DIAGNOSTICS_CONFIG),
																						  CONFIG(config)
{
	past_location.reset();
}

void pathgenerator::Pathgenerator::step(const State & location, Path & path)
{
	if(utils::geometry::euclideanDistance(past_location.x, past_location.y, location.x, location.y) > CONFIG.POINT_DISTANCE)
	{
		path.add(location.x, location.y);
		past_location = location;
		diagnostics.update(MESSAGE_PATH_SIZE, MESSAGE_PATH_SIZE_LENGTH, static_cast<float>(path.getSize()));
	}
}

bool pathgenerator::Pathgenerator::finishPath(Path & path)
{
	diagnostics.update(MESSAGE_PATH_SIZE, MESSAGE_PATH_SIZE_LENGTH, static_cast<float>(path.getSize()));
	if(path.makeCircular())
	{
		smooth(path);
		path.setCurrToPointClosestToOrigin();
		return true;
	}
	else
	{
		return false;
	}
}

void pathgenerator::Pathgenerator::smooth(Path & path)
{
	bool is_smooth = false;
	uint32_t count = 0u;
	float dummy = 0.0f;
	
	// Loop through the path, removing segments, until there are no segments that violate curvature restraints
	while(!is_smooth)
	{
		is_smooth = true;
		float curvature = utils::geometry::findCurvature(path.getCurrXY(), path.getCurrXY(), path.getCurrXY());
		path.moveCurrBack();
		path.moveCurrBack();
		path.addCurvature(curvature);
	
		while(!path.isCurrAtHead())
		{
			float curvature = utils::geometry::findCurvature(path.getCurrXY(), path.getCurrXY(), path.getCurrXY());
			path.moveCurrBack();
			path.moveCurrBack();
			if(curvature > utils::geometry::abs(CONFIG.MAX_CURVATURE) && !path.isCurrAtHead())
			{
				is_smooth = false;
				path.removeCurr(); // Make rightmost node curr
			}
			else
			{
				path.addCurvature(curvature); // Make the middle node curr
			}
		}
		diagnostics.update(MAKING_SMOOTH, 13u, dummy);
		diagnostics.update(NUM_OF_ITERATIONS, 20u, static_cast<float>(count));
		count++;
	}
	
	// Print out current path for reference
	utils::geometry::TwoDim waypoint = path.getCurrXY();
	diagnostics.update(MESSAGE_WAYPOINT_X, MESSAGE_WAYPOINT_LENGTH, waypoint.x_part);
	diagnostics.update(MESSAGE_WAYPOINT_Y, MESSAGE_WAYPOINT_LENGTH, waypoint.y_part);
	while(!path.isCurrAtHead())
	{
		utils::geometry::TwoDim waypoint = path.getCurrXY();
		diagnostics.update(MESSAGE_WAYPOINT_X, MESSAGE_WAYPOINT_LENGTH, waypoint.x_part);
		diagnostics.update(MESSAGE_WAYPOINT_Y, MESSAGE_WAYPOINT_LENGTH, waypoint.y_part);
	}
	diagnostics.update(MESSAGE_FINISHED_SMOOTHING, MESSAGE_FINISHED_SMOOTHING_LENGTH, dummy);
}

pathgenerator::Pathgenerator::~Pathgenerator()
{
	
}

#ifdef PATH_GENERATOR_UNIT_TEST

int main()
{
	Path path;
	pathgenerator::Pathgenerator path_gen(configPathgenerator::CONFIG_DEFAULT);
	diagnostics::Diagnostics diagnostics(PATH_GENERATOR);
	uint8_t name_x[] = "LOCATION_X";
	uint8_t name_y[] = "LOCATION_Y";
	
	float* test = new float[5];
	float* temp = test;
	delete temp;
	float five = test[0];
	
	State locations[89u];
	
	uint32_t num_of_locations = 89u; // (A number divisible by 4) - 1
	uint32_t save, save1, save2;
	
	// Create a square path
	for(uint32_t i = 0u; i < num_of_locations; i++)
	{
		if(i <= num_of_locations / 4u + 1u) // 0 to 5
		{
			locations[i].x = static_cast<float>(i);
			locations[i].y = 0.0f;
			save = i;
		}
		else if(i <= num_of_locations / 2u + 1u)
		{
			locations[i].x = static_cast<float>(save);
			locations[i].y = static_cast<float>(i - (num_of_locations / 4u + 1u));
			save1 = i - (num_of_locations / 4u + 1u);
		}
		else if(i < 3u * num_of_locations / 4u + 1u)
		{
			locations[i].x = static_cast<float>(--save);
			locations[i].y = static_cast<float>(save1);
		}
		else if (i < num_of_locations)
		{
			locations[i].x = 0.0f;
			locations[i].y = static_cast<float>(--save1);
		}
		
		//diagnostics.update(name_x, 10u, locations[i].x);
		//diagnostics.update(name_y, 10u, locations[i].y);
	}
	
	for(uint32_t i = 0; i < num_of_locations; i++)
	{
		path_gen.step(locations[i], path);
	}
	
	path_gen.finishPath(path);
		
	path_gen.smooth(path);
	
	while(1);
}
#endif
