#ifndef PATHGENERATOR_H
#define PATHGENERATOR_H
#include "../../common.hpp"
#include "../../diagnostics/include/diagnostics.hpp"

namespace pathgenerator
{
	static uint8_t MAKING_SMOOTH[] = "MAKING SMOOTH";
	static uint8_t NUM_OF_ITERATIONS[] = "NUMBER OF ITERATIONS";
	static uint8_t CURVATURE[] = "CURVATURE";
	static uint8_t MESSAGE_WAYPOINT_X[] = "WAYPOINT_X";
	static uint8_t MESSAGE_WAYPOINT_Y[] = "WAYPOINT_Y";
	static const uint8_t MESSAGE_WAYPOINT_LENGTH = 10u;
	static uint8_t MESSAGE_PATH_SIZE[] = "PATH SIZE";
	static const uint8_t MESSAGE_PATH_SIZE_LENGTH= 9u;
	static uint8_t MESSAGE_FINISHED_SMOOTHING[] = "FINISHING SMOOTHING";
	static const uint8_t MESSAGE_FINISHED_SMOOTHING_LENGTH= 19u;
	
    class Pathgenerator
    {
        private:
            diagnostics::Diagnostics diagnostics;
			const configPathgenerator::Config CONFIG;
			void smooth(Path & path);
			State past_location;
        public:
            Pathgenerator(const configPathgenerator::Config & config);
            void step(const State & location, Path & path);
			bool finishPath(Path & path);
            ~Pathgenerator();
    };
	
	static TwoDim TestPath[12] = {{0.0f, 0.0f},
						   {50.0f, 0.0f},
						   {100.0f, 0.0f},
						   {125.0f, -25.0f},
						   {150.0f, -50.0f},
						   {150.0f, -100.0f},
						   {125.0f, -125.0f},
						   {100.0f, -150.0f},
						   {50.0f, -150.0f},
						   {25.0f, -125.0f},
						   {0.0f, -100.0f},
						   {0.0f, -50.0f}};
}

#endif

