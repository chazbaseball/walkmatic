# pathgenerator #
This module is responsible for creating a Path Object from filtered position measurements.

![](../../diagrams/pathgenerator.png)