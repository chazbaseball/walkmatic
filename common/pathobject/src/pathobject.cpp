#include "../include/pathobject.hpp"

Path::Path()
{
	size = 0u;
	reset();
}

bool Path::add(float x, float y)
{
	if(size < 1000u)
	{
		path[curr + 1u].prev = curr;
		curr++;
		path[curr].next = curr + 1u;
		path[curr].x = x;
		path[curr].y = y;
		size++;
		return true;
	}
	else
	{
		return false;
	}
}

void Path::reset()
{
	head = 0u;
	curr = 0xFFFFFFFFu;
	circular = false;
	size = 0u;
}

uint32_t Path::getSize()
{
	return size;
}

void Path::addCurvature(float curv)
{
	path[curr].curvature = curv;
}

bool Path::makeCircular()
{
	if(size > 0u)
	{
		circular = true;
		path[curr].next = 0u;
		path[head].prev = curr;
		curr = head; // Set curr to beginning of path
		return true;
	}
	else
	{
		return false;
	}
}

bool Path::isCircular()
{
	return circular;
}

bool Path::moveCurrForward() 
{
    bool output = false;
	curr = path[curr].next;
    return output;
}

bool Path::removeCurr()
{
	if(size > 0u)
	{
		path[path[curr].prev].next = path[curr].next;
		path[path[curr].next].prev = path[curr].prev;
		curr = path[curr].next;
		size--;
		return true;
	}
	else
	{
		return false;
	}
}

bool Path::moveCurrBack()
{
	curr = path[curr].prev;
	return true;
}

float Path::getCurrX()
{
    return path[curr].x;
}

float Path::getCurrY()
{
    return path[curr].y;
}

uint32_t Path::getPrev()
{
	return path[curr].prev;
}

uint32_t Path::getNext()
{
	return path[curr].next;
}

uint32_t Path::getCurr()
{
	return curr;
}

void Path::setCurrToPointClosestToOrigin()
{
	// Find the minimum distance to the path, this is the current index
	float min_distance = 100000.0f;
	uint32_t index_of_closest_point;
	for(uint32_t i = 0u; i < size; i++)
	{
		float current_distance = utils::geometry::euclideanDistance(0.0f, 0.0f, getCurrX(), getCurrY());
		if(current_distance < min_distance)
		{
			min_distance = current_distance;
			index_of_closest_point = getCurr();
		}
		moveCurrForward();
	}
	setCurr(index_of_closest_point);
}

utils::geometry::TwoDim Path::getCurrSegmentUnitVector()
{
	float x_path = path[path[curr].next].x - path[curr].x;
	float y_path = path[path[curr].next].y - path[curr].y;
	float segment_distance = utils::geometry::mag(x_path, y_path);
	utils::geometry::TwoDim segment_direction = {x_path / segment_distance, y_path / segment_distance};
	return segment_direction;
}

float Path::getXFromIndex(const uint32_t & index)
{
	return path[index].x;
}

float Path::getYFromIndex(const uint32_t & index)
{
	return path[index].y;
}

bool Path::setCurr(const uint32_t & input)
{
	// Only change curr to a valid index
	if(input == path[path[input].prev].next && input == path[path[input].next].prev)
	{
		curr = input;
		return true;
	}
	
	return false;
}

/*
bool Path::pointInsideLoop(const uint32_t & index, const float & x, const float & y)
{
	uint32_t first_curr = curr;
	
	while(curr != first_curr)
	{
		float m = (path[path[curr].next].y - path[curr].y) / (path[path[curr].next].x - path[curr].x);
		y - path[curr].y = m * (x - path[curr].y);
		
		if(y <= m * (x - path[curr].y) + path[curr].y && x 
	}
}
*/

utils::geometry::TwoDim Path::getCurrXY()
{
	utils::geometry::TwoDim output;
	output.x_part = getCurrX();
	output.y_part = getCurrY();
	moveCurrForward();
	return output;
}

float Path::getCurrCurvature()
{
	return path[curr].curvature;
}
bool Path::isCurrAtHead()
{
	return curr == head;
}

Path::~Path()
{

}

