#ifndef PATHOBJECT_HPP
#define PATHOBJECT_HPP

#include "../../utils/include/utils.hpp"

class Path
{
    struct Node
    {   
		uint32_t prev;
        float x;
        float y;
		float curvature;
        uint32_t next;
    };
    private:
		Node path[1000];
        uint32_t head;
        uint32_t curr;
		uint32_t size;
		bool circular;
    public:
        Path();
		void reset();
        uint32_t getSize();
		bool isCircular();
        bool add(float x, float y);
		void addCurvature(float curv);
        bool makeCircular();
		void setCurrToPointClosestToOrigin();
        float getCurrX();
        float getCurrY();
		float getCurrCurvature();
        bool moveCurrForward();
        bool moveCurrBack();
		bool isCurrAtHead();
		utils::geometry::TwoDim getCurrXY();
		bool removeCurr();
		float getXFromIndex(const uint32_t & index);
		float getYFromIndex(const uint32_t & index);
		utils::geometry::TwoDim getCurrSegmentUnitVector();
		uint32_t getCurr();
		uint32_t getPrev();
		uint32_t getNext();
		bool setCurr(const uint32_t & input);
        ~Path();
};

#endif 
