#include "../include/uart.hpp"

// Constructor
uart::Uart::Uart(const configUart::Config & config) : UART(config.UART),
													  GPIORX(config.GPIORX),
													  GPIOTX(config.GPIOTX),
													  BAUD_RATE_DIVISOR(config.BAUD_RATE_DIVISOR),
													  RESET_COUNT(config.RESET_COUNT)
{
    this->pin_RX = config.PIN_RX;
    this->pin_TX = config.PIN_TX;
    this->gpio_clock_enable_RX = config.GPIO_CLOCK_ENABLE_RX;
    this->gpio_clock_enable_TX = config.GPIO_CLOCK_ENABLE_TX;
    this->af_RX = config.AF_RX;
    this->af_TX = config.AF_TX;
    this->uart_clock_enable = config.UART_CLOCK_ENABLE;
	configureGPIO();
    initializeUART();
}

void uart::Uart::configureGPIO()
{
    g_RCC->AHB1ENR |= gpio_clock_enable_RX;
    g_RCC->AHB1ENR |= gpio_clock_enable_TX;

    // Clear and then set RX GPIO pin to Alternate Function
    uint32_t clear_MODER = ~(0x00000003u << (pin_RX * 2u)); // Clears 2 bits for setting mode
	GPIORX->MODER &= clear_MODER;

    uint32_t set_MODER = (0x00000002u << (pin_RX * 2u)); // Set to alternate function
	GPIORX->MODER |= set_MODER;

    // Clear and then set TX GPIO pin to Alternate Function
    clear_MODER = ~(0x00000003u << (pin_TX * 2u)); // Clears 2 bits for setting mode
    GPIOTX->MODER &= clear_MODER;

    set_MODER = (0x00000002u << (pin_TX * 2u)); // Set to alternate function
    GPIOTX->MODER |= set_MODER;
    
	// Clear and then set RX GPIO pin to UART
    uint32_t clear_AFR = ~(0x0000000Fu << ((pin_RX % 8u) * 4u)); // Each pin has 4 bits
	GPIORX->AFR[pin_RX / 8u] &= clear_AFR;

    uint32_t set_AFR = af_RX << ((pin_RX % 8u) * 4u);
    GPIORX->AFR[pin_RX / 8u] |= set_AFR;

    // Clear and then set TX GPIO pin to UART
    clear_AFR = ~(0x0000000Fu << ((pin_TX % 8u) * 4u)); // Each pin has 4 bits
	GPIOTX->AFR[pin_TX / 8u] &= clear_AFR;

    set_AFR = af_RX << ((pin_TX % 8u) * 4u);
    GPIOTX->AFR[pin_TX / 8u] |= set_AFR;

    // Set RX and TX GPIO pins to Very High Speed (0b11)
	GPIORX->OSPEEDR |= (0x00000003u << (pin_RX * 2u));
    GPIOTX->OSPEEDR |= (0x00000003u << (pin_TX * 2u));

    // Clear and then set RX GPIO pin to Pull-Up
    clear_MODER = ~(0x00000003u << (pin_RX * 2u)); // Clears 2 bits for setting mode
	GPIORX->PUPDR &= clear_MODER;

    set_MODER = (0x00000001u << (pin_RX * 2u));
    GPIORX->PUPDR |= set_MODER;

    // Clear and then set TX GPIO pin to Pull-Up
    uint32_t clear_PUPDR = ~(0x00000003u << (pin_TX * 2u)); // Clears 2 bits for setting mode
	GPIOTX->PUPDR &= clear_PUPDR;

    uint32_t set_PUPDR = (0x00000001u << (pin_TX * 2u));
    GPIOTX->PUPDR |= set_PUPDR;
	
    // Set RX and TX GPIO pins as Push/Pull
	GPIORX->OTYPER &= ~(0x00000001U << (pin_RX));
    GPIOTX->OTYPER &= ~(0x00000001U << (pin_TX));
	
    // Enable UART clock
	g_RCC->APB1ENR |= uart_clock_enable;
}

void uart::Uart::initializeUART()
{
	// Disable UART
	UART->CR1 &= ~USART_CR1_UE;
	
	// Set data length to 8 bits
	UART->CR1 &= ~USART_CR1_M;
	
	// Select 1 stop bit
	UART->CR2 &= ~USART_CR2_STOP;
	
	// Set parity as no parity
	UART->CR1 &= ~USART_CR1_PCE;
	
	// Oversampling by 16
	UART->CR1 &= ~USART_CR1_OVER8;
	
	// Set Baud rate to 9600
	UART->BRR = BAUD_RATE_DIVISOR; 
	
	// Enable transmission and reception
	UART->CR1 |= USART_CR1_TE;
	UART->CR1 |= USART_CR1_RE;
	
	// Enable UART
	UART->CR1 |= USART_CR1_UE;
}

bool uart::Uart::send(unsigned char* const input, const unsigned int & len)
{
	for(uint32_t i = 0; i < len; i++)
	{
		while(!(UART->SR & USART_SR_TXE)); // Wait for hardware to set the TXE (Transmit Data Register Empty) flag
		UART->DR = input[i] & 0xFF;
	}
	
	while(!(UART->SR & USART_SR_TC)); // Wait for TC (Transmission Complete) Flag
	
	return true;
}

bool uart::Uart::receive(uint8_t* const output, const uint32_t & len)
{
	for(uint32_t i = 0u; i < len; i++)
	{
		while(!(UART->SR & USART_SR_RXNE)); // Wait until data register is not empty
		output[i] = UART->DR & 0xFF;
	}
	
	return true;
}

#ifdef UART_UNIT_TEST
int main()
{
	uart::Uart deviceUart(configUart::CONFIG_DIAGNOSTICS);
	bool result;
	unsigned char data[2];
	unsigned int length = 2;
    while (1)
    {
		deviceUart.receive(data, 2u);
		deviceUart.send(data, length);
    }
    
    return 0; // Should never get here
}
#endif

