#ifndef UART_H
#define UART_H
#include "../../../common.hpp"

namespace uart
{
    class Uart
    {
        private:
			USART_TypeDef * const UART; // pointer is const but the value it points to is not
            uint32_t uart_clock_enable;
			GPIO_TypeDef * const GPIORX; // PD2
			uint8_t pin_RX;
            uint32_t gpio_clock_enable_RX;
            uint32_t af_RX;
			GPIO_TypeDef * const GPIOTX; // PC12
			uint8_t pin_TX;
            uint32_t gpio_clock_enable_TX;
            uint32_t af_TX;
			const uint32_t BAUD_RATE_DIVISOR;
			const uint32_t RESET_COUNT;
			uint32_t count;
            unsigned int length;
            unsigned char* output;
            void configureGPIO();
            //void initializeUART();
        public:
			Uart(const configUart::Config & config);
			void initializeUART();
            bool send(unsigned char* const input, const unsigned int & len);
			bool receive(uint8_t* const output, const uint32_t & len);
    };
}
#endif

