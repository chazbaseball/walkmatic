#include "../include/imu.hpp"

imu::Imu::Imu(const configImu::Config & config) : diagnostics(config.DIAGNOSTICS_CONFIG),
												  CONFIG(config),
												  i2c(config.I2C_CONFIG)
{
	dummy = 0.0f;
	uint8_t message_length = 12u;
	diagnostics.update(imu::MESSAGE_START, message_length, dummy);
	initializeImu();
}

void imu::Imu::initializeImu()
{
	uint8_t mode[1] = {CONFIG.MODE};
	write(OPR_MODE, mode); // Set to correct mode
	
	uint8_t units[1] = {CONFIG.UNITS};
	write(UNIT_SEL, units); // Set correct units
	
	if(CONFIG.MODE < 0x08)
	{
		uint8_t first_page[1];
		read(PAGE_ID, first_page, 1u);
		uint8_t second_page[1] = {0x01};
		write(PAGE_ID, second_page); // Go to second page
	
		uint8_t gyr_config_0[1] = {CONFIG.GYR_CONFIGURATION_0};
		write(GYR_CONFIG_0, gyr_config_0); // Set GYRO_0 configuration
	
		uint8_t gyr_config_1[1] = {CONFIG.GYR_CONFIGURATION_1};
		write(GYR_CONFIG_0, gyr_config_1); // Set GYRO_1 configuration
	
		uint8_t mag_config[1] = {CONFIG.MAG_CONFIGURATION};
		write(GYR_CONFIG_0, mag_config); // Set MAGNETOMETER configuration
	
		uint8_t acc_config[1] = {CONFIG.ACC_CONFIGURATION};
		write(GYR_CONFIG_0, acc_config); // Set ACCELEROMETER configuration
	
		write(PAGE_ID, first_page); // Return to first page
	}
	uint8_t message_length = 23u;
	diagnostics.update(imu::MESSAGE_FINISH_INITIALIZATION, message_length, dummy);
}

bool imu::Imu::read(const uint8_t & address, uint8_t * const data, const uint8_t & len)
{
	uint8_t address_to_read[1] = {address};
	bool successful_write = i2c.Send(IMU_ADDR_WRITE, address_to_read, 1u, false);
	bool successful_read = i2c.Receive(IMU_ADDR_READ, data, len); // Address is automatically incremented after reading a byte
	return successful_write && successful_read;
}

bool imu::Imu::write(const uint8_t & address, uint8_t * const data)
{
	uint8_t data_to_write[2] = {address, data[0]};
	return i2c.Send(IMU_ADDR_WRITE, data_to_write, 2u, true);
}

float imu::Imu::i2cDataToFloat(const I2cData & data)
{
	int16_t temp = data.msb; // Data in IMU is stored as signed, 2-byte integers
	temp = (temp << 8) | data.lsb;
	return static_cast<float>(temp);
}


ImuData imu::Imu::getMeasurement()
{
	ImuData output;
	
	// Get data from gyroscope
	uint8_t data_z[2]; // Heading
	uint32_t count = 0u;
	while(!read(GYR_DATA_Z_LSB, data_z, 2u)) // Address automatically increments
	{
		if(count > CONFIG.RESET_COUNT)
		{
			uint8_t message_length = 21u;
			diagnostics.update(imu::MESSAGE_RESET_COUNT, message_length, dummy);
			initializeImu(); // If the IMU turned off, it will need to be reinitialized
		}
		count++;
	}		
	output.timestamp_gyro = getTimestamp() - configTimestamp::TICKS_DELAY_IMU; // Get timestamp
	
	// Assign LSB and MSB parts
	I2cData gyro_z = {data_z[0], data_z[1]};
	
	// Convert to float
	output.z_rot_vel = i2cDataToFloat(gyro_z);
	
	// Scale output according to units per bit
	output.z_rot_vel /= (CONFIG.UNITS & 0x02) ? CONFIG.GYR_NUM_TO_RAD : CONFIG.GYR_NUM_TO_DEG;
	
	// Get data from accelerometer
	uint8_t data_x_y[4]; // Acceleration in x and y directions
	count = 0u;
	while(!read(LIA_DATA_X_LSB, data_x_y, 4u)) // Address automatically increments
	{
		if(count > CONFIG.RESET_COUNT)
		{
			uint8_t message_length = 21u;
			diagnostics.update(imu::MESSAGE_RESET_COUNT, message_length, dummy);
			initializeImu(); // If the IMU turned off, it will need to be reinitialized
		}
		count++;
	}
	output.timestamp_acc = getTimestamp() - configTimestamp::TICKS_DELAY_IMU; // Get timestamp
	
	// Assign LSB and MSB parts
	I2cData acc_x = {data_x_y[0], data_x_y[1]};
	I2cData acc_y = {data_x_y[2], data_x_y[3]};
	
	// Convert to float
	output.x_acc = i2cDataToFloat(acc_x);
	output.y_acc = i2cDataToFloat(acc_y);
	
	// Scale output according to units per bit
	output.x_acc /= (CONFIG.UNITS & 0x01) ? CONFIG.ACC_NUM_TO_MILLIG : CONFIG.ACC_NUM_TO_MPERSECSQ;
	output.y_acc /= (CONFIG.UNITS & 0x01) ? CONFIG.ACC_NUM_TO_MILLIG : CONFIG.ACC_NUM_TO_MPERSECSQ;
	
	output.heading = getHeading();
	
	// Diagnostic information
	diagnostics.update(MESSAGE_X_ACC, MESSAGE_X_ACC_LENGTH, output.x_acc);
	diagnostics.update(MESSAGE_Y_ACC, MESSAGE_Y_ACC_LENGTH, output.y_acc);
	diagnostics.update(MESSAGE_HEADING, MESSAGE_HEADING_LENGTH, output.heading);
	
	return output;		
}

float imu::Imu::getHeading()
{
	uint8_t data[2];
	uint32_t count = 0u;
	while(!read(EUL_HEADING_LSB, data, 2u))
	{
		if(count > CONFIG.RESET_COUNT)
		{
			uint8_t message_length = 21u;
			diagnostics.update(imu::MESSAGE_RESET_COUNT, message_length, dummy);
			initializeImu(); // If the IMU turned off, it will need to be reinitialized
		}
		count++;
	}
	
	I2cData heading = {data[0], data[1]};
	float output = i2cDataToFloat(heading); 
	
	// Scale output according to units per bit
	output /= (CONFIG.UNITS & 0x04) ? CONFIG.HEADING_NUM_TO_RAD : CONFIG.HEADING_NUM_TO_DEG;
	
	// Saturation
	if(CONFIG.UNITS & 0x04)
	{
		if(output > 2.0f * utils::trig::PI)
		{
			output = 2.0f * utils::trig::PI;
		}
		else if(output < 0.0f)
		{
			output = 0.0f;
		}
	}
	else
	{
		if(output > 360.0f)
		{
			output = 360.0f;
		}
		else if(output < 0.0f)
		{
			output = 0.0f;
		}
	}
	
	return output;
}

float imu::Imu::getTemp()
{
	uint8_t data[1];
	uint32_t count = 0u;
	while(!read(OPR_MODE, data, 1u))
	{
		if(count > CONFIG.RESET_COUNT)
		{
			uint8_t message_length = 21u;
			diagnostics.update(imu::MESSAGE_RESET_COUNT, message_length, dummy);
			initializeImu(); // If the IMU turned off, it will need to be reinitialized
		}
		count++;
	}
	
	return static_cast<float>(data[0]);
}

imu::Imu::~Imu()
{
	
}

#ifdef IMU_UNIT_TEST

int main()
{
	timestampInitialize();
	imu::Imu imuDevice(configImu::CONFIG_DEFAULT);
	diagnostics::Diagnostics diagnostics(IMU);
	uint8_t name[] = "ACCEL_X";
	uint8_t time[] = "TIME";
	while(1)
	{
		ImuData current = imuDevice.getMeasurement();
		diagnostics.update(name, 7u, current.y_acc);
		float sec = static_cast<float>(current.timestamp_acc) * configTimestamp::TICKS_TO_SECONDS;
		diagnostics.update(time, 4u, sec);
	}
}
#endif

