#ifndef IMU_H
#define IMU_H
#include "../../../common.hpp"
#include "../../i2c/include/i2c.hpp"

namespace imu
{
	// IMU i2c addresses
	static const uint8_t IMU_ADDR_WRITE = 0x50; // 7-bit address is 0x28. This value includes R/W bit
	static const uint8_t IMU_ADDR_READ = 0x51;
	
	// Register Addresses for BNO055 IMU (see pages 52-53 of datasheet)
	static const uint8_t CHIP_ID = 0x00;
	static const uint8_t PAGE_ID = 0x07;
	static const uint8_t OPR_MODE = 0x3D;
	static const uint8_t CONFIG_MODE = 0x07; // Value for OPR_MODE register
	static const uint8_t UNIT_SEL = 0x3B;
	static const uint8_t AXIS_MAP_CONFIG = 0x41;
	static const uint8_t AXIS_MAP_SIGN = 0x42;
	static const uint8_t TEMP = 0x34;
	static const uint8_t EUL_HEADING_MSB = 0x1B; // Only in Fusion Modes
	static const uint8_t EUL_HEADING_LSB = 0x1A; // Only in Fusion Modes
	
	// Gyroscope
	static const uint8_t GYR_CONFIG_1 = 0x0B; // Page 1
	static const uint8_t GYR_CONFIG_0 = 0x0A; // Page 1
	static const uint8_t GYR_DATA_Z_MSB = 0x19;
	static const uint8_t GYR_DATA_Z_LSB = 0x18;
	
	// Magnetometer
	static const uint8_t MAG_CONFIG = 0x09; // Page 1
	static const uint8_t MAG_DATA_Y_MSB = 0x11;
	static const uint8_t MAG_DATA_Y_LSB = 0x10;
	static const uint8_t MAG_DATA_X_MSB = 0x0F;
	static const uint8_t MAG_DATA_X_LSB = 0x0E;
	
	// Accelerometer
	static const uint8_t ACC_CONFIG = 0x08; // Page 1
	static const uint8_t ACC_DATA_Y_MSB = 0x0B;
	static const uint8_t ACC_DATA_Y_LSB = 0x0A;
	static const uint8_t ACC_DATA_X_MSB = 0x09;
	static const uint8_t ACC_DATA_X_LSB = 0x08;
	static const uint8_t LIA_DATA_Y_MSB = 0x2B;
	static const uint8_t LIA_DATA_Y_LSB = 0x2A;
	static const uint8_t LIA_DATA_X_MSB = 0x29;
	static const uint8_t LIA_DATA_X_LSB = 0x28;
	
	// Diagnostic message
	static uint8_t MESSAGE_RESET_COUNT[] = "RESET COUNT VIOLATED";
	static uint8_t MESSAGE_FINISH_INITIALIZATION[] = "INITIALIZATION FINISHED";
	static uint8_t MESSAGE_START[] = "STARTING IMU";
	static uint8_t MESSAGE_X_ACC[] = "X_ACC";
	static uint8_t MESSAGE_X_ACC_LENGTH = 5u;
	static uint8_t MESSAGE_Y_ACC[] = "Y_ACC";
	static uint8_t MESSAGE_Y_ACC_LENGTH = 5u;
	static uint8_t MESSAGE_HEADING[] = "HEADING";
	static uint8_t MESSAGE_HEADING_LENGTH = 7u;
	
	typedef struct // data is stored in ucontroller as little-endian
	{
		uint8_t lsb;
		uint8_t msb;
	} I2cData;

    class Imu
    {
        private:
			diagnostics::Diagnostics diagnostics;
            i2c::I2c i2c;
			const configImu::Config CONFIG;
			void initializeImu();
			float i2cDataToFloat(const I2cData & data);
			bool read(const uint8_t & address, uint8_t * const data, const uint8_t & len);
			bool write(const uint8_t & address, uint8_t * const data);
			float dummy;
        public:
            Imu(const configImu::Config & config);
            ImuData getMeasurement();
            float getHeading(); // Gets euler angle representation of state directly from imu
            float getTemp();
            ~Imu();
    };
}
#endif 
