# imu
This module is responsible for communicating with the Inertial Measurement Unit (IMU). This driver will provide signalgenerator with the measurements it needs.

![](../../../diagrams/imu.png)