#ifndef SPI_H
#define SPI_H
#include "../../../common.hpp"
#include "../../../diagnostics/include/diagnostics.hpp"

namespace spi
{
	enum SlaveSelect
	{
		ONE,
		TWO,
		THREE
	};
	
	// Diagnostic Messages
	static uint8_t MESSAGE_TXE_NOT_SET[] = "ERROR: TX BUFFER NOT EMPTY";
	static const uint8_t MESSAGE_TXE_NOT_SET_LENGTH = 26u;
	static uint8_t MESSAGE_RXNE_NOT_SET[] = "ERROR: RX BUFFER EMPTY";
	static const uint8_t MESSAGE_RXNE_NOT_SET_LENGTH = 22u;
	static uint8_t MESSAGE_WARNING[] = "WARNING: SLAVE COULD BE DISCONNECTED";
	static const uint8_t MESSAGE_WARNING_LENGTH = 36u;
	
    class Spi
    {
        private:
			diagnostics::Diagnostics diagnostics;
			const configSpi::Config CONFIG;
			float dummy_diagnostics;
			uint32_t count;
            void configureGPIO();
            void initializeSpi();
        public:
            Spi(const configSpi::Config & config);
			void slaveSelect(SlaveSelect slave, bool low);
            bool send(const uint8_t* const output, const uint8_t & len);
			bool receive(uint8_t* const input, const uint8_t & len);
    };
}

#endif
