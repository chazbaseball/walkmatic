#include "../include/spi.hpp"

// Constructor
spi::Spi::Spi(const configSpi::Config & config) : diagnostics(config.DIAGNOSTICS_CONFIG),
												  CONFIG(config)
{
	dummy_diagnostics = 0.0f;
	
	// Enable SPI Clock before anything else
	
	if(CONFIG.SPI == SPI1)
	{
		g_RCC->APB2ENR |= CONFIG.SPI_CLOCK_EN; // Enable SPI Clock
	}
	else
	{
		g_RCC->APB1ENR |= CONFIG.SPI_CLOCK_EN; // Enable SPI Clock
	}
	

	configureGPIO();
	initializeSpi();
}

void spi::Spi::configureGPIO()
{
	// Enable GPIO Clocks
	g_RCC->AHB1ENR |= CONFIG.GPIO_CLOCK_ENABLE_MOSI;
    g_RCC->AHB1ENR |= CONFIG.GPIO_CLOCK_ENABLE_MISO;
	g_RCC->AHB1ENR |= CONFIG.GPIO_CLOCK_ENABLE_SCL;
	g_RCC->AHB1ENR |= CONFIG.GPIO_CLOCK_ENABLE_SS;
	
	// Clear and then set MOSI GPIO pin to Alternate Function
	CONFIG.GPIO_MOSI->MODER &= ~(0x00000003u << (CONFIG.PIN_MOSI * 2u)); // Clears 2 bits for setting mode
	CONFIG.GPIO_MOSI->MODER |= (0x00000002u << (CONFIG.PIN_MOSI * 2u)); // Set to alternate function

    // Clear and then set MISO GPIO pin to Alternate Function
	CONFIG.GPIO_MISO->MODER &= ~(0x00000003u << (CONFIG.PIN_MISO * 2u)); // Clears 2 bits for setting mode
	CONFIG.GPIO_MISO->MODER |= (0x00000002u << (CONFIG.PIN_MISO * 2u)); // Set to alternate function
	
	// Clear and then set SCL GPIO pin to Alternate Function
	CONFIG.GPIO_SCL->MODER &= ~(0x00000003u << (CONFIG.PIN_SCL * 2u)); // Clears 2 bits for setting mode
	CONFIG.GPIO_SCL->MODER |= (0x00000002u << (CONFIG.PIN_SCL * 2u)); // Set to alternate function
    
	// Clear and then set MOSI GPIO pin to SPI
	CONFIG.GPIO_MOSI->AFR[CONFIG.PIN_MOSI / 8u] &= ~(0x0000000Fu << ((CONFIG.PIN_MOSI % 8u) * 4u)); // Each pin has 4 bits
    CONFIG.GPIO_MOSI->AFR[CONFIG.PIN_MOSI / 8u] |= CONFIG.AF_MOSI << ((CONFIG.PIN_MOSI % 8u) * 4u);
	
	// Clear and then set MISO GPIO pin to SPI
	CONFIG.GPIO_MISO->AFR[CONFIG.PIN_MISO / 8u] &= ~(0x0000000Fu << ((CONFIG.PIN_MISO % 8u) * 4u)); // Each pin has 4 bits
    CONFIG.GPIO_MISO->AFR[CONFIG.PIN_MISO / 8u] |= CONFIG.AF_MISO << ((CONFIG.PIN_MISO % 8u) * 4u);
	
	// Clear and then set SCL GPIO pin to SPI
	CONFIG.GPIO_SCL->AFR[CONFIG.PIN_SCL / 8u] &= ~(0x0000000Fu << ((CONFIG.PIN_SCL % 8u) * 4u)); // Each pin has 4 bits
    CONFIG.GPIO_SCL->AFR[CONFIG.PIN_SCL / 8u] |= CONFIG.AF_SCL << ((CONFIG.PIN_SCL % 8u) * 4u);
	
	// Set GPIO pins as Output Push-Pull (0b0)
	CONFIG.GPIO_MOSI->OTYPER &= ~(0x00000001U << (CONFIG.PIN_MOSI));
	CONFIG.GPIO_MISO->OTYPER &= ~(0x00000001U << (CONFIG.PIN_MISO));
	CONFIG.GPIO_SCL->OTYPER &= ~(0x00000001U << (CONFIG.PIN_SCL));
	
	// Set GPIO pins to disable Pull-Up (0b00)
	CONFIG.GPIO_MOSI->PUPDR &= ~(0x00000003u << (CONFIG.PIN_MOSI * 2u)); // Clears 2 bits for setting mode
	CONFIG.GPIO_MISO->PUPDR &= ~(0x00000003u << (CONFIG.PIN_MISO * 2u)); // Clears 2 bits for setting mode
	CONFIG.GPIO_SCL->PUPDR &= ~(0x00000003u << (CONFIG.PIN_SCL * 2u)); // Clears 2 bits for setting mode
	
	// Set SS to high (SPI not selected)
	slaveSelect(ONE, false);
}

void spi::Spi::initializeSpi()
{
	// Disable SPI
	CONFIG.SPI->CR1 &= ~SPI_CR1_SPE; 
	
	// Configure as duplex (0)
	CONFIG.SPI->CR1 &= ~SPI_CR1_RXONLY;
	
	// Enable bidirectional data mode
	CONFIG.SPI->CR1 &= ~SPI_CR1_BIDIMODE;
	
	// Enable output in bidirectional mode
	CONFIG.SPI->CR1 &= ~SPI_CR1_BIDIOE;
	
	// Set 8-bit data frame format
	CONFIG.SPI->CR1 &= ~SPI_CR1_DFF;
	
	// Transmit/Receive MSB first
	CONFIG.SPI->CR1 &= ~SPI_CR1_LSBFIRST;
	
	// First clock transition is the first data capture edge
	CONFIG.SPI->CR1 |= SPI_CR1_CPHA;
	
	// Set Clock polarity as low when idle
	CONFIG.SPI->CR1 |= SPI_CR1_CPOL;
	
	// Set Baud Rate
	CONFIG.SPI->CR1 |= CONFIG.BR_CONTROL << 3u;
	
	// CRC Polynomial
	CONFIG.SPI->CRCPR = 10u;
	
	// Disable hardware CRC calculation
	CONFIG.SPI->CR1 &= ~SPI_CR1_CRCEN;
	
	// Set Frame format to Motorola mode (0)
	CONFIG.SPI->CR2 &= ~SPI_CR2_FRF;
	
	// Enable software slave management
	CONFIG.SPI->CR1 |= SPI_CR1_SSM;
	
	// Set microcontroller as master
	CONFIG.SPI->CR1 |= SPI_CR1_MSTR;
	
	// Manage slave selection using software
	CONFIG.SPI->CR1 |= SPI_CR1_SSI;
	
	// Enable SPI
	CONFIG.SPI->CR1 |= SPI_CR1_SPE;
}

void spi::Spi::slaveSelect(SlaveSelect slave, bool low)
{
	switch(slave)
	{
		case ONE:
			if(low)
			{
				CONFIG.GPIO_SS->ODR &= ~(0x00000001U << (CONFIG.PIN_SS));
			}
			else
			{
				CONFIG.GPIO_SS->ODR |= (0x00000001U << (CONFIG.PIN_SS));
			}
			break;
		default:
			break;
	}
}

bool spi::Spi::send(const uint8_t* const output, const uint8_t & len)
{
	for(uint8_t i = 0u; i < len; i++)
	{
		count = 0u;
		while((CONFIG.SPI->SR & SPI_SR_TXE) == 0) // Wait for transmit buffer to be empty
		{
			if(count > CONFIG.RESET_COUNT)
			{
				diagnostics.update(spi::MESSAGE_TXE_NOT_SET, spi::MESSAGE_TXE_NOT_SET_LENGTH, dummy_diagnostics);
				initializeSpi();
				return false;
			}
			count++;
		} 
		
		CONFIG.SPI->DR = output[i];
	}
	
	return true;
}

bool spi::Spi::receive(uint8_t* const input, const uint8_t & len)
{
	for(uint8_t i = 0u; i < len; i++)
	{
		count = 0u;
		while((CONFIG.SPI->SR & SPI_SR_RXNE) == 0) // Wait for receive buffer to not be empty
		{
			if(count > CONFIG.RESET_COUNT)
			{
				diagnostics.update(spi::MESSAGE_RXNE_NOT_SET, spi::MESSAGE_RXNE_NOT_SET_LENGTH, dummy_diagnostics);
				initializeSpi();
				return false;
			}
			count++;
		}
		input[i] = CONFIG.SPI->DR;
		
		if(input[i] == 0xFF)
		{
			diagnostics.update(spi::MESSAGE_WARNING, spi::MESSAGE_WARNING_LENGTH, dummy_diagnostics);
		}
	}
	
	return true;
}

#ifdef SPI_UNIT_TEST

int main()
{
	timestampInitialize();
	diagnostics::Diagnostics DeviceDiagnostics(SPI);
	spi::Spi testSpi(configSpi::CONFIG_TEST);
	uint8_t address_who_am_i[1] = {0x8A}; // 0x0F is the address and 0x80 is the R/W bit
	
	uint8_t value[1] = {0u};
	uint8_t name[] = "WHO_AM_I";
	uint8_t message_length = 8u;
	float dummy = 0;
	
	testSpi.slaveSelect(spi::ONE, false);
	
	// The following test used loopback (MOSI->MISO)
	while(1)
	{
		testSpi.send(address_who_am_i, 1u);
		testSpi.receive(value, 1u);
		DeviceDiagnostics.update(name, message_length, static_cast<float>(value[0]));
	}
}

#endif
