#ifndef ULTRASONIC_HPP
#define ULTRASONIC_HPP
#include "../../../common.hpp"
#include "../../../diagnostics/include/diagnostics.hpp"

namespace ultrasonic
{
	// Commands
	static const uint8_t GET_DISTANCE = 0x55; // in millimeters
	static const uint8_t GET_TEMP = 0x50; // in Celsius
	
	// Diagnostic message
	static uint8_t MESSAGE_FINISH_INITIALIZATION[] = "INITIALIZATION FINISHED";
	static uint8_t MESSAGE_FINISH_INITIALIZATION_LENGTH = 23u;
	static uint8_t MESSAGE_START[] = "STARTING ULTRASONIC";
	static uint8_t MESSAGE_START_LENGTH = 19u;
	static uint8_t MESSAGE_DISTANCE[] = "DISTANCE";
	static uint8_t MESSAGE_DISTANCE_LENGTH = 8u;
	
	typedef struct // data is stored in ucontroller as little-endian
	{
		uint8_t lsb;
		uint8_t msb;
	} UartData;

    class Ultrasonic
    {
        private:
			diagnostics::Diagnostics diagnostics;
			uart::Uart ultrasonic_1;
			const configUltrasonic::Config CONFIG;
			float uartDataToFloat(const UartData & data);
			void delay(const float milliseconds);
			float getDistance(uart::Uart & ultrasonic); // Gets mm distance measurement from all ultrasonic sensors
            float getTemp(uart::Uart & ultrasonic); // Get C temp data from all ultrasonic sensors
			float dummy;
        public:
            Ultrasonic(const configUltrasonic::Config & config);
			float getTempPublic();
			UltrasonicData getMeasurement();
            ~Ultrasonic();
    };
}

#endif