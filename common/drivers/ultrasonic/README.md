# ultrasonic #
This module is responsible for getting data from three ultrasonic sensors placed on the walkmatic. This data will be used to get relative position data in `conditioner`.

![](../../diagrams/pid.png)