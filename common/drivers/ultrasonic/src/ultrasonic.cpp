#include "../include/ultrasonic.hpp"

ultrasonic::Ultrasonic::Ultrasonic(const configUltrasonic::Config & config) : diagnostics(config.DIAGNOSTICS_CONFIG),
																			  CONFIG(config),
																			  ultrasonic_1(config.CONFIG_ULTRASONIC_1)
{
	dummy = 0.0f;
	diagnostics.update(MESSAGE_START, MESSAGE_START_LENGTH, dummy);
	delay(CONFIG.START_UP_DELAY);
	ultrasonic_1.initializeUART();
	diagnostics.update(MESSAGE_FINISH_INITIALIZATION, MESSAGE_FINISH_INITIALIZATION_LENGTH, dummy);
}

float ultrasonic::Ultrasonic::getDistance(uart::Uart & ultrasonic)
{
	// Send Command
	uint8_t get_distance[1] = {GET_DISTANCE}; 
	ultrasonic.send(get_distance, 1u); 
	
	// Get Result
	uint8_t data[2]; // Allocate space for data
	ultrasonic.receive(data, 2u); 
	
	UartData data_ultrasonic = {data[1], data[0]}; // Assign lsb and msb parts
	float output = uartDataToFloat(data_ultrasonic);
	
	if(output > CONFIG.MAX_RANGE_MM) // US-100 Sensors do not always send data in correct order
	{
		UartData data_ultrasonic = {data[0], data[1]}; // Assign lsb and msb parts
		output = uartDataToFloat(data_ultrasonic);
		
		if(output > CONFIG.MAX_RANGE_MM) // Signal to data consumer that data should be ignored
		{
			output = -1.0f;
		}
	}
	
	return output;
}

float ultrasonic::Ultrasonic::getTemp(uart::Uart & ultrasonic)
{
	// Send Command
	uint8_t get_distance[1] = {GET_TEMP};
	ultrasonic.send(get_distance, 1u);
	
	// Get Result
	uint8_t data[1];
	ultrasonic.receive(data, 1u);
	
	float output = static_cast<float>(data[0]) - 45.0f;
	
	return output;
}

float ultrasonic::Ultrasonic::getTempPublic()
{
	return getTemp(ultrasonic_1);
}

UltrasonicData ultrasonic::Ultrasonic::getMeasurement()
{
	UltrasonicData output;
	
	// Get data from ultrasonic_1
	output.distance = getDistance(ultrasonic_1);
	output.timestamp = getTimestamp() - configTimestamp::TICKS_DELAY_ULTRASONIC;
	
	diagnostics.update(MESSAGE_DISTANCE, MESSAGE_DISTANCE_LENGTH, output.distance);
	
	return output;
}

void ultrasonic::Ultrasonic::delay(const float milliseconds)
{
	uint64_t past_timestamp = getTimestamp();
	uint64_t current_timestamp = 0u;
	
	float past_time = static_cast<float>(past_timestamp) * configTimestamp::TICKS_TO_SECONDS * 1000.0f;
	float current_time = past_time;
	
	while(current_time - past_time < milliseconds)
	{
		current_timestamp = getTimestamp();
		current_time = static_cast<float>(current_timestamp) * configTimestamp::TICKS_TO_SECONDS * 1000.0f;
	}
}


float ultrasonic::Ultrasonic::uartDataToFloat(const UartData & data)
{
	uint16_t temp = data.msb; // Data in UART is stored as unsigned, 2-byte integers
	temp = (temp << 8) | data.lsb;
	return static_cast<float>(temp);
}

ultrasonic::Ultrasonic::~Ultrasonic()
{
	
}

#ifdef ULTRASONIC_UNIT_TEST

int main()
{
	timestampInitialize();
	ultrasonic::Ultrasonic ultrasonicDevice(configUltrasonic::CONFIG_DEFAULT);
	diagnostics::Diagnostics diagnostics(ULTRASONIC);
	uint8_t name[] = "DISTANCE";
	uint8_t time[] = "TIME";
	float dummy = 0.0f;
	while(1)
	{
		UltrasonicData current = ultrasonicDevice.getMeasurement();
		diagnostics.update(name, 8u, current.distance);
		//float sec = static_cast<float>(current.ultrasonic_1.timestamp) * configTimestamp::TICKS_TO_SECONDS;
		//float temperature = ultrasonicDevice.getTempPublic();
		//diagnostics.update(time, 4u, temperature);
	}
}
#endif
 