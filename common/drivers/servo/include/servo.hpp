#ifndef SERVO_HPP
#define SERVO_HPP
#include "../../../common.hpp"
#include "../../common/diagnostics/include/diagnostics.hpp"

namespace servo
{
    class Servo
    {
        private:
			diagnostics::Diagnostics diagnostics;
			const configServo::Config CONFIG;
			uint16_t duty_cycle;
			float theta_range;
			uint16_t duty_cycle_range_0;
			uint16_t duty_cycle_range_1;
			void configureGPIO();
			void initializeTIMER();
        public:
			Servo(const configServo::Config & config);
			void disableAndReset();
			bool updateTheta(const float & theta_0, const float & theta_1);
			~Servo();
    };
}

#endif

