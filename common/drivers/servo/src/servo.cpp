#include "../include/servo.hpp"

servo::Servo::Servo(const configServo::Config & config) : diagnostics(config.DIAGNOSTICS_CONFIG),
														  CONFIG(config)
{
	float min_theta = CONFIG.MIN_THETA;
	float max_theta = CONFIG.MAX_THETA;
	
	// Calculate the range of theta values and the corresponding range of duty cycle values
	theta_range = max_theta - min_theta;
	if(theta_range < 0)
	{
		theta_range = -theta_range;
	}
	duty_cycle_range_0 = CONFIG.SERVO_0.MAX_DUTY_CYCLE - CONFIG.SERVO_0.MIN_DUTY_CYCLE;
	duty_cycle_range_1 = CONFIG.SERVO_1.MAX_DUTY_CYCLE - CONFIG.SERVO_1.MIN_DUTY_CYCLE;
	configureGPIO();
	initializeTIMER();
	disableAndReset();
}

void servo::Servo::configureGPIO()
{
	g_RCC->AHB1ENR |= CONFIG.SERVO_0.GPIO_CLOCK_ENABLE;
	g_RCC->AHB1ENR |= CONFIG.SERVO_1.GPIO_CLOCK_ENABLE;

    // Clear and then set GPIO pins to Alternate Function
    uint32_t clear_MODER = ~(0x00000003u << (CONFIG.SERVO_0.PIN * 2u)); // Clears 2 bits for setting mode
	CONFIG.SERVO_0.GPIO->MODER &= clear_MODER;

    uint32_t set_MODER = (0x00000002u << (CONFIG.SERVO_0.PIN * 2u)); // Set to alternate function
	CONFIG.SERVO_0.GPIO->MODER |= set_MODER;
	
	clear_MODER = ~(0x00000003u << (CONFIG.SERVO_1.PIN  * 2u)); // Clears 2 bits for setting mode
	CONFIG.SERVO_1.GPIO->MODER &= clear_MODER;

    set_MODER = (0x00000002u << (CONFIG.SERVO_1.PIN * 2u)); // Set to alternate function
	CONFIG.SERVO_1.GPIO->MODER |= set_MODER;
	
	// Clear and then set GPIO pin to TIMER
    uint32_t clear_AFR = ~(0x0000000Fu << ((CONFIG.SERVO_0.PIN % 8u) * 4u)); // Each pin has 4 bits
	CONFIG.SERVO_0.GPIO->AFR[CONFIG.SERVO_0.PIN / 8u] &= clear_AFR;

    uint32_t set_AFR = CONFIG.SERVO_0.AF << ((CONFIG.SERVO_0.PIN % 8u) * 4u);
    CONFIG.SERVO_0.GPIO->AFR[CONFIG.SERVO_0.PIN / 8u] |= set_AFR;
	
	clear_AFR = ~(0x0000000Fu << ((CONFIG.SERVO_1.PIN % 8u) * 4u)); // Each pin has 4 bits
	CONFIG.SERVO_1.GPIO->AFR[CONFIG.SERVO_1.PIN / 8u] &= clear_AFR;

    set_AFR = CONFIG.SERVO_1.AF << ((CONFIG.SERVO_1.PIN % 8u) * 4u);
    CONFIG.SERVO_1.GPIO->AFR[CONFIG.SERVO_1.PIN / 8u] |= set_AFR;
	
	// Set GPIO pin to Low Speed (0b00)
	CONFIG.SERVO_0.GPIO->OSPEEDR &= (0xFFFFFFFCu << (CONFIG.SERVO_0.PIN * 2u));
	
	CONFIG.SERVO_1.GPIO->OSPEEDR &= (0xFFFFFFFCu << (CONFIG.SERVO_1.PIN * 2u));
	
	// Clear and then set GPIO pin to no pull-up/pull-down (0b00)
    CONFIG.SERVO_0.GPIO->PUPDR &= (0xFFFFFFFCu << (CONFIG.SERVO_0.PIN * 2u)); 
	
	CONFIG.SERVO_1.GPIO->PUPDR &= (0xFFFFFFFCu << (CONFIG.SERVO_1.PIN * 2u));
}

void servo::Servo::initializeTIMER()
{
	g_RCC->APB1ENR |= CONFIG.SERVO_0.TIMER_CLOCK_ENABLE;
	g_RCC->APB1ENR |= CONFIG.SERVO_1.TIMER_CLOCK_ENABLE;
	
	// Set Counting direction to up (0)
	CONFIG.SERVO_0.TIMER->CR1 &= ~TIM_CR1_DIR;
	
	CONFIG.SERVO_1.TIMER->CR1 &= ~TIM_CR1_DIR;
	
	// Set prescaler
	CONFIG.SERVO_0.TIMER->PSC = CONFIG.PRESCALER;
	
	CONFIG.SERVO_1.TIMER->PSC = CONFIG.PRESCALER;
	
	// Set Auto-Reload
	CONFIG.SERVO_0.TIMER->ARR = CONFIG.AUTO_RELOAD;
	
	CONFIG.SERVO_1.TIMER->ARR = CONFIG.AUTO_RELOAD;
	
	// Initialize duty cycle
	CONFIG.SERVO_0.TIMER->CCR4 = CONFIG.SERVO_0.INITIAL_DUTY_CYCLE; // Channel 4
	
	CONFIG.SERVO_1.TIMER->CCR3 = CONFIG.SERVO_1.INITIAL_DUTY_CYCLE; // Channel 3
	
	// Clear output compare mode bits for channels 2 and 3 **CHANNELS ARE NOT IN CONFIGURATION HEADER**
	CONFIG.SERVO_0.TIMER->CCMR2 &= ~TIM_CCMR2_OC4M;
	
	CONFIG.SERVO_1.TIMER->CCMR2 &= ~TIM_CCMR2_OC3M;
	
	// Select pwm mode
	CONFIG.SERVO_0.TIMER->CCMR2 |= TIM_CCMR2_OC4M_1 | TIM_CCMR2_OC4M_2;
	
	CONFIG.SERVO_1.TIMER->CCMR2 |= TIM_CCMR2_OC3M_1 | TIM_CCMR2_OC3M_2;
	
	// Enable Preload
	CONFIG.SERVO_0.TIMER->CCMR2 |= TIM_CCMR2_OC4PE;
	CONFIG.SERVO_1.TIMER->CCMR2 |= TIM_CCMR2_OC3PE;
	
	// Select output polarity to active high (0)
	CONFIG.SERVO_0.TIMER->CCER &= ~TIM_CCER_CC4P;
	
	CONFIG.SERVO_1.TIMER->CCER &= ~TIM_CCER_CC3P;
	
	// Enable output for channel 1 complementary output
	CONFIG.SERVO_0.TIMER->CCER |= TIM_CCER_CC4E;
	
	CONFIG.SERVO_1.TIMER->CCER |= TIM_CCER_CC3E;
	
	// Main output enable (MOE)
	CONFIG.SERVO_0.TIMER->BDTR |= TIM_BDTR_MOE;
	
	CONFIG.SERVO_1.TIMER->BDTR |= TIM_BDTR_MOE;
}

bool servo::Servo::updateTheta(const float & theta_0, const float & theta_1)
{
	float theta_0_saturated, theta_1_saturated;
	
	// Saturate theta_0 if out of bounds
	if(theta_0 > CONFIG.MAX_THETA)
	{
		theta_0_saturated = CONFIG.MAX_THETA;
	}
	else if(theta_0 < CONFIG.MIN_THETA)
	{
		theta_0_saturated = CONFIG.MIN_THETA;
	}
	else
	{
		theta_0_saturated = theta_0;
	}
	
	// Saturate theta_1 if out of bounds
	if(theta_1 > CONFIG.MAX_THETA)
	{
		theta_1_saturated = CONFIG.MAX_THETA;
	}
	else if(theta_1 < CONFIG.MIN_THETA)
	{
		theta_1_saturated = CONFIG.MIN_THETA;
	}
	else
	{
		theta_1_saturated = theta_1;
	}
	
	// Solve the following equation for x:
	// theta - MIN_THETA = x - MIN_DUTY_CYCLE
	// -----------------   ------------------
	//   theta_range	    duty_cycle_range
	
	// Find ratio of input theta to the range
	// Offset theta = 0 such that -theta is at the lowest dutycyle
	float theta_ratio_0 = (theta_0_saturated - CONFIG.MIN_THETA) / theta_range; 
	
	float theta_ratio_1 = (theta_1_saturated - CONFIG.MIN_THETA) / theta_range;
	
	// Solve equation
	uint16_t value_0 = static_cast<uint16_t>(theta_ratio_0 * static_cast<float>(duty_cycle_range_0)) + CONFIG.SERVO_0.MIN_DUTY_CYCLE;
	
	uint16_t value_1 = static_cast<uint16_t>(theta_ratio_1 * static_cast<float>(duty_cycle_range_1)) + CONFIG.SERVO_1.MIN_DUTY_CYCLE;
	
	// Disable timer
	CONFIG.SERVO_0.TIMER->CR1 &= ~TIM_CR1_CEN;
	
	CONFIG.SERVO_1.TIMER->CR1 &= ~TIM_CR1_CEN;
	
	// Update Compare and Capture Registers
	CONFIG.SERVO_0.TIMER->CCR4 = value_0;
	
	CONFIG.SERVO_1.TIMER->CCR3 = value_1;
	
	// Enable timer
	CONFIG.SERVO_0.TIMER->CR1 |= TIM_CR1_CEN;
	
	CONFIG.SERVO_1.TIMER->CR1 |= TIM_CR1_CEN;
	
	// Update diagnostics
	unsigned char theta_0_name[] = "THETA_0";
	unsigned char theta_1_name[] = "THETA_1";
	diagnostics.update(theta_0_name, 7u, theta_0);
	diagnostics.update(theta_1_name, 7u, theta_1);
	
	float ccr_left = static_cast<float>(value_0);
	float ccr_right = static_cast<float>(value_1);
	unsigned char ccr2_name[] = "CCR_LEFT";
	unsigned char ccr3_name[] = "CCR_RIGHT";
	diagnostics.update(ccr2_name, 8u, ccr_left);
	diagnostics.update(ccr3_name, 8u, ccr_right);
	
	return true;
}

void servo::Servo::disableAndReset()
{
	updateTheta(0.0f, 0.0f); // Set wheels straight
	
	uint64_t current_timestamp, past_timestamp;
	current_timestamp = getTimestamp();
	past_timestamp = current_timestamp;
	
	// Give servos time to adjust
	while(static_cast<float>(current_timestamp - past_timestamp) * configTimestamp::TICKS_TO_SECONDS < 1.0f)
	{
		current_timestamp = getTimestamp();
	}
	
	// Disable timers for servos
	CONFIG.SERVO_0.TIMER->CR1 &= ~TIM_CR1_CEN;
	
	CONFIG.SERVO_1.TIMER->CR1 &= ~TIM_CR1_CEN;
}

servo::Servo::~Servo()
{

}

#ifdef SERVO_UNIT_TEST
int main()
{
	timestampInitialize();
	servo::Servo motor = servo::Servo(configServo::CONFIG_DEFAULT);
	float theta_0 = configServo::CONFIG_DEFAULT.MIN_THETA;
	float theta_1 = configServo::CONFIG_DEFAULT.MIN_THETA;
	
	uint64_t current_timestamp, past_timestamp;
	current_timestamp = getTimestamp();
	past_timestamp = current_timestamp;
	
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN; //PA14 for green LED
	GPIOA->MODER |= 0x10000000u; // Set as output
	GPIOA->OTYPER |= 0x00004000u; // Set at open drain
	GPIOA->ODR |= 0x00004000u;
	
	while(1)
	{
		motor.updateTheta(theta_0, theta_1);
		current_timestamp = getTimestamp();
		
		if(static_cast<float>(current_timestamp - past_timestamp) * configTimestamp::TICKS_TO_SECONDS > 1.0f)
		{
			theta_0 += (configServo::CONFIG_DEFAULT.MAX_THETA - configServo::CONFIG_DEFAULT.MIN_THETA) / 4.0f;
			theta_1 += (configServo::CONFIG_DEFAULT.MAX_THETA - configServo::CONFIG_DEFAULT.MIN_THETA) / 4.0f;
			
			if(theta_0 >= configServo::CONFIG_DEFAULT.MAX_THETA)
			{
				theta_0 = configServo::CONFIG_DEFAULT.MAX_THETA;
			}
			
			if(theta_1 >= configServo::CONFIG_DEFAULT.MAX_THETA)
			{
				theta_1 = configServo::CONFIG_DEFAULT.MAX_THETA;
			}
			past_timestamp = current_timestamp;
		}
	}
	
	return 0;
}
#endif

