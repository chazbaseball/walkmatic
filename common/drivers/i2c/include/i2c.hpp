#ifndef I2C_HPP
#define I2C_HPP
#include "../../../common.hpp"
#include "../../../diagnostics/include/diagnostics.hpp"

namespace i2c
{
    class I2c
    {
        private:
			diagnostics::Diagnostics diagnostics;
			const configI2c::Config CONFIG;
			float dummy_diagnostics;
			uint32_t count;
            void configureGPIO();
            void initializeI2c();
			bool genStart(const uint8_t address);
			void genStop();
        public:
            I2c(const configI2c::Config & config);
            bool Send(const uint8_t address, const uint8_t* const input, const uint8_t & len, bool gen_stop);
			bool Receive(const uint8_t address, uint8_t* const input, const uint8_t & len);
    };
}
#endif

