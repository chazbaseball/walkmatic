#include "../include/i2c.hpp"

// Constructor
i2c::I2c::I2c(const configI2c::Config & config) : diagnostics(config.DIAGNOSITCS_CONFIG),
												  CONFIG(config)
{
	dummy_diagnostics = 0.0f;
	
	// Enable I2c clock (I2c is automatically coneccted to APB clock, pg. 863 of manual)
	g_RCC->APB1ENR |= CONFIG.I2C_CLOCK_ENABLE;
	
	configureGPIO();
	initializeI2c();
}

void i2c::I2c::configureGPIO()
{
	g_RCC->AHB1ENR |= CONFIG.GPIO_CLOCK_ENABLE_SDA;
    g_RCC->AHB1ENR |= CONFIG.GPIO_CLOCK_ENABLE_SCL;
	
    // Clear and then set SDA GPIO pin to Alternate Function
    uint32_t clear_MODER = ~(0x00000003u << (CONFIG.PIN_SDA * 2u)); // Clears 2 bits for setting mode
	CONFIG.GPIO_SDA->MODER &= clear_MODER;

    uint32_t set_MODER = (0x00000002u << (CONFIG.PIN_SDA * 2u)); // Set to alternate function
	CONFIG.GPIO_SDA->MODER |= set_MODER;

    // Clear and then set SCL GPIO pin to Alternate Function
    clear_MODER = ~(0x00000003u << (CONFIG.PIN_SCL * 2u)); // Clears 2 bits for setting mode
    CONFIG.GPIO_SCL->MODER &= clear_MODER;

    set_MODER = (0x00000002u << (CONFIG.PIN_SCL * 2u)); // Set to alternate function
    CONFIG.GPIO_SCL->MODER |= set_MODER;
    
	// Clear and then set SDA GPIO pin to I2C
    uint32_t clear_AFR = ~(0x0000000Fu << ((CONFIG.PIN_SDA % 8u) * 4u)); // Each pin has 4 bits
	CONFIG.GPIO_SDA->AFR[CONFIG.PIN_SDA/ 8u] &= clear_AFR;

    uint32_t set_AFR = CONFIG.AF_SDA << ((CONFIG.PIN_SDA % 8u) * 4u);
    CONFIG.GPIO_SDA->AFR[CONFIG.PIN_SDA / 8u] |= set_AFR;

    // Clear and then set SCL GPIO pin to I2C
    clear_AFR = ~(0x0000000Fu << ((CONFIG.PIN_SCL % 8u) * 4u)); // Each pin has 4 bits
	CONFIG.GPIO_SCL->AFR[CONFIG.PIN_SCL / 8u] &= clear_AFR;

    set_AFR = CONFIG.AF_SCL << ((CONFIG.PIN_SCL % 8u) * 4u);
    CONFIG.GPIO_SCL->AFR[CONFIG.PIN_SCL / 8u] |= set_AFR;

    // Set SDA and SCL GPIO pins to Very High Speed (0b11)
	CONFIG.GPIO_SDA->OSPEEDR |= (0x00000003u << (CONFIG.PIN_SDA * 2u));
    CONFIG.GPIO_SCL->OSPEEDR |= (0x00000003u << (CONFIG.PIN_SCL * 2u));

    // Set SDA GPIO pin to disable Pull-Up (0b00)
    uint32_t clear_PUPDR = ~(0x00000003u << (CONFIG.PIN_SDA * 2u)); // Clears 2 bits for setting mode
	CONFIG.GPIO_SDA->PUPDR &= clear_PUPDR;

    // Set SCL GPIO pin to disable Pull-Up (0b00)
    clear_PUPDR = ~(0x00000003u << (CONFIG.PIN_SCL * 2u)); // Clears 2 bits for setting mode
	CONFIG.GPIO_SCL->PUPDR &= clear_PUPDR;
	
    // Set SDA GPIO pin as Open-drain (0b1)
	CONFIG.GPIO_SDA->OTYPER &= ~(0x00000001U << (CONFIG.PIN_SDA));
	CONFIG.GPIO_SDA->OTYPER |= (0x00000001U << (CONFIG.PIN_SDA));
	
	// Set SCL GPIO pin as Open-drain
    CONFIG.GPIO_SCL->OTYPER &= ~(0x00000001U << (CONFIG.PIN_SCL));
	CONFIG.GPIO_SCL->OTYPER |= (0x00000001U << (CONFIG.PIN_SCL));
}

void i2c::I2c::initializeI2c()
{
	CONFIG.I2C->CR1 &= ~I2C_CR1_PE; // Disable I2C
	
	// Reset I2C
	CONFIG.I2C->CR1 |= I2C_CR1_SWRST;
	CONFIG.I2C->CR1 &= ~I2C_CR1_SWRST;
	
	// Select I2C clock frequency
	CONFIG.I2C->CR2 = CONFIG.I2C_FREQ & I2C_CR2_FREQ; 
	
	// Setup Master SCL frequency
	CONFIG.I2C->CCR &= I2C_CCR_CCR_Msk; // Reset CCR
	CONFIG.I2C->CCR |= CONFIG.I2C_CCR; // Set CCR
	
	// Setup maximum rise time for SCL
	CONFIG.I2C->TRISE &= ~I2C_TRISE_TRISE;
	CONFIG.I2C->TRISE |= CONFIG.I2C_TRISE;
	
	CONFIG.I2C->CR1 &= ~I2C_CR1_SMBUS; // 0 = I2C mode
	CONFIG.I2C->CR1 &= ~I2C_CR1_NOSTRETCH; // Enable clock streching -> connect with slower slaves

	// No OAR setting needed -> STM32F407 will always be a master in our application
	
	// Keep in master mode -> do not set NACK
	
	CONFIG.I2C->CR1 |= I2C_CR1_PE; // Enable I2C
}

bool i2c::I2c::genStart(const uint8_t address)
{
	uint32_t dummy; // For reading from registers to clear bits
	
	// Generate start bit (Sets the STM32F407 in Master Mode)
	CONFIG.I2C->CR1 |= I2C_CR1_START;

	// Do not write to DR until Start Bit has been generated
	count = 0u;
	while((CONFIG.I2C->SR1 & I2C_SR1_SB) ? false : true)
	{
		if(count > CONFIG.RESET_COUNT)
		{
			unsigned char message[] = "ERROR: SB NOT GENERATED";
			uint8_t message_length = 23u;
			diagnostics.update(message, message_length, dummy_diagnostics);
			initializeI2c();
			return false;
		}
		count++;
	}
	
	// Clear SB bit (after write to DR below)
	dummy = CONFIG.I2C->SR1;
	
	// Write slave address to data register
	CONFIG.I2C->DR = address & I2C_DR_DR;
	
	// The start bit is cleared after read from SR1 and write to DR

	// Only continue if address has been sent
	count = 0u;
	while((CONFIG.I2C->SR1 & I2C_SR1_ADDR) ? false : true)
	{
		if(count > CONFIG.RESET_COUNT)
		{
			unsigned char message[] = "ERROR: ADDR NOT SENT";
			uint8_t message_length = 20u;
			diagnostics.update(message, message_length, dummy_diagnostics);
			initializeI2c();
			return false;
		}
		count++;
	}
	
	// Clear ADDR bit
	dummy = CONFIG.I2C->SR2; 
	
	return true;
}

void i2c::I2c::genStop()
{
	// Generate a stop bit after current byte has been transferred
	CONFIG.I2C->CR1 |= I2C_CR1_STOP; 
	
	// The ST32F407 will always be master, checking STOPF only applies to slaves
}

bool i2c::I2c::Send(const uint8_t address, const uint8_t* const input, const uint8_t & len, bool gen_stop)
{
	// See pg. 846 in manual for more details
	count = 0u;
	while((CONFIG.I2C->SR2 & I2C_SR2_BUSY) == I2C_SR2_BUSY) // If the line is busy, wait
	{
		if(count > CONFIG.RESET_COUNT)
		{
			unsigned char message[] = "ERROR: BUS STAYING BUSY";
			uint8_t message_length = 23u;
			diagnostics.update(message, message_length, dummy_diagnostics);
			initializeI2c();
			return false;
		}
		count++;
	}

	if(!genStart(address))
	{
		return false;
	}
	
	for(uint8_t i = 0; i < len; i++)
	{
		count = 0u;
		while((CONFIG.I2C->SR1 & I2C_SR1_TXE) == 0) // Wait for data previously in DR to be sent (TXE = 1)
		{
			if(count > CONFIG.RESET_COUNT)
			{
				unsigned char message[] = "ERROR: TX, DR NOT EMPTY";
				uint8_t message_length = 23u;
				diagnostics.update(message, message_length, dummy_diagnostics);
				initializeI2c();
				return false;
			}
			count++;
		}
		CONFIG.I2C->DR = input[i] & I2C_DR_DR; // Write data to DR
	}
	
	count = 0u;
	while((CONFIG.I2C->SR1 & I2C_SR1_TXE) == 0) // Wait for data previously in DR to be sent (TXE = 1)
	{
		if(count > CONFIG.RESET_COUNT)
		{
			unsigned char message[] = "ERROR: TX, DR NOT EMPTY";
			uint8_t message_length = 23u;
			diagnostics.update(message, message_length, dummy_diagnostics);
			initializeI2c();
			return false;
		}
		count++;
	}
	
	
	if(gen_stop)
	{
		genStop();
	}
	
	return true;
}

bool i2c::I2c::Receive(const uint8_t address, uint8_t* const input, const uint8_t & len)
{
	CONFIG.I2C->CR1 |= I2C_CR1_POS; // When the ACK bit is set, an ACK is set after the next byte is received
	if(!genStart(address))
	{
		return false;
	}
	
	CONFIG.I2C->CR1 |= I2C_CR1_ACK; // Send acknowledge bit until 2nd to last bit is received
	
	for(uint8_t i = 0; i < len; i++)
	{
		if(i == len - 1)
		{
			CONFIG.I2C->CR1 &= ~I2C_CR1_ACK; // Do not send acknowledge bit after last byte is sent
		}
		
		count = 0u;
		while((CONFIG.I2C->SR1 & I2C_SR1_RXNE) == 0) // Wait for data to be placed in DR (RXNE = 1)
		{
			if(count > CONFIG.RESET_COUNT)
			{
				unsigned char message[] = "ERROR: RX, DR EMPTY";
				uint8_t message_length = 19u;
				diagnostics.update(message, message_length, dummy_diagnostics);
				initializeI2c();
				return false;
			}
			count++;
		}
		input[i] = static_cast<uint8_t>(CONFIG.I2C->DR & I2C_DR_DR); // Read data from DR
	}
	
	count = 0u;
	while((CONFIG.I2C->SR1 & I2C_SR1_RXNE) == 0) // Wait for DR to be empty (RXNE = 0)
	{
		if(count > CONFIG.RESET_COUNT)
		{
			unsigned char message[] = "ERROR: RX, DR NOT EMPTY";
			uint8_t message_length = 23u;
			diagnostics.update(message, message_length, dummy_diagnostics);
			initializeI2c();
			return false;
		}
		count++;
	}
	
	genStop();
	CONFIG.I2C->CR1 &= ~I2C_CR1_POS;

	return true;
}

#ifdef I2C_UNIT_TEST

int main()
{		
	timestampInitialize();
	diagnostics::Diagnostics DeviceDiagnostics(IMU);
	unsigned char name[] = "TEMPERATURE";
	unsigned char time[] = "TIMESTAMP";
	i2c::I2c Sensor;
	uint8_t imu_address = 0x28;
	imu_address = imu_address << 1u; // R/W bit set to 0
	uint8_t imu_address_rec = imu_address + 1; // R/W bit set to 1
	
	// Set Operational Mode to enable ACC, GYR, MAG
	uint8_t opr_address = 0x3D; // OPR_MODE Register
	uint8_t mode = 0x07;
	uint8_t set_opr[2] = {opr_address, mode};
	Sensor.Send(imu_address, set_opr, 2, true);
	
	// Set which register to read from
	uint8_t read_address = 0x34;
	uint8_t data[1] = {read_address}; // input data
	uint8_t output_data[1];
	 
    while (1)
    {
		bool result = true;
		while(result)
		{
			Sensor.Send(imu_address, set_opr, 2, true);
			bool successful_write = Sensor.Send(imu_address, data, 1, false);
			bool successful_read = Sensor.Receive(imu_address_rec, output_data, 1);
			result = !(successful_write && successful_read);
		}
		float sec = static_cast<float>(getTimestamp()) * configTimestamp::TICKS_TO_SECONDS;
		DeviceDiagnostics.update(name, 11, static_cast<float>(output_data[0]));
		DeviceDiagnostics.update(time, 9, sec);
    }
}
#endif

