# uart #
This module is responsible for printing strings to a PuTTY terminal over UART. This is a nonfunctional module used only for debugging.

![](../../../diagrams/uart.png)