#ifndef LCD_H
#define LCD_H
#include "../../../common.hpp"
#include "../../spi/include/spi.hpp"

namespace lcd
{
    class Lcd
    {
        private:
			bool displayArray[configPlotter::ARRAY_SIZE];
			int currentPage;
			int currentColumn;
			uint8_t commandOn;
			uint8_t commandOff;
			uint8_t commandDispStartLine;
			uint8_t commandPageAddr;
			uint16_t commandColAddr;
			uint8_t commandStatus;
			uint8_t commandDataWrite;
			uint8_t commandReadModifyWrite;
			uint8_t commandEnd;
			spi::Spi deviceSpi;
        public:
			Lcd();
			void updateDisplay(const bool * const array);
    };
}

#endif

