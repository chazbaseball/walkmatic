#ifndef MODE_MANAGER_HPP
#define MODE_MANAGER_HPP
#include "../../common/common.hpp"
#include "../../common/diagnostics/include/diagnostics.hpp"
#include "../../common/buzzer/include/buzzer.hpp"

namespace modemanager
{
	enum InterruptType
	{
		RISING, // Rising edge
		FALLING, // Falling edge
		RISE_FALL // Rising and Falling edge
	};
	
	enum Mode
	{
		INITIAL,
		RECORD,
		READY,
		PLAY,
		CHARGE
	};
	
	enum InputSignal
	{
		RECORD_SIGNAL,
		READY_SIGNAL,
		PLUGGED_IN,
		CELL_1_BAL_L,
		CELL_2_BAL_L,
		CELL_3_BAL_L,
		CELL_4_BAL_L,
		CHARGE_EN
	};
	
	// Diagnostics messages and lengths
	static uint8_t MESSAGE_CELL_1_BAL_EN[] = "CELL_1_BAL_EN";
	static uint8_t MESSAGE_CELL_1_BAL_EN_LENGTH = 13u;
	static uint8_t MESSAGE_CELL_2_BAL_EN[] = "CELL_2_BAL_EN";
	static uint8_t MESSAGE_CELL_2_BAL_EN_LENGTH = 13u;
	static uint8_t MESSAGE_CELL_3_BAL_EN[] = "CELL_3_BAL_EN";
	static uint8_t MESSAGE_CELL_3_BAL_EN_LENGTH = 13u;
	static uint8_t MESSAGE_CELL_4_BAL_EN[] = "CELL_4_BAL_EN";
	static uint8_t MESSAGE_CELL_4_BAL_EN_LENGTH = 13u;
    static uint8_t MESSAGE_CELL_1_BAL_L[] = "CELL_1_BAL_L";
	static uint8_t MESSAGE_CELL_1_BAL_L_LENGTH = 12u;
	static uint8_t MESSAGE_CELL_2_BAL_L[] = "CELL_2_BAL_L";
	static uint8_t MESSAGE_CELL_2_BAL_L_LENGTH = 12u;
	static uint8_t MESSAGE_CELL_3_BAL_L[] = "CELL_3_BAL_L";
	static uint8_t MESSAGE_CELL_3_BAL_L_LENGTH = 12u;
	static uint8_t MESSAGE_CELL_4_BAL_L[] = "CELL_4_BAL_L";
	static uint8_t MESSAGE_CELL_4_BAL_L_LENGTH = 12u;
	static uint8_t MESSAGE_PLUGGED_IN[] = "PLUGGED_IN";
	static uint8_t MESSAGE_PLUGGED_IN_LENGTH = 10u;
	static uint8_t MESSAGE_MODE_RECORD[] = "RECORD MODE";
	static uint8_t MESSAGE_MODE_RECORD_LENGTH = 11u;
	static uint8_t MESSAGE_MODE_PLAY[] = "PLAY MODE";
	static uint8_t MESSAGE_MODE_PLAY_LENGTH = 9u;
	static uint8_t MESSAGE_MODE_READY[] = "READY MODE";
	static uint8_t MESSAGE_MODE_READY_LENGTH = 10u;
	static uint8_t MESSAGE_MODE_CHARGE[] = "CHARGE MODE";
	static uint8_t MESSAGE_MODE_CHARGE_LENGTH = 11u;
	static uint8_t MESSAGE_MODE_INITIAL[] = "INITIAL MODE";
	static uint8_t MESSAGE_MODE_INITIAL_LENGTH = 12u;
	static uint8_t MESSAGE_RECORD_SIGNAL[] = "RECORD SIGNAL";
	static uint8_t MESSAGE_RECORD_SIGNAL_LENGTH = 13u;
	static uint8_t MESSAGE_READY_SIGNAL[] = "READY SIGNAL";
	static uint8_t MESSAGE_READY_SIGNAL_LENGTH = 12u;
	
    class ModeManager
    {			
        private:
			struct SystemSignal
			{
				Mode mode = INITIAL;
				bool record = false;
				bool ready = false;
				bool play = false;
				bool plugged_in = false;
				bool cell_1_bal_en = false;
				bool cell_2_bal_en = false;
				bool cell_3_bal_en = false;
				bool cell_4_bal_en = false;
				bool cell_1_bal_l = false;
				bool cell_2_bal_l = false;
				bool cell_3_bal_l = false;
				bool cell_4_bal_l = false;
				bool charge_en = false;
				bool normal_operation = true;		   
			} system_signals;
			const configModeManager::Config CONFIG;
			// Add buzzer object
			diagnostics::Diagnostics diagnostics;
			buzzer::Buzzer buzzer;
			void configureGPIOInput(const configModeManager::SignalConfig & config_signal);
			void configureGPIOInputPullUp(const configModeManager::SignalConfig & config_signal);
			void initializeInterrupt(const configModeManager::InterruptConfig & config_inter, const InterruptType & type);
			void configureGPIOOutput(const configModeManager::SignalConfig & config_signal);
			void updateChargeEn();
			void updateNormalOperation();
			bool updateSignal(const configModeManager::SignalConfig & config_signal);
			bool disableOutput(const configModeManager::SignalConfig & config_signal);
			bool enableOutput(const configModeManager::SignalConfig & config_signal);
			float dummy;
			bool rising_edge_detected;
        public:
            ModeManager(const configModeManager::Config & config);
			bool getSignal(InputSignal signal);
			Mode getMode();
			void printMode();
			void setRisingEdgeIndicator(bool enable);
			void configureTimer5(const configModeManager::ChargeTimerConfig & config_timer);
			Mode updateMode();
			void checkCellChargeLevel();
            ~ModeManager();
    };
}

#endif
