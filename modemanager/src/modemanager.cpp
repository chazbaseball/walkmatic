#include "../include/modemanager.hpp"

// Constructor
modemanager::ModeManager::ModeManager(const configModeManager::Config & config) : CONFIG(config),
																				  diagnostics(config.DIAGNOSTICS_CONFIG),
																				  buzzer(config.BUZZER_CONFIG)
{
	dummy = 0.0f;
  g_RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN; // Enable SYSCFG clock

	configureGPIOOutput(CONFIG.CHARGE_EN); // Initialize CHARGE_EN signal
	configureGPIOInput(CONFIG.READY); // Initialize Input Signals
	initializeInterrupt(CONFIG.READY_INT, RISING);
	configureTimer5(CONFIG.TIMER);
	configureGPIOInput(CONFIG.RECORD);
	configureGPIOInput(CONFIG.PLUGGED_IN);
	configureGPIOOutput(CONFIG.CELL_1_BAL_EN);
	configureGPIOOutput(CONFIG.CELL_2_BAL_EN);
	configureGPIOOutput(CONFIG.CELL_3_BAL_EN);
	configureGPIOOutput(CONFIG.CELL_4_BAL_EN);
	configureGPIOInputPullUp(CONFIG.CELL_1_BAL_L);
	configureGPIOInputPullUp(CONFIG.CELL_2_BAL_L);
	configureGPIOInputPullUp(CONFIG.CELL_3_BAL_L);
	configureGPIOInputPullUp(CONFIG.CELL_4_BAL_L);
	system_signals.cell_1_bal_en = disableOutput(CONFIG.CELL_1_BAL_EN);
	system_signals.cell_2_bal_en = disableOutput(CONFIG.CELL_2_BAL_EN);
	system_signals.cell_3_bal_en = disableOutput(CONFIG.CELL_3_BAL_EN);
	system_signals.cell_4_bal_en = disableOutput(CONFIG.CELL_4_BAL_EN);	
	rising_edge_detected = false;
	
	diagnostics.update(MESSAGE_MODE_INITIAL, MESSAGE_MODE_INITIAL_LENGTH, dummy);
}

void modemanager::ModeManager::initializeInterrupt(const configModeManager::InterruptConfig & config_inter, const InterruptType & type)
{
    // Select source for interrupt
    g_SYSCFG->EXTICR[config_inter.EXTI_CR] &= ~config_inter.SYSCFG_EXTI_CLEAR;
    g_SYSCFG->EXTICR[config_inter.EXTI_CR] |= config_inter.SYSCFG_EXTI_SET;

    // Set trigger edge
	switch(type)
	{
		case RISING:
			g_EXTI->RTSR |= config_inter.EXTI_RT; // Enable rising edge trigger
			g_EXTI->FTSR &= ~config_inter.EXTI_FT; // Disable falling edge trigger
			break;
		case FALLING:
			g_EXTI->RTSR &= ~config_inter.EXTI_RT; // Disable rising edge trigger
			g_EXTI->FTSR |= config_inter.EXTI_FT; // Enable falling edge trigger
			break;
		case RISE_FALL:
			g_EXTI->RTSR |= config_inter.EXTI_RT; // Enable rising edge trigger
			g_EXTI->FTSR |= config_inter.EXTI_FT; // Enable falling edge trigger
			break;
		default:
			break;
	}

    g_EXTI->IMR |= config_inter.EXTI_IM; // Enable interrupt

    NVIC_SetPriority(config_inter.EXTI_TYPE, config_inter.PRIORITY);
	
	NVIC_EnableIRQ(config_inter.EXTI_TYPE);
}

void modemanager::ModeManager::configureTimer5(const configModeManager::ChargeTimerConfig & config_timer)
{
		// Enable timer 5 clock
	  g_RCC->APB1ENR |= config_timer.TIMER_5_CLOCK_ENABLE;
	
	  // Set timer 5 count direction to up
		config_timer.TIMER_5->CR1 &= ~TIM_CR1_DIR;
	
	  // Set the prescaler to
	  config_timer.TIMER_5->PSC = config_timer.PRESCALER;
	
		// Set the auto-reload value to 
	  config_timer.TIMER_5->ARR = config_timer.AUTO_RELOAD;
	
		// Disable timer 5 preload requirement (CCMR1 bit 3)
	  config_timer.TIMER_5->CCMR1 &= ~TIM_CCMR1_OC1PE;
	
		// Set CCR1 Value
	  config_timer.TIMER_5->CCR1 = config_timer.CAPTURE_VALUE;
	
	  // Set the CC1IE bit to enable interrupts on channel 1 (May need to set in EGR register as well)
	  config_timer.TIMER_5->DIER = TIM_DIER_CC1IE;
	
	  // Set capture/compare selection to output (CCMR1 bits 1:0 = 00)
	  config_timer.TIMER_5->CCMR1 &= ~TIM_CCMR1_CC1S;
		
		// Set channel 1 to active compare on match (bit 4)
		config_timer.TIMER_5->CCMR1 |= TIM_CCMR1_OC1M_0;

		// Set the output clear enable bit (bit 7) for channel 1
		config_timer.TIMER_5->CCMR1 |= TIM_CCMR1_OC1CE;
		
		// Enable the Main output enable bit
		config_timer.TIMER_5->BDTR |= (TIM_BDTR_MOE || TIM_BDTR_AOE);
		
		// Enable interrupt in NVIC table
		NVIC_SetPriority(TIM5_IRQn,config_timer.PRIORITY);
		NVIC_EnableIRQ(TIM5_IRQn);
		
		// Enable the counter (CEN bit in CR1 (bit 0))
		config_timer.TIMER_5->CR1 |= TIM_CR1_CEN;
}

void modemanager::ModeManager::configureGPIOInput(const configModeManager::SignalConfig & config_signal)
{
	// Enable GPIO clock
	g_RCC->AHB1ENR |= config_signal.GPIO_CLOCK_ENABLE;
	
	// Clear and then set GPIO pin to Input mode (0b00)
    config_signal.GPIO->MODER &= ~(0x00000003u << (config_signal.PIN * 2u)); // Clears 2 bits for setting mode
	
	// Set SCL GPIO pin to clear and set to no pull-up or pull-down (pulled down externally)
    config_signal.GPIO->PUPDR &= ~(0x00000003u << (config_signal.PIN * 2u)); // Clears 2 bits for setting mode
}

void modemanager::ModeManager::configureGPIOInputPullUp(const configModeManager::SignalConfig & config_signal)
{
	// Enable GPIO clock
	g_RCC->AHB1ENR |= config_signal.GPIO_CLOCK_ENABLE;
	
	// Clear and then set GPIO pin to Input mode (0b00)
    config_signal.GPIO->MODER &= ~(0x00000003u << (config_signal.PIN * 2u)); // Clears 2 bits for setting mode
	
	// Set SCL GPIO pin to clear and set to no pull-up or pull-down (pulled down externally)
    config_signal.GPIO->PUPDR &= ~(0x00000003u << (config_signal.PIN * 2u)); // Clears 2 bits for setting mode
	  config_signal.GPIO->PUPDR |= (0x00000001u << (config_signal.PIN * 2u)); // Clears 2 bits for setting mode
}

void modemanager::ModeManager::configureGPIOOutput(const configModeManager::SignalConfig & config_signal)
{
	// Enable GPIO clock
	g_RCC->AHB1ENR |= config_signal.GPIO_CLOCK_ENABLE;
	
	// Clear and then set GPIO pin to Output mode (0b01)
    config_signal.GPIO->MODER &= ~(0x00000003u << (config_signal.PIN * 2u)); // Clears 2 bits for setting mode
    config_signal.GPIO->MODER |= (0x00000001u << (config_signal.PIN * 2u)); // Set to general purpose output mode
	
	// Set SCL GPIO pin to disable Pull-Up (0b00)
    config_signal.GPIO->PUPDR &= ~(0x00000003u << (config_signal.PIN * 2u)); // Clears 2 bits for setting mode
	
	// Set SCL GPIO pin as Push-pull (0b0)
    config_signal.GPIO->OTYPER &= ~(0x00000001U << (config_signal.PIN));
}

void modemanager::ModeManager::updateChargeEn()
{
	system_signals.charge_en = !(system_signals.cell_1_bal_en || system_signals.cell_2_bal_en || system_signals.cell_3_bal_en || system_signals.cell_4_bal_en) && system_signals.plugged_in;
	if(system_signals.charge_en)
	{
		CONFIG.CHARGE_EN.GPIO->ODR |= (0x00000001u << CONFIG.CHARGE_EN.PIN);
		CONFIG.TIMER.TIMER_5->CR1 |= TIM_CR1_CEN;
	}
	else
	{
		CONFIG.CHARGE_EN.GPIO->ODR &= ~(0x00000001u << CONFIG.CHARGE_EN.PIN);
		//CONFIG.TIMER.TIMER_5->CR1 &= ~TIM_CR1_CEN;
	}
}

void modemanager::ModeManager::updateNormalOperation()
{
	system_signals.normal_operation = !system_signals.plugged_in && !system_signals.charge_en;
}

void modemanager::ModeManager::setRisingEdgeIndicator(bool enable)
{
	rising_edge_detected = enable;
}

modemanager::Mode modemanager::ModeManager::updateMode()
{
	//CONFIG.CHARGE_EN.GPIO->ODR |= (0x00000001u << CONFIG.CHARGE_EN.PIN);
	for(uint32_t i = 0; i < CONFIG.DEBOUNCE_COUNT; i++);
	
	system_signals.record = updateSignal(CONFIG.RECORD);
	system_signals.play = !system_signals.record;
	system_signals.ready = rising_edge_detected;
	bool store_detected = rising_edge_detected;
	system_signals.plugged_in = updateSignal(CONFIG.PLUGGED_IN);
	updateChargeEn();
	updateNormalOperation();
	
	// Walkmatic State Machine
	switch(system_signals.mode)
	{
		case INITIAL:
			if(system_signals.plugged_in)
			{
				system_signals.mode = CHARGE;
				diagnostics.update(MESSAGE_MODE_CHARGE, MESSAGE_MODE_CHARGE_LENGTH, dummy);
				buzzer.playCharge();
			}
			else if(system_signals.record)
			{
				system_signals.mode = RECORD;
				diagnostics.update(MESSAGE_MODE_RECORD, MESSAGE_MODE_RECORD_LENGTH, dummy);
				buzzer.playRecord();
			}
			else
			{
				system_signals.mode = INITIAL;
			}
			break;
		case RECORD:
			if(system_signals.plugged_in)
			{
				system_signals.mode = CHARGE;
				diagnostics.update(MESSAGE_MODE_CHARGE, MESSAGE_MODE_CHARGE_LENGTH, dummy);
				buzzer.playCharge();
			}
			else if(system_signals.record && system_signals.ready)
			{
				system_signals.mode = READY;
				diagnostics.update(MESSAGE_MODE_READY, MESSAGE_MODE_READY_LENGTH, dummy);
				buzzer.playReady();
			}
			else
			{
				system_signals.mode = RECORD;
			}
			break;
		case READY:
			if(system_signals.plugged_in)
			{
				system_signals.mode = CHARGE;
				diagnostics.update(MESSAGE_MODE_CHARGE, MESSAGE_MODE_CHARGE_LENGTH, dummy);
				buzzer.playCharge();
			}
			else if(system_signals.record && system_signals.ready)
			{
				system_signals.mode = RECORD;
				diagnostics.update(MESSAGE_MODE_RECORD, MESSAGE_MODE_RECORD_LENGTH, dummy);
				buzzer.playRecord();
			}
			else if(!system_signals.record && !system_signals.ready)
			{
				system_signals.mode = PLAY;
				diagnostics.update(MESSAGE_MODE_PLAY, MESSAGE_MODE_PLAY_LENGTH, dummy);
				buzzer.playPlay();
			}
			else
			{
				system_signals.mode = READY;
			}
			break;
		case PLAY:
			if(system_signals.plugged_in)
			{
				system_signals.mode = CHARGE;
				diagnostics.update(MESSAGE_MODE_CHARGE, MESSAGE_MODE_CHARGE_LENGTH, dummy);
				buzzer.playCharge();
			}
			else if(system_signals.record)
			{
				system_signals.mode = RECORD;
				diagnostics.update(MESSAGE_MODE_RECORD, MESSAGE_MODE_RECORD_LENGTH, dummy);
				buzzer.playRecord();
			}
			else
			{
				system_signals.mode = PLAY;
			}
			break;
		case CHARGE:
			if(!system_signals.charge_en)
			{
				if (system_signals.plugged_in)
				{
					system_signals.mode = CHARGE;
					CONFIG.TIMER.TIMER_5->CR1 |= TIM_CR1_CEN;
				}
				else if(system_signals.ready && system_signals.record)
				{
					system_signals.mode = RECORD;
					system_signals.cell_1_bal_en = disableOutput(CONFIG.CELL_1_BAL_EN);
					system_signals.cell_2_bal_en = disableOutput(CONFIG.CELL_2_BAL_EN);
					system_signals.cell_3_bal_en = disableOutput(CONFIG.CELL_3_BAL_EN);
					system_signals.cell_4_bal_en = disableOutput(CONFIG.CELL_4_BAL_EN);
					diagnostics.update(MESSAGE_MODE_RECORD, MESSAGE_MODE_RECORD_LENGTH, dummy);
					CONFIG.TIMER.TIMER_5->CR1 &= ~TIM_CR1_CEN;
					buzzer.playRecord();
				}
			}
			else
			{
				system_signals.mode = CHARGE;
				CONFIG.TIMER.TIMER_5->CR1 |= TIM_CR1_CEN;
			}
			break;
		default:
			break;
	}

	rising_edge_detected = (rising_edge_detected != store_detected) ? rising_edge_detected : false;
	return system_signals.mode;
}

void modemanager::ModeManager::printMode()
{
	switch(system_signals.mode)
	{
		case INITIAL:
			diagnostics.update(MESSAGE_MODE_INITIAL, MESSAGE_MODE_INITIAL_LENGTH, dummy);
			break;
		case RECORD:
			diagnostics.update(MESSAGE_MODE_RECORD, MESSAGE_MODE_RECORD_LENGTH, dummy);
			break;
		case READY:
			diagnostics.update(MESSAGE_MODE_READY, MESSAGE_MODE_READY_LENGTH, dummy);
			break;
		case PLAY:
			diagnostics.update(MESSAGE_MODE_PLAY, MESSAGE_MODE_PLAY_LENGTH, dummy);
			break;
		case CHARGE:
			diagnostics.update(MESSAGE_MODE_CHARGE, MESSAGE_MODE_CHARGE_LENGTH, dummy);
			break;
		default:
			break;
	}
}

modemanager::Mode modemanager::ModeManager::getMode()
{
	return system_signals.mode;
}

bool modemanager::ModeManager::updateSignal(const configModeManager::SignalConfig & config_signal)
{
	bool output = (config_signal.GPIO->IDR & (0x00000001u << config_signal.PIN)) ? true : false;
	return output;
}

bool modemanager::ModeManager::disableOutput(const configModeManager::SignalConfig & config_signal)
{
	config_signal.GPIO->ODR &= ~(0x00000001u << config_signal.PIN);
	bool output = (config_signal.GPIO->ODR & (0x00000001u << config_signal.PIN)) ? true : false;
	output = output + 0;
	return output;
}

bool modemanager::ModeManager::enableOutput(const configModeManager::SignalConfig & config_signal)
{
	config_signal.GPIO->ODR |= (0x00000001u << config_signal.PIN);
	bool output = (config_signal.GPIO->ODR & (0x00000001u << config_signal.PIN)) ? true : false;
	return output;
}

bool modemanager::ModeManager::getSignal(InputSignal signal)
{
	switch(signal)
	{
		case RECORD_SIGNAL:
			return system_signals.record;
		case READY_SIGNAL:
			return system_signals.ready;
		case PLUGGED_IN:
			return system_signals.plugged_in;
		case CELL_1_BAL_L:
			return system_signals.cell_1_bal_l;
		case CELL_2_BAL_L:
			return system_signals.cell_2_bal_l;
		case CELL_3_BAL_L:
			return system_signals.cell_3_bal_l;
		case CELL_4_BAL_L:
			return system_signals.cell_4_bal_l;
		case CHARGE_EN:
			return system_signals.charge_en;
		default:
			break;
	}
}

void modemanager::ModeManager::checkCellChargeLevel()
{
	uint64_t start, time;
	// Disable Charging
	system_signals.charge_en = false;
	CONFIG.CHARGE_EN.GPIO->ODR &= ~(0x00000001u << CONFIG.CHARGE_EN.PIN);
	
	// Wait for battery steady state
	//for (uint32_t debounce = 0; debounce < CONFIG.DEBOUNCE_COUNT*10000; debounce++);
	
	// Disable Balancing
	system_signals.cell_1_bal_en = disableOutput(CONFIG.CELL_1_BAL_EN);
	system_signals.cell_2_bal_en = disableOutput(CONFIG.CELL_2_BAL_EN);
	system_signals.cell_3_bal_en = disableOutput(CONFIG.CELL_3_BAL_EN);
	system_signals.cell_4_bal_en = disableOutput(CONFIG.CELL_4_BAL_EN); 
	
	// Wait for comparator and battery steady state
	while (TIM5->CNT < 2000);
	
	// Check comparator outputs
	system_signals.cell_1_bal_l = updateSignal(CONFIG.CELL_1_BAL_L);
	system_signals.cell_2_bal_l = updateSignal(CONFIG.CELL_2_BAL_L);
	system_signals.cell_3_bal_l = updateSignal(CONFIG.CELL_3_BAL_L);
	system_signals.cell_4_bal_l = updateSignal(CONFIG.CELL_4_BAL_L);
	
	// Enable balancing between cells if needed
	if (!system_signals.cell_1_bal_l)
	{
		system_signals.cell_1_bal_l = updateSignal(CONFIG.CELL_1_BAL_L);
		if (!system_signals.cell_1_bal_l)
			system_signals.cell_1_bal_en = enableOutput(CONFIG.CELL_1_BAL_EN);
	}
	if (!system_signals.cell_2_bal_l)
	{
		if (!system_signals.cell_2_bal_l)
			system_signals.cell_2_bal_en = enableOutput(CONFIG.CELL_2_BAL_EN);
	}
	if (!system_signals.cell_3_bal_l)
	{
		if (!system_signals.cell_3_bal_l)
			system_signals.cell_3_bal_en = enableOutput(CONFIG.CELL_3_BAL_EN);
	}
	if (!system_signals.cell_4_bal_l)
	{
		if (!system_signals.cell_4_bal_l)
			system_signals.cell_4_bal_en = enableOutput(CONFIG.CELL_4_BAL_EN);
	}
	
	// Enable charging if none of the cells need to be balanced
	updateChargeEn();
}

// Deconstructor
modemanager::ModeManager::~ModeManager()
{

}

#ifdef MODE_MANAGER_UNIT_TEST
#include "../../common/interrupts/include/interrupts.hpp"

int main()
{
	timestampInitialize();
	modemanager::ModeManager* manager;
	manager = getModeManager();
	while(1)
	{
		manager->updateMode();
	}
}

#endif

