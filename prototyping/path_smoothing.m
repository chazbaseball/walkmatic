close all;
input_points = [0,0;
                0,1;
                0,2;
                0.2,1.8;
                0.4,1.6;
                0.6,1.4;
                0.8,1.2;
                1,1;
                2,3;
                3,4;
                3.2, 3.6;
                3.8, 3.1;
                4,3;
                3,2;
                2,1;
                0,0];

num_of_points = 15;

for i=1:num_of_points
    line([input_points(i,1), input_points(i+1,1)],[input_points(i,2), input_points(i+1,2)]);
    hold on;
end

filtered(1,1) = input_points(1,1);
filtered(1,2) = input_points(1,2);
beta = 0.6;
for i=2:num_of_points
    filtered(i,1) = beta * filtered(i-1,1) + (1 - beta) * input_points(i,1);
    filtered(i,2) = beta * filtered(i-1,2) + (1 - beta) * input_points(i,2);
end

filtered(num_of_points + 1,1) = input_points(num_of_points + 1,1);
filtered(num_of_points + 1,2) = input_points(num_of_points + 1,2);

for i=1:num_of_points
    line([filtered(i,1), filtered(i+1,1)],[filtered(i,2), filtered(i+1,2)]);
    hold on;
end