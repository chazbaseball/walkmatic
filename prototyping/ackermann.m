command_curvature = -3; % 1/m
turning_radius = 1 / command_curvature;
axle_length = 24;
axle_to_axle_length = 23;
scale_to_meters = 0.01;

front_left_wheel_location(2) = axle_length / 2.0 * scale_to_meters;
front_right_wheel_location(2) = -axle_length / 2.0 * scale_to_meters;
	
front_left_wheel_location(1) = axle_to_axle_length / 2.0 * scale_to_meters;
front_right_wheel_location(1) = axle_to_axle_length / 2.0 * scale_to_meters;

back_left_wheel_location(2) = front_left_wheel_location(2);
back_right_wheel_location(2) = front_right_wheel_location(2);

back_left_wheel_location(1) = 0;
back_left_wheel_location(1) = 0;

turning_center(1) = 0;
turning_center(2) = turning_radius;

curvature_left_wheel = 1 / norm(front_left_wheel_location - turning_center);
curvature_right_wheel = 1 / norm(front_right_wheel_location - turning_center);

if(turning_radius < 0)
    curvature_left_wheel = -curvature_left_wheel;
    curvature_right_wheel = -curvature_right_wheel;
end


left_adj = norm(turning_center - back_left_wheel_location);
right_adj = norm(turning_center - back_right_wheel_location);

theta_left = acos(curvature_left_wheel * left_adj);
theta_right = acos(curvature_right_wheel * right_adj);

if(theta_left > pi/2)
    theta_left = theta_left - pi;
end

if(theta_right > pi/2)
    theta_right = theta_right - pi;
end

