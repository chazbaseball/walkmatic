pause on;
close all;
% Create Boundary Box
y_bottom = -5;
x_left = -5;
y_top = 5;
x_right = 5;

boundary_box = [y_bottom, x_left, y_top, x_right];

line([x_left, y_bottom],[x_left, y_top]);
hold on;
line([x_left, y_top],[x_right, y_top]);
hold on;
line([x_right, y_top],[x_right, y_bottom]);
hold on;
line([x_right, y_bottom],[x_left, y_bottom]);
hold on;
xlim([x_left - 5, x_right + 5]);
ylim([y_bottom - 5, y_top + 5]);

% Create vehicle
vehicle = [7.5;5]; % x;y Starting location

% Create sensor unit vector wrt vehicle
sensor_1 = [0;100];
sensor_2 = [100;0];
sensor_3 = [-100;0];

yaw = 0;
yaw_incr = 0;
translation = [0;0];
past_centroid = [0;5];
sensor_1_vehicle_past = [0;0];
position = [0;5];
filtered_position = [0;0];
old_translation = [0;0];
old_yaw = 0;
curv = 0;
% Treat each iteration as one second
for i=0:200
    rot = [cos(yaw), -sin(yaw);
           sin(yaw),  cos(yaw)];
    
    % Apply attitude and position transformation
    sensor_1_world = rot * sensor_1 + translation;
    sensor_2_world = rot * sensor_2 + translation;
    sensor_3_world = rot * sensor_3 + translation;
    
    test = translation(1) > boundary_box(2) && translation(1) < boundary_box(4)
    test = translation(2) > boundary_box(1) && translation(2) < boundary_box(3) && test;
    
    scatter(translation(1), translation(2), 'black','filled');
    axis square;
    hold on;
    if(test)
        % Get position of sensor returns in world frame
        sensor_1_world = findPosition(sensor_1_world, translation, boundary_box);
        sensor_2_world = findPosition(sensor_2_world, translation, boundary_box);
        sensor_3_world = findPosition(sensor_3_world, translation, boundary_box);
    
        % Plot sensor returns
        scatter(sensor_1_world(1),sensor_1_world(2),'r');
        hold on;
        scatter(sensor_2_world(1),sensor_2_world(2),'g');
        hold on;
        scatter(sensor_3_world(1),sensor_3_world(2),'b');
        hold on;
    end
    
    % Implement ultrasonic localization algo
    sensor_1_vehicle = getPointInVehicleFrame(sensor_1_world, translation, 1);
    sensor_2_vehicle = getPointInVehicleFrame(sensor_2_world, translation, 2);
    sensor_3_vehicle = getPointInVehicleFrame(sensor_3_world, translation, 3);
    
    current_centroid = findUpdate(sensor_1_vehicle, sensor_2_vehicle, sensor_3_vehicle);
    
    % Option 1: Adding up all measurements...Does not work
    % position =  position + (past_centroid - current_centroid) .* [0.5;1];
    % past_centroid = current_centroid;
    
    % Option 2: Only using from ultrasonic and then rotating...Does not work
    past_position = position;
    position = rot * (sensor_1_vehicle_past - sensor_1_vehicle) + position;
    sensor_1_vehicle_past = sensor_1_vehicle;
    
    % Option 3: Curvature Complimentary filter...Works!
    curv_norm = curv / 10000;
    disp(curv_norm);
    new_pos = filtered_position + position - past_position;
    filtered_position = (1 - curv_norm) * translation + curv_norm * new_pos;
    
    scatter(filtered_position(1),filtered_position(2),'r','filled');

    pause(0.01); % Pause for visualization
    
    % Increment attitude and position
    % new_location = rot * [1;0]; % Pure rotation for updating translation
    % increment = new_location - translation;
    % increment = [0.00;0.03];
    old_translation = translation;
    old_yaw = yaw;
    
    if(i > 74 && i < 125)
        yaw = yaw + 2 * pi/100;
        new_location = rot * [1;0]; % Pure rotation for updating translation
        increment = new_location - translation;
        translation = translation + increment;
        
        if(i == 75)
            translation = translation + base_translation;
        end
    end
    
    if(i > 124)
        increment = [0.00;-0.03];
        translation = translation + increment;
    end
    
    ang_rate = yaw - old_yaw;
    vel = sqrt((old_translation(1) - translation(1))^2 + (old_translation(2) - translation(2))^2);
    
    if(abs(ang_rate) == 0)
        curv = 10000;
    else
        curv = vel / ang_rate;
    end
    curv = abs(curv);
end

function position = findPosition(sensor_point, vehicle_point, boundary_box)

    % Calculate slope
    m = (vehicle_point(2) - sensor_point(2)) / (vehicle_point(1) - sensor_point(1));
    x = sensor_point(1);
    y = sensor_point(2);
    
    % Test against boundaries
    test = (x < boundary_box(2)) && (boundary_box(2) < vehicle_point(1));
    
    if(test)
        x_old = x;
        y_old = y;
        x = boundary_box(2);
        y = m * (x - x_old) + y_old;
    end
    
    test = (x > boundary_box(4)) && (boundary_box(4) > vehicle_point(1));
    
    if(test)
        x_old = x;
        y_old = y;
        x = boundary_box(4);
        y = m * (x - x_old) + y_old;
    end
    
    test = (y > boundary_box(3)) && (boundary_box(3) > vehicle_point(2));
    
    if(test)
        x_old = x;
        y_old = y;
        y = boundary_box(3);
        if(x_old == vehicle_point(1))
            x = x_old;
        else
            x = (y - y_old) / m + x_old;
        end
    end
    
    test = (y < boundary_box(1)) && (boundary_box(1) < vehicle_point(2));
    
    if(test)
        x_old = x;
        y_old = y;
        y = boundary_box(1);
        if(x_old == vehicle_point(1))
            x = x_old;
        else
            x = (y - y_old) / m + x_old;
        end
    end
    
    position = [x;y];
end

function point_vehicle_frame = getPointInVehicleFrame(point, vehicle, type)

    distance = sqrt((point(1) - vehicle(1))^2 + (point(2) - vehicle(2))^2);
    
    point_vehicle_frame = [0;0];
    
    if(type == 1)
        point_vehicle_frame = [0;distance];
    end
    
    if(type == 2)
        point_vehicle_frame = [distance;0];
    end
    
    if(type == 3)
        point_vehicle_frame = [-distance;0];
    end
end

function centroid = findUpdate(point1, point2, point3)
    
    centroid = [point1(1) + point2(1) + point3(1);
                point1(2) + point2(2) + point3(2)];
    
end
