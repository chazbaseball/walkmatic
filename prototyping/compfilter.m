% Script for testing the concept of a comp filter
actual = [1 2 3 4 5 4 3 2 1 0 1 2];

length_of_signal = 12;
x1 = [2 3 4 5 6 6 5 4 3 2 3 4]; % Low frequency noise (bias)
x2 = [0.5 2 3 4 6 4 3 2 0 0 1 2]; % High frequency noise
y  = zeros(1,12);

% alpha = time_constant / sampling_period
alpha = 0.9;

% Continuous-Time Low-Pass -> H_low(s) = 1 / (tau * s + 1)
% Continuous-Time High-Pass -> H_high(s) = (tau * s) / (tau * s + 1)
% Complimentary Filter -> H_low(s) + H_high(s) = 1
% Y(s) = H_low(s) * X2(s) + H_high(s) * X1(s)
% Substitute s = (1 - z^-1) / sampling_period
for i=2:length_of_signal
    y(i) = x2(i) + alpha * x1(i) - alpha * x1(i - 1) + alpha * y(i - 1);
    y(i) = y(i) / (1 + alpha);
end

plot(actual, 'DisplayName','actual');
hold on;
plot(y, 'DisplayName','Complementary Filter');
hold on;
plot(x1, 'DisplayName','Measurement with Low-Freq Noise');
hold on;
plot(x2, 'DisplayName','Measurement with High-Freq Noise');
legend;

