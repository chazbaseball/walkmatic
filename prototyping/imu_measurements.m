% Example accelerometer output (m/s^2) in Sensor Frame
x_acc = [1 2 3 4 5 4 3 4 5 2 6 7 8 7 6 5 4 5 6 7]; % 20 measurements
y_acc = [1 2 3 2 1 4 6 7 5 4 8 9 1 2 3 4 3 4 5 6];

% World Reference Frame
world = [0;
         0];

% Calculate position from double integration of acceleration data
sum_x = 0;
sum_y = 0;
for i=1:20
   sum_x = sum_x + x_acc(i);
   sum_y = sum_y + y_acc(i);
   x_vel(i) = sum_x;
   y_vel(i) = sum_y;
end

sum_x = 0;
sum_y = 0;
for i=1:20
   sum_x = sum_x + x_vel(i);
   sum_y = sum_y + y_vel(i);
   x_pos(i) = sum_x;
   y_pos(i) = sum_y;
end

% Example gyroscope output (rad/s) in Sensor Frame
z_rot_vel = [1.54 1.54 0.1 -0.1 0.1 -0.1 0.1 0.1 0.1 0.2 0.3 0.4 0.5 0.2 0.3 0.4 0.3 -0.2 0.1 0.1]; % 20 measurements

clf; % Clear current figure

% Plot x part
plot(x_acc, 'DisplayName', 'Acceleration');
hold on;
plot(x_vel, 'DisplayName', 'Velocity');
hold on;
plot(x_pos, 'DisplayName', 'Position');
legend;

figure; % Create world position scatter plot
sum_z = 0;
for i=1:20
   sum_z = sum_z + z_rot_vel(i);
   z_rot_pos(i) = sum_x;
end

% Define rotation matrix
for i =1:20
    Rot = [cos(z_rot_pos(i)), -sin(z_rot_pos(i));
           sin(z_rot_pos(i)), cos(z_rot_pos(i))];

    pos_sensor_frame = [x_pos(i);
                        y_pos(i)];

    pos_world_frame(:,1) = Rot * world + pos_sensor_frame;
    pos_world_frame_x(i) = pos_world_frame(1,1);
    pos_world_frame_y(i) = pos_world_frame(2,1);
end

scatter(pos_world_frame_x, pos_world_frame_y);

figure; % New figure

% Calculate curvature

z_rot_acc = diff(z_rot_vel);
z_rot_acc = [1 z_rot_acc];

 curvature = z_rot_vel ./ x_vel;
 plot(curvature);
 hold on;
 curvature = z_rot_acc ./ x_acc;
 plot(curvature);

