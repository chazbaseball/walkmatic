#ifndef CONFIG_HPP
#define CONFIG_HPP

#include "stm32f4xx.h"
#include "../common/datatypes.hpp"

/*********************************/
// Running Configuration
// *******************************
// Define one of the following preprocessing
// variables to determine how this project should
// should be ran
// *******************************
// LCD_UNIT_TEST
// UART_UNIT_TEST
// SERVO_UNIT_TEST
// I2C_UNIT_TEST
// SPI_UNIT_TEST
// IMU_UNIT_TEST
// CONTROLLER_UNIT_TEST
// PLOTTER_UNIT_TEST
// CONDITIONER_UNIT_TEST
// PATH_GENERATOR_UNIT_TEST
// RECORD_UNIT_TEST
// PLAY_UNIT_TEST
// PATH_OBJECT_UNIT_TEST
// DIAGNOSTICS_UNIT_TEST
// UTILS_GEO_UNIT_TEST
// UTILS_TRIG_UNIT_TEST
// MODE_MANAGER_UNIT_TEST
// BBF_UNIT_TEST
// COMP_FILTER_UNIT_TEST
// ULTRASONIC_UNIT_TEST
// RUN_ALL
/*********************************/
#define RUN_ALL

 RCC_TypeDef * const g_RCC = reinterpret_cast<RCC_TypeDef *>(RCC_BASE);
 SYSCFG_TypeDef * const g_SYSCFG = reinterpret_cast<SYSCFG_TypeDef *>(SYSCFG_BASE);
 EXTI_TypeDef * const g_EXTI = reinterpret_cast<EXTI_TypeDef *>(EXTI_BASE);

namespace configTimestamp
{
	// Processor clock is currently at 16 MHz (HSI)
	 const uint32_t TICKS = 500u; // Number of ticks between two interrupts
	 const float TICKS_TO_SECONDS = 0.000031f; // 1 (PCLK / TICKS) = SECONDS
	 const float TICKS_TO_MICROSECONDS = 31.25f; // TICKS_TO_SECONDS * 1000000 = uSeconds. 
	 const uint32_t TICKS_DELAY_IMU = 10u; // Number of ticks from IMU data capture until data reception via I2C
	 const uint32_t TICKS_DELAY_ULTRASONIC = 50u; // Number of ticks from IMU data capture until data reception via I2C
	 const uint8_t PRIORITY = 30u;
}

namespace configDiagnostics
{
	typedef struct
	{
		const Module MODULE;
		const bool ENABLE;
	} Config;
	
	const Config UNIT_TEST_DIAGNOSTICS 		= {TEST,
											   true};
	const Config COMP_FILTER_DIAGNOSTICS 	= {COMP_FILTER,
											   false};
	const Config CONDITIONER_DIAGNOSTICS 	= {CONDITIONER,
											   false};
	const Config IMU_DIAGNOSTICS 		 	= {IMU,
											   false};
	const Config ULTRASONIC_DIAGNOSTICS 	= {ULTRASONIC,
											   false};
	const Config I2C_DIAGNOSTICS 			= {I2C,
											   false};
	const Config SPI_DIAGNOSTICS 			= {SPI,
											   false};
	const Config SERVO_DIAGNOSTICS 			= {SERVO,
											   true};
	const Config MODE_MANAGER_DIAGNOSTICS 	= {MODE_MANAGER,
											   true};
	const Config PATH_GENERATOR_DIAGNOSTICS = {PATH_GENERATOR,
											   true};
	const Config CONTROLLER_DIAGNOSTICS 	= {CONTROLLER,
											   true};
	const Config RECORD_DIAGNOSTICS 		= {RECORD,
											   false};
	const Config PLAY_DIAGNOSTICS 			= {PLAY,
											   false};
}

/*********************************/
// Common - Higher Level Modules
/*********************************/
namespace configPlotter
{
	 const int ARRAY_HEIGHT = 64;
	 const int ARRAY_WIDTH = 128;
     const int ARRAY_SIZE = 8192;
	 const int WALKER_ARRAY_HEIGHT = 25;
	 const int WALKER_ARRAY_WIDTH = 25;
	 const int WALKER_ARRAY_SIZE = 225;
}

namespace configController
{
	typedef struct
	{
		const configDiagnostics::Config DIAGNOSTICS_CONFIG;
		const float SCALE_TO_METERS;
		const float MAX_CURVATURE_ERROR;
		const float MAX_DISPLACEMENT_ERROR;
		const float MAX_CURVATURE; // cm
		const float MIN_CURVATURE; // cm
		const uint32_t WAYPOINTS_TO_CHECK;
	} Config;
	
	const Config DEFAULT = {configDiagnostics::CONTROLLER_DIAGNOSTICS,
							0.01f,
							0.1f,
							10.0f,
							0.03f, // Turning radius of 30cm
							0.0001f,
							2u};
}

namespace configWalkerDimensions
{
	const float axle_length = 24.0f; // cm
	const float axle_to_axle_length = 23.0f; // cm
}

namespace configTransform
{
	// The diagram below shows the axis of the walker frame (body frame) X+ is towards the walker's front
	// and Y+ is towards the walker's left. The Z+ axis is straight up. Rotations (yaw) follow
	// the right-hand rule such that 0 is at X+, pi/2 is at Y+, pi is at X-, and 3pi/2 is at Y-.
	//    		X
	//     		|
	//    	 ___|___
	//    	 |	|  |
	// Y ____|__|__|____
	//       |  |  |    
	//       |  |  |
	//       -------
	//			|
	//          |
	
	const Transform world = {0.0f, 0.0f, 0.0f}; 
	const Transform imu_to_walker = {-3.0f, -15.0f, -1.5708}; // Defined in imu's coordinate system in cm
	const Transform walker_to_ultrasonic = {30.0f, 0.0f, 0.0f}; // Defined in walker's coordinate system in cm
}

namespace configBinaryBayesFilter
{
	typedef struct
	{
		const float POS_INITIAL_STATE_PROB; // These two must equal 1
		const float NEG_INITIAL_STATE_PROB;
		const float POS_TO_POS_TRANSITION_PROB; // These two must equal 1
		const float POS_TO_NEG_TRANSITION_PROB;
		const float NEG_TO_POS_TRANSITION_PROB; // These two must equal 1
		const float NEG_TO_NEG_TRANSITION_PROB;
	} Config;
	
	const Config ZERO_VELOCITY = {0.5f,				
								  0.5f,
								  0.9f,
								  0.1f,
								  0.4f,
								  0.6f};
								  
}

namespace configConditioner
{
	enum CurvatureCalculation // Remember: arc_length = radius * theta
	{
		INTEGRATE_ACC, // angular_velocity / linear_velocity = curvature (Low Freq Noise)
		DIFF_GYRO // angular_acceleration / linear_acceleration = curvature (High Freq Noise)
	};
	
	enum DisplacementCalculation // Remember: arc_length = radius * theta
	{
		INTEGRATE_TWICE_ACC, // (Low Freq Noise)
		ULTRASONIC // (High Freq Noise)
	};
	
	enum ImuMeasurement // Refer to IMU Mode
	{
		IMU_MODE,
		AMG_MODE
	};
	
	typedef struct
	{
		const configDiagnostics::Config DIAGNOSTICS_CONFIG;
		const float HIGH_PASS_GAIN;
		const float LOW_PASS_GAIN;
		const float ALPHA_HIGH_PASS;
		const float ALPHA_LOW_PASS;
		float const LOWER_BOUND_TO_ZERO;
		float const UPPER_BOUND_TO_ZERO;
		float const BBF_ZERO_VEL_PROB_THRESH;
		const float DIVIDE_BY_ZERO_THRESH;
		const float MAX_SPEED; // m/s
		const float ULTRASONIC_BBF_CONTRADICT_THRESH;
		const ImuMeasurement MODE;
		const DisplacementCalculation DISPLACEMENT_OPTION_HIGH_FREQ_NOISE;
		const DisplacementCalculation DISPLACEMENT_OPTION_LOW_FREQ_NOISE;
		const CurvatureCalculation CURVATURE_OPTION_HIGH_FREQ_NOISE;
		const CurvatureCalculation CURVATURE_OPTION_LOW_FREQ_NOISE;
	} Config;
	
	const Config CONFIG_PLAY = {configDiagnostics::CONDITIONER_DIAGNOSTICS,
								 500.0f, //850.0f
								 10.0f,
								 0.001f,
								 0.5f,
								 -0.0001f,
								 0.0001f,
								 0.75f,
								 0.00001f,
								 1.0f,
								 10.0f,
								 IMU_MODE,
								 ULTRASONIC,
								 INTEGRATE_TWICE_ACC,
								 DIFF_GYRO,
								 INTEGRATE_ACC};
	 
	 const Config CONFIG_RECORD = {configDiagnostics::CONDITIONER_DIAGNOSTICS,
								   500.0f,
								   10.0f,
								   0.001f,
		                           0.5f,
								   -0.0001f,
								   0.0001f,
								   0.75f,
								   0.00001f,
								   1.0f,
								   10.0f,
								   IMU_MODE,
								   ULTRASONIC,
								   INTEGRATE_TWICE_ACC,
								   DIFF_GYRO,
								   INTEGRATE_ACC};
}

namespace configPathgenerator
{
	typedef struct
	{
		const configDiagnostics::Config DIAGNOSTICS_CONFIG;
		const float POINT_DISTANCE; // centimeters
		const float MAX_CURVATURE; // centimeters
	} Config;
	
	const Config CONFIG_DEFAULT = {configDiagnostics::PATH_GENERATOR_DIAGNOSTICS,
								   1.0f,
								   configController::DEFAULT.MAX_CURVATURE};
}

namespace configCompFilter
{
	typedef enum
	{
		MEASURED,
		COMMANDED
	} Curvature;
	
	typedef struct
	{
		const configDiagnostics::Config DIAGNOSTICS_CONFIG;
		const Curvature CURVATURE_OPTION;
		const float ALPHA;
		const float CURVATURE_THRESH; // 1/m
		const float CURVATURE_MAX;
		const float SCALE_TO_CENTIMETERS;
		const float SCALE_CURVATURE_TRAVERSAL;
	} Config;
	
	const Config CONFIG_RECORD = {configDiagnostics::COMP_FILTER_DIAGNOSTICS,
								   MEASURED,
								   0.5f,
								   0.1f,
								   5.0f,
								   1.2f,
								   100.0f}; // Account for discrepancy in the ultrsonic data
	
	const Config CONFIG_PLAY = {configDiagnostics::COMP_FILTER_DIAGNOSTICS,
								   COMMANDED,
								   0.5f,
								   0.1f,
								   5.0f,
								   1.2f,
								   100.0f}; // Account for discrepancy in the ultrsonic data
}

/***********************************************************************************************************/
// Common - Serial Interfaces
/***********************************************************************************************************/
namespace configI2c // No analog noise filter available on STM32F407 devices
{
	typedef struct
	{
		const configDiagnostics::Config DIAGNOSITCS_CONFIG;
		const uint16_t STM32F4_ADDRESS; // MAX 7 bits
		I2C_TypeDef * const I2C;
		const uint32_t I2C_RESET;
		const uint32_t I2C_CLOCK_ENABLE;
		const IRQn_Type I2C_ERROR_EVENT;
		const uint32_t RESET_COUNT;
		const uint32_t I2C_FREQ; // Select 16 Mhz as frequency (HSI is default system clock)
		const uint16_t I2C_CCR; // 0x50 = 80d -> 100kHz = 1/(T_high + T_low) where in Standard Mode T_high = 1/(16Mhz) * I2C_CCR and T_low = T_high, see pg. 870 in manual
		const uint16_t I2C_TRISE; // Maximum rise time for SCL
		GPIO_TypeDef * const GPIO_SDA; // PB7
		const uint32_t GPIO_CLOCK_ENABLE_SDA; 
		const uint8_t PIN_SDA;
		const uint32_t AF_SDA;
		GPIO_TypeDef * const GPIO_SCL; // PB8
		const uint32_t GPIO_CLOCK_ENABLE_SCL;
		const uint8_t PIN_SCL;
		const uint32_t AF_SCL;
	} Config;
	
	const Config IMU_CONFIG = {configDiagnostics::I2C_DIAGNOSTICS,
								   0x52, // MAX 7 bits
								   reinterpret_cast<I2C_TypeDef *>(I2C1_BASE),
								   RCC_APB1RSTR_I2C1RST,
								   RCC_APB1ENR_I2C1EN,
								   I2C1_ER_IRQn,
								   10000u,
								   0x10, // Select 16 Mhz as frequency (HSI is default system clock)
								   0x50, // 0x50 = 80d -> 100kHz = 1/(T_high + T_low) where in Standard Mode T_high = 1/(16Mhz) * I2C_CCR and T_low = T_high, see pg. 870 in manual
								   0x11, // Maximum rise time for SCL
								   reinterpret_cast<GPIO_TypeDef *>(GPIOB_BASE), // PB7
								   RCC_AHB1ENR_GPIOBEN,
								   7u,
								   4u,
								   reinterpret_cast<GPIO_TypeDef *>(GPIOB_BASE), // PB8
								   RCC_AHB1ENR_GPIOBEN,
								   8u,
								   4u};
}

namespace configSpi
{
	typedef struct
	{
		const configDiagnostics::Config DIAGNOSTICS_CONFIG;
		SPI_TypeDef * const SPI;
		const uint32_t SPI_CLOCK_EN;
		const uint32_t SPI_CLOCK_RST;
		GPIO_TypeDef * const GPIO_MOSI;
		const uint32_t GPIO_CLOCK_ENABLE_MOSI;
		const uint8_t PIN_MOSI;
		const uint32_t AF_MOSI;
		GPIO_TypeDef * const GPIO_MISO;
		const uint32_t GPIO_CLOCK_ENABLE_MISO;
		const uint8_t PIN_MISO;
		const uint32_t AF_MISO;
		GPIO_TypeDef * const GPIO_SCL;
		const uint32_t GPIO_CLOCK_ENABLE_SCL;
		const uint8_t PIN_SCL;
		const uint32_t AF_SCL;
		GPIO_TypeDef * const GPIO_SS;
		const uint32_t GPIO_CLOCK_ENABLE_SS;
		const uint8_t PIN_SS;
		const uint8_t BR_CONTROL; // 0u = f_PCLK/2 ... 7u = f_PCLK/256 (Increase by powers of 2)
		const uint32_t RESET_COUNT;
		
	} Config;
	
	const Config CONFIG_TEST = {configDiagnostics::SPI_DIAGNOSTICS,
								reinterpret_cast<SPI_TypeDef *>(SPI1_BASE),
								RCC_APB2ENR_SPI1EN,
								RCC_APB2RSTR_SPI1RST,
								reinterpret_cast<GPIO_TypeDef *>(GPIOA_BASE), // PA7
								RCC_AHB1ENR_GPIOAEN,
								7u,
								5u,
								reinterpret_cast<GPIO_TypeDef *>(GPIOA_BASE), // PA6
								RCC_AHB1ENR_GPIOAEN,
								6u,
								5u,
								reinterpret_cast<GPIO_TypeDef *>(GPIOA_BASE), // PA5
								RCC_AHB1ENR_GPIOAEN,
								5u,
								5u,
								reinterpret_cast<GPIO_TypeDef *>(GPIOE_BASE), // PE3
								RCC_AHB1ENR_GPIOEEN,
								3u,
								0u,
								100u};// 16/4 = 4Mhz
	
	const Config CONFIG_LCD = {configDiagnostics::SPI_DIAGNOSTICS,
								reinterpret_cast<SPI_TypeDef *>(SPI1_BASE),
								RCC_APB2ENR_SPI1EN,
								RCC_APB2RSTR_SPI1RST,
								reinterpret_cast<GPIO_TypeDef *>(GPIOA_BASE), // PA7
								RCC_AHB1ENR_GPIOAEN,
								7u,
								5u,
								reinterpret_cast<GPIO_TypeDef *>(GPIOA_BASE), // PA6
								RCC_AHB1ENR_GPIOAEN,
								6u,
								5u,
								reinterpret_cast<GPIO_TypeDef *>(GPIOA_BASE), // PA5
								RCC_AHB1ENR_GPIOAEN,
								5u,
								5u,
								reinterpret_cast<GPIO_TypeDef *>(GPIOE_BASE), // PE3
								RCC_AHB1ENR_GPIOEEN,
								3u,
								1u,
								100u};// 16/4 = 4Mhz
		
}

namespace configUart
{
	typedef struct
	{
		USART_TypeDef * const UART;
		GPIO_TypeDef * const GPIORX; 
		const uint8_t PIN_RX;
		GPIO_TypeDef * const GPIOTX; 
		const uint8_t PIN_TX;
		const uint32_t GPIO_CLOCK_ENABLE_RX; 
		const uint32_t GPIO_CLOCK_ENABLE_TX;
		const uint32_t AF_RX; // Alternate Function selection 0-15
		const uint32_t AF_TX; 
		const uint32_t UART_CLOCK_ENABLE;
		const uint32_t BAUD_RATE_DIVISOR; 
		const uint32_t RESET_COUNT;
	} Config;
	
	const Config CONFIG_DIAGNOSTICS = {reinterpret_cast<USART_TypeDef *>(UART5_BASE),
											  reinterpret_cast<GPIO_TypeDef *>(GPIOD_BASE), // PD2
											  2u,
											  reinterpret_cast<GPIO_TypeDef *>(GPIOC_BASE), // PC12
											  12u,
											  RCC_AHB1ENR_GPIODEN,
											  RCC_AHB1ENR_GPIOCEN,
											  8u,
											  8u,
											  RCC_APB1ENR_UART5EN,
											  0x682u, // 16Mhz / 9600 (HSI is the default system clock)
											  100u}; 
	
	const Config CONFIG_ULTRASONIC_1 = {reinterpret_cast<USART_TypeDef *>(USART3_BASE),
											  reinterpret_cast<GPIO_TypeDef *>(GPIOD_BASE), // PD9
											  9u,
											  reinterpret_cast<GPIO_TypeDef *>(GPIOD_BASE), // PD8
											  8u,
											  RCC_AHB1ENR_GPIODEN,
											  RCC_AHB1ENR_GPIODEN,
											  7u,
											  7u,
											  RCC_APB1ENR_USART3EN,
											  0x682u, // 16Mhz / 9600 (HSI is the default system clock)
											  300u}; 
	
	const Config CONFIG_ULTRASONIC_2 = {reinterpret_cast<USART_TypeDef *>(USART3_BASE),
											  reinterpret_cast<GPIO_TypeDef *>(GPIOD_BASE), // PD9
											  9u,
											  reinterpret_cast<GPIO_TypeDef *>(GPIOD_BASE), // PD8
											  8u,
											  RCC_AHB1ENR_GPIODEN,
											  RCC_AHB1ENR_GPIODEN,
											  7u,
											  7u,
											  RCC_APB1ENR_USART3EN,
											  0x682u,
											  100u}; // 16Mhz / 9600 (HSI is the default system clock)
	
	const Config CONFIG_ULTRASONIC_3 = {reinterpret_cast<USART_TypeDef *>(UART5_BASE),
											  reinterpret_cast<GPIO_TypeDef *>(GPIOD_BASE), // PD2
											  2u,
											  reinterpret_cast<GPIO_TypeDef *>(GPIOC_BASE), // PC12
											  12u,
											  RCC_AHB1ENR_GPIODEN,
											  RCC_AHB1ENR_GPIOCEN,
											  8u,
											  8u,
											  RCC_APB1ENR_UART5EN,
											  0x682u,
											  100u}; // 16Mhz / 9600 (HSI is the default system clock)

}

/***********************************************************************************************************/
// Common - Peripheral Drivers
/***********************************************************************************************************/
namespace configLcd
{
    
}

namespace configUltrasonic
{
	typedef struct
	{
		const configDiagnostics::Config DIAGNOSTICS_CONFIG;
		configUart::Config CONFIG_ULTRASONIC_1;
		configUart::Config CONFIG_ULTRASONIC_2;
		configUart::Config CONFIG_ULTRASONIC_3;
		const float MAX_RANGE_MM;
		const float START_UP_DELAY; // milliseconds
	} Config;
	
	const Config CONFIG_DEFAULT = {configDiagnostics::ULTRASONIC_DIAGNOSTICS,
								   configUart::CONFIG_ULTRASONIC_1,
								   configUart::CONFIG_ULTRASONIC_2,
								   configUart::CONFIG_ULTRASONIC_3,
								   5000.0f,
								   4000.0f};
}

namespace configServo
{
	typedef struct
	{
		GPIO_TypeDef * const GPIO;
		const uint32_t GPIO_CLOCK_ENABLE;
		const uint32_t AF;
		const uint8_t PIN;
		TIM_TypeDef * const TIMER;
		const uint32_t TIMER_CH;
		const uint32_t TIMER_CLOCK_ENABLE;
		const uint16_t INITIAL_DUTY_CYCLE; // Any number between 0 and AUTO_RELOAD
		const uint16_t MAX_DUTY_CYCLE;
		const uint16_t MIN_DUTY_CYCLE;
	} Servo;
	
	const Servo SERVO_0_DEFAULT = {reinterpret_cast<GPIO_TypeDef *>(GPIOB_BASE),
								   RCC_AHB1ENR_GPIOBEN,
								   2u,
								   1u,
								   reinterpret_cast<TIM_TypeDef *>(TIM3_BASE),
								   4u,
								   RCC_APB1ENR_TIM3EN,
								   1000u,
								   3800u,
								   2500u};
	
	const Servo SERVO_1_DEFAULT = {reinterpret_cast<GPIO_TypeDef *>(GPIOB_BASE),
								   RCC_AHB1ENR_GPIOBEN,
								   1u,
								   10u,
								   reinterpret_cast<TIM_TypeDef *>(TIM2_BASE),
								   3u,
								   RCC_APB1ENR_TIM2EN,
								   1000u,
								   3800u,
								   2500u};
	
	typedef struct
	{
		const configDiagnostics::Config DIAGNOSTICS_CONFIG;
		const Servo SERVO_0;
		const Servo SERVO_1;
		const uint16_t PRESCALER; // f_system_clock / f_timer_clock - 1 -> Get to 100kHz
		const uint16_t AUTO_RELOAD; // (1 + AUTO_RELOAD) * (1 / f_timer_clock) = T_timer_period
		const float MIN_THETA; // All angles are in radians!
		const float MAX_THETA;
	} Config;
	
	const Config CONFIG_DEFAULT = {configDiagnostics::SERVO_DIAGNOSTICS,
								  SERVO_0_DEFAULT,
								  SERVO_1_DEFAULT,
								  9u,
								  31999u,
								  -0.785398f, // pi/4
								  0.785398f};
}

namespace configImu
{
	typedef struct
	{
		const configDiagnostics::Config DIAGNOSTICS_CONFIG;
		const configI2c::Config I2C_CONFIG;
		const uint8_t MODE; // AMG mode = 0x07, IMU mode = 0x08 (see page 21 in BNO055 datasheet)
		const uint8_t UNITS; // m/s^2, degrees/s, Celsius (see page 30 in BNO055 datasheet)
		const uint8_t AXIS_MAP; // Default (see page 24 in BNO055 datasheet)
		const uint8_t AXIS_SIGN; // Default (see page 24 in BNO055 datasheet)
		const uint8_t ACC_CONFIGURATION; // Default (see page 27 in BNO055 datasheet)
		const uint8_t GYR_CONFIGURATION_1; // Default (see page 28 in BNO055 datasheet)
		const uint8_t GYR_CONFIGURATION_0; // Default
		const uint8_t MAG_CONFIGURATION; // Default (see page 29 in BNO055 datasheet)
		const uint32_t RESET_COUNT;
		const float ACC_NUM_TO_MPERSECSQ; // DO NOT CHANGE m/s^2 per bit
		const float ACC_NUM_TO_MILLIG; // DO NOT CHANGE
		const float GYR_NUM_TO_DEG; // DO NOT CHANGE
		const float GYR_NUM_TO_RAD; // DO NOT CHANGE
		const float HEADING_NUM_TO_DEG; // DO NOT CHANGE
		const float HEADING_NUM_TO_RAD; // DO NOT CHANGE
	} Config;
	
	const Config CONFIG_DEFAULT = {configDiagnostics::IMU_DIAGNOSTICS,
									configI2c::IMU_CONFIG,
									0x08,
									0x04, // m/s^2, degrees/s, Celsius
									0x24, // Default
									0x00, // Default
									0x0D, // Default
									0x00, // Default
									0x38, // Default
									0x6D, // Default
									10u,
									100.0f, // DO NOT CHANGE m/s^2 per bit
									1.0f, // DO NOT CHANGE
									16.0f, // DO NOT CHANGE
									900.0f, // DO NOT CHANGE
									16.0f, // DO NOT CHANGE
									915.0f}; // DO NOT CHANGE
}

/***********************************************************************************************************/
// Modes
/***********************************************************************************************************/
namespace configRecord 
{
	typedef struct
	{
		const configDiagnostics::Config DIAGNOSTICS_CONFIG;
		const configImu::Config IMU_CONFIG;
		const configUltrasonic::Config ULTRASONIC_CONFIG;
		const configConditioner::Config CONDITIONER_CONFIG;
		const configCompFilter::Config COMP_FILTER_CONFIG;
		const configPathgenerator::Config PATH_GENERATOR_CONFIG; 
		const float MAX_FREQUENCY; // Hz
	} Config;
	
	const Config CONFIG_DEFAULT = {configDiagnostics::RECORD_DIAGNOSTICS,
								   configImu::CONFIG_DEFAULT,
								   configUltrasonic::CONFIG_DEFAULT,
								   configConditioner::CONFIG_RECORD,
								   configCompFilter::CONFIG_RECORD,
								   configPathgenerator::CONFIG_DEFAULT,
								   20.0f};

}

namespace configPlay
{
	typedef struct
	{
		const configDiagnostics::Config DIAGNOSTICS_CONFIG;
		const configServo::Config SERVO_CONFIG;
		const configImu::Config IMU_CONFIG;
		const configUltrasonic::Config ULTRASONIC_CONFIG;
		const configConditioner::Config CONDITIONER_CONFIG;
		const configCompFilter::Config COMP_FILTER_CONFIG;
		const configController::Config CONTROLLER_CONFIG;
		const float MAX_FREQUENCY;
	} Config;
	
	const Config CONFIG_DEFAULT = {configDiagnostics::PLAY_DIAGNOSTICS,
								   configServo::CONFIG_DEFAULT,
								   configImu::CONFIG_DEFAULT,
								   configUltrasonic::CONFIG_DEFAULT,
								   configConditioner::CONFIG_PLAY,
								   configCompFilter::CONFIG_PLAY,
								   configController::DEFAULT,
								   5.0f};
}

namespace configBuzzer
{
	typedef struct
	{
		GPIO_TypeDef * const GPIO;
		const uint32_t GPIO_CLOCK_ENABLE;
		const uint8_t  PIN;
		const uint32_t ALTERNATE_FUNCTION;
		TIM_TypeDef * const BUZZER_TIMER;
		const uint32_t TIMER1_CLOCK_ENABLE;
		const uint32_t PRESCALER;
		const uint32_t AUTO_RELOAD;
		const uint32_t DUTY_CYCLE;
	} Config;
		
	const Config CONFIG_DEFAULT = 
	{
		reinterpret_cast<GPIO_TypeDef *>(GPIOE_BASE),
		RCC_AHB1ENR_GPIOEEN,												// GPIOE Clock
		11u,																				// Pin E11
		0x1000,																			// bit 12 in AFR[1] for Alternate function 1 on PE11
		reinterpret_cast<TIM_TypeDef *>(TIM1_BASE),	// Timer 1
		RCC_APB2ENR_TIM1EN,													// Timer 1 clock
		90u,																				// Prescaler = 90 (44.1 kHz timer)
		999u,																				// ARR = 999 (441 Hz default note)
		500u																				// Set duty cycle to 50%
	};
}

namespace configModeManager
{
	/*********************************/
	// Interrupt configs
	/*********************************/

	typedef struct
	{
		const uint8_t EXTI_CR;
		const uint16_t SYSCFG_EXTI_CLEAR; 
		const uint16_t SYSCFG_EXTI_SET;
		const uint32_t EXTI_RT; // Rising Trigger
		const uint32_t EXTI_FT; // Falling Trigger
		const uint32_t EXTI_IM; // Interrupt mask
		const IRQn_Type EXTI_TYPE; // Used to set NVIC
		const uint8_t PRIORITY; // Cannot be lower than 13
	} InterruptConfig;
	
	typedef struct
	{
		const uint32_t TIMER_5_CLOCK_ENABLE;
		const uint16_t PRESCALER; 		// f_system_clock / f_timer_clock - 1
		const uint32_t AUTO_RELOAD; 	// (1 + AUTO_RELOAD) * (1 / f_timer_clock) = T_timer_period
		const uint32_t PRELOAD; 			// bit 2 mask
		const uint32_t CAPTURE_VALUE;
		const uint8_t PRIORITY;
		TIM_TypeDef * const TIMER_5;
	} ChargeTimerConfig;
	
	// GPIO Configs
	typedef struct
	{
		GPIO_TypeDef * const GPIO;
		const uint32_t GPIO_CLOCK_ENABLE;		
		const uint8_t PIN;
	} SignalConfig;
	
	const ChargeTimerConfig CHARGE_TIMER = {RCC_APB1ENR_TIM5EN,	// TIMER_5_CLOCK_ENABLE
																					15999u,							// PRESCALER
																					29999u,							// AUTO_RELOAD
																					0x00000004,					// PRELOAD (bit 2 mask)
																					0u,									// CAPTURE VALUE
																					0u,									// PRIORITY (highest priority)
																					reinterpret_cast<TIM_TypeDef *>(TIM5_BASE)};

	// PA9
	const SignalConfig CONFIG_READY = {reinterpret_cast<GPIO_TypeDef *>(GPIOA_BASE),
											  RCC_AHB1ENR_GPIOAEN,
											  9u};

	const InterruptConfig CONFIG_READY_INTERRUPT = {1u,
														   SYSCFG_EXTICR3_EXTI9,
														   SYSCFG_EXTICR3_EXTI9_PA,
														   EXTI_RTSR_TR9,
														   EXTI_FTSR_TR9,
														   EXTI_IMR_IM9,
														   EXTI9_5_IRQn,
														   17u};
	// PA8
	const SignalConfig CONFIG_RECORD = {reinterpret_cast<GPIO_TypeDef *>(GPIOA_BASE),
											   RCC_AHB1ENR_GPIOAEN,
											   8u};
	
	// PA15
	const SignalConfig CONFIG_PLUGGED_IN = {reinterpret_cast<GPIO_TypeDef *>(GPIOA_BASE),
												   RCC_AHB1ENR_GPIOAEN,
												   15u};
	
	// PD10
	const SignalConfig CONFIG_CELL_1_EN = {reinterpret_cast<GPIO_TypeDef *>(GPIOD_BASE),
											   RCC_AHB1ENR_GPIODEN,
											   10u};
	
	// PD11
	const SignalConfig CONFIG_CELL_2_EN = {reinterpret_cast<GPIO_TypeDef *>(GPIOD_BASE),
											   RCC_AHB1ENR_GPIODEN,
											   11u};
	
	// PD12
	const SignalConfig CONFIG_CELL_3_EN = {reinterpret_cast<GPIO_TypeDef *>(GPIOD_BASE),
											   RCC_AHB1ENR_GPIODEN,
											   12u};
	
	// PD13
	const SignalConfig CONFIG_CELL_4_EN = {reinterpret_cast<GPIO_TypeDef *>(GPIOD_BASE),
											   RCC_AHB1ENR_GPIODEN,
											   13u};
	
	// PA4
	const SignalConfig CONFIG_CELL_1_L = {reinterpret_cast<GPIO_TypeDef *>(GPIOA_BASE),
											   RCC_AHB1ENR_GPIOAEN,
											   4u};
	
	// PA5
	const SignalConfig CONFIG_CELL_2_L = {reinterpret_cast<GPIO_TypeDef *>(GPIOA_BASE),
											   RCC_AHB1ENR_GPIOAEN,
											   5u};

	// PA6
	const SignalConfig CONFIG_CELL_3_L = {reinterpret_cast<GPIO_TypeDef *>(GPIOA_BASE),
											   RCC_AHB1ENR_GPIOAEN,
											   6u};
	
	// PA7
	const SignalConfig CONFIG_CELL_4_L = {reinterpret_cast<GPIO_TypeDef *>(GPIOA_BASE),
											   RCC_AHB1ENR_GPIOAEN,
											   7u};											 
												 
	// PD14
	const SignalConfig CONFIG_CHARGE_EN = {reinterpret_cast<GPIO_TypeDef *>(GPIOD_BASE),
										   RCC_AHB1ENR_GPIODEN,
										   14u};
	/*********************************/
	// ModeManager configs
	/*********************************/
	typedef struct
	{
		const configDiagnostics::Config DIAGNOSTICS_CONFIG;
		const configBuzzer::Config BUZZER_CONFIG;
		const SignalConfig READY;
		const InterruptConfig READY_INT;
		const ChargeTimerConfig TIMER;
		const SignalConfig RECORD;
		const SignalConfig PLUGGED_IN;
		const SignalConfig CELL_1_BAL_EN;
		const SignalConfig CELL_2_BAL_EN;
		const SignalConfig CELL_3_BAL_EN;
		const SignalConfig CELL_4_BAL_EN;
		const SignalConfig CELL_1_BAL_L;
		const SignalConfig CELL_2_BAL_L;
		const SignalConfig CELL_3_BAL_L;
		const SignalConfig CELL_4_BAL_L;
		const SignalConfig CHARGE_EN;
		const uint32_t DEBOUNCE_COUNT;
	} Config;
	
	const Config DEFAULT = {configDiagnostics::MODE_MANAGER_DIAGNOSTICS,
							configBuzzer::CONFIG_DEFAULT,
								  CONFIG_READY,
								  CONFIG_READY_INTERRUPT,
								  CHARGE_TIMER,
								  CONFIG_RECORD,
								  CONFIG_PLUGGED_IN,
								  CONFIG_CELL_1_EN,
								  CONFIG_CELL_2_EN,
								  CONFIG_CELL_3_EN,
								  CONFIG_CELL_4_EN,
								  CONFIG_CELL_1_L,
								  CONFIG_CELL_2_L,
								  CONFIG_CELL_3_L,
								  CONFIG_CELL_4_L,
								  CONFIG_CHARGE_EN,
								  100000u};
}

#endif

