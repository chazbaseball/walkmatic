# Senior Design 2020: Walkmatic #

This repository contains the C++ code used for the Walkmatic. The Walkmatic is a baby walker that allows an infant to learn how to walk
without learning how to steer at the same time. A parent traces a closed path, and the walker ensures that baby stays on that path.

![](diagrams/main.png)

![](diagrams/ICD.png)

## Contained in this Repo ##
There are five folders which represent the most important, upper level modules in our system:

* play
* record
* configuration
* common

The fifth folder, diagrams, contains images of plots that are included in the READMEs throughout this repository.

## Branching/Source Control Strategy ##

1. Only working code shall be placed in the master branch. 
2. Before merging to master, a pull request shall be created.This includes documentation. 
3. Branches shall have the form of "feature/<name_of_feature>".
4. Code shall be committed atleast once every 4 hours of work.
5. Two people shall not work on the same file at the same time.

## Build Environment ##

1. Download Keil uVision from [here](https://www.keil.com/demo/eval/arm.htm)
2. When the PACK installer windows pops up, double click on STM32F407G (or whatever is closest)
3. On the top of the right hand side of the PACK installer, install the device specific PACK
4. Install the ARM::CMSIS PACK
5. Clone this repository
6. Open the Keil uVision project "walkmatic" in this directory
7. Add include paths to the CMSIS Core includes and the stm32f4xx header files
8. Download the [ST-Link Debugger Driver](https://www.st.com/content/st_com/en/products/development-tools/software-development-tools/stm32-software-development-tools/stm32-programmers/stsw-link004.html?dl=aegtb9vTj54uuHHtPZUThA%3D%3D%2C8tlVlQlsQl2BaDxvJ38l8n%2FRFEPmNHvGeI0Y86KKPLto%2FPP1foaJMaoPawOgY2AWI8pFqUnrnRXtClevGKGCr5KjZA35t6j0mf3fHdqr4fPcM7V70KbW1fs%2FWwICnltdCKTplOeTAWo%2FsDMOrDAKhq%2Bg2EMtChlk5Ir0MU4wyhj%2FeGeTJYB2luwdPmyXPxEfBBturWsHe3ScTQg%2BH5PjXqCRAQHMGn0QIRVW8bXGmkEdvIbnXtr7G30ejfbLLy1WsZdfH4grEWZTnIH8eLgzRsckoakzE4VXo%2FGKZz0D%2BDCKEGx9sFLdryierpcfetWzOMUaKougE%2BXbcZlkEMVF%2Fg%3D%3D&uid=jDZ+iXzaQ0x0uPD3URz9PbNFpwBUcUVmN6F7mG4KnqM=)

![](documents/includepaths.jpg)

9. Under Target->Options->Debug

![](documents/uvisionDebug.jpg)

10. Under Target->Options->Linker

![](documents/uvisionLinker.jpg)

11. Under Target->Options->Debug->Settings

![](documents/uvisionDebugSettings.jpg)

## Special Notes ##
1. Include paths use each file's disk location. Do not change includes when they do not match when viewed in the uVision IDE.



